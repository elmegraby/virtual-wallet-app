/*
Template:  		Payyed HTML Template
Written by: 	Harnish Design - (http://www.harnishdesign.net)
*/

(function ($) {
    "use strict";

// Preloader
    $(window).on('load', function () {
        $('[data-loader="circle-side"]').fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(333).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(333);
    });

    /*---------------------------------------------------
        Primary Menu
    ----------------------------------------------------- */

// Dropdown show on hover
    $('.primary-menu ul.navbar-nav li.dropdown, .login-signup ul.navbar-nav li.dropdown').on("mouseover", function () {
        if ($(window).width() > 991) {
            $(this).find('> .dropdown-menu').stop().slideDown('fast');
            $(this).bind('mouseleave', function () {
                $(this).find('> .dropdown-menu').stop().css('display', 'none');
            });
        }
    });

// When dropdown going off to the out of the screen.
    $('.primary-menu .dropdown-menu').each(function () {
        var menu = $('#header .header-row').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#header .header-row').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });
    $(function () {
        $(".dropdown li").on('mouseenter mouseleave', function (e) {
            if ($(window).width() > 991) {
                var elm = $('.dropdown-menu', this);
                var off = elm.offset();
                var l = off.left;
                var w = elm.width();
                var docW = $(window).width();

                var isEntirelyVisible = (l + w <= docW);
                if (!isEntirelyVisible) {
                    $(elm).addClass('dropdown-menu-right');
                } else {
                    $(elm).removeClass('dropdown-menu-right');
                }
            }
        });
    });

// DropDown Arrow
    $('.primary-menu').find('a.dropdown-toggle').append($('<i />').addClass('arrow'));

// Mobile Collapse Nav
    $('.primary-menu .dropdown-toggle[href="#"], .primary-menu .dropdown-toggle[href!="#"] .arrow').on('click', function (e) {
        if ($(window).width() < 991) {
            e.preventDefault();
            var $parentli = $(this).closest('li');
            $parentli.siblings('li').find('.dropdown-menu:visible').slideUp();
            $parentli.find('> .dropdown-menu').stop().slideToggle();
            $parentli.siblings('li').find('a .arrow.open').toggleClass('open');
            $parentli.find('> a .arrow').toggleClass('open');
        }
    });

// Mobile Menu Button Icon
    $('.navbar-toggler').on('click', function () {
        $(this).toggleClass('open');
    });


    /*---------------------------------------------------
       Carousel (Owl Carousel)
    ----------------------------------------------------- */

    $(".owl-carousel").each(function (index) {
        var a = $(this);
        $(this).owlCarousel({
            autoplay: a.data('autoplay'),
            autoplayTimeout: a.data('autoplaytimeout'),
            autoplayHoverPause: a.data('autoplayhoverpause'),
            loop: a.data('loop'),
            speed: a.data('speed'),
            nav: a.data('nav'),
            dots: a.data('dots'),
            autoHeight: a.data('autoheight'),
            autoWidth: a.data('autowidth'),
            margin: a.data('margin'),
            stagePadding: a.data('stagepadding'),
            slideBy: a.data('slideby'),
            lazyLoad: a.data('lazyload'),
            navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
            animateOut: a.data('animateOut'),
            animateIn: a.data('animateIn'),
            video: a.data('video'),
            items: a.data('items'),
            responsive: {
                0: {items: a.data('items-xs'),},
                576: {items: a.data('items-sm'),},
                768: {items: a.data('items-md'),},
                992: {items: a.data('items-lg'),}
            }
        });
    });

    /*---------------------------------------------------
        YouTube video to autoplay in modal
    ----------------------------------------------------- */
// Gets the video src from the data-src on each button
    var $videoSrc;
    $('.video-btn').on('click', function () {
        $videoSrc = $(this).data("src");
    });
    console.log($videoSrc);
// when the modal is opened autoplay it  
    $('#videoModal').on('shown.bs.modal', function (e) {
// set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates...you never know what you're gonna get
        $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0&amp;rel=0");
    })
// stop playing the youtube video when I close the modal
    $('#videoModal').on('hide.bs.modal', function (e) {
        $("#video").attr('src', $videoSrc);
    })

    /*---------------------------------------------------
       tooltips
    ----------------------------------------------------- */
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    /*---------------------------------------------------
       Scroll to top
    ----------------------------------------------------- */
    $(function () {
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 150) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
    });
    $('#back-to-top').on("click", function () {
        $('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });


    $('.smooth-scroll a').on("click", function () {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(sectionTo).offset().top - 50
        }, 600);
    });


})(jQuery);

function getRates(form) {
    var sendAmount = $('#youSend').val();
    var senderCurrency = $('#youSendCurrency').val();
    var recipientCurrency = $('#recipientCurrency').val();
    var recipientAmount = $('#recipientGets').val();

    alert(sendAmount);
    alert(senderCurrency);
    alert(recipientCurrency);

}

function deleteCard(id) {

    var answer = confirm('Are you sure you want to remove this card?');

    if (answer == true) {
        $.ajax({
            type: "POST",
            url: '/payment-tools/card-delete',
            data: {id: id}, //change to id
            success: function () {
                location.reload();
            }
            // dataType: dataType
        });
    }

}

function manageWallet(id) {
    window.location = 'http://localhost:8080/payment-tools/manage' + id;
}

function setRecipient(id) {

    $("#recipientId" + id).val(id);
    $('#myForm' + id).modal({backdrop: false});
    $('#myForm' + id).modal('toggle');

}

function confirmTransaction(form) {
    var formNumId = form.id;
    alert(formNumId);
}

function setWallet(id) {
    $("#walletTopUpId" + id).val(id);

    $('#walletForm' + id).modal('toggle');
}

// function editCard() {
//     document.getElementById("updateForm").submit();
// }

function lockForm(inputForm) {
    var formId = inputForm.id.split('transactionForm')[1];

    var walletUsed = $("#selectWallet" + formId).val();
    var recipientId = $("#recipientId" + formId).val();
    var amountSent = $("#amount" + formId).val();
    var senderId = $("#senderId" + formId).val();


    $.ajax({
        type: "POST",
        url: '/api/v1/exchange',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            walletId: walletUsed,
            amount: amountSent,
            recipientId: recipientId,
            senderId: senderId
        }),
        success: function (data) {
            $('#showExRate' + formId).text(data);
            document.getElementById('showExRate' + formId).style.display = 'block';
            document.getElementById('warning' + formId).style.display = 'block';
        }, error: function (jqxhr) {
            if (jqxhr.status == 503 || jqxhr.status == 200) {
                $('#showExRate' + formId).text(jqxhr.responseText);
            } else {
                $('#showExRate' + formId).text('We could not provide an exchange rate');
            }

            document.getElementById('showExRate' + formId).style.display = 'block';
            document.getElementById('warning' + formId).style.display = 'block';
        }

    });


    var recipientName = $('#senderMessage' + formId).text().split('to ')[1];
    $('#senderMessage' + formId).text('Please, confirm the following payment to ' + recipientName);
    $(".innerFormDiv").css("background-color", "#FAEBD7");
    document.getElementById('mockSend' + formId).style.display = 'none';
    document.getElementById('realSumbitButton' + formId).style.display = 'block';
    document.getElementById('editButton' + formId).style.display = 'block';
}

function unlockForm(inputForm) {
    var formId = inputForm.id.split('transactionForm')[1];
    var recipientName = $('#senderMessage' + formId).text().split('to ')[1];
    $(".innerFormDiv").css("background-color", "white");
    document.getElementById('mockSend' + formId).style.display = 'block';
    document.getElementById('realSumbitButton' + formId).style.display = 'none';
    document.getElementById('editButton' + formId).style.display = 'none';
    $('#senderMessage' + formId).text('Sending money to ' + recipientName);
    document.getElementById('showExRate' + formId).style.display = 'none';
    document.getElementById('warning' + formId).style.display = 'none';
}

function lockToUpForm(inputForm) {
    var formId = inputForm.id.split('fundWalletForm')[1];

    var walletName = $('#senderMessage' + formId).text().split('wallet ')[1];


    $('#senderMessage' + formId).text('Please, confirm adding money to ' + walletName);
    $(".innerFormDiv").css("background-color", "#FAEBD7");
    document.getElementById('mockSend' + formId).style.display = 'none';
    document.getElementById('realSumbitButton' + formId).style.display = 'block';
    document.getElementById('editButton' + formId).style.display = 'block';
}

function unlockToUpForm(inputForm) {
    var formId = inputForm.id.split('fundWalletForm')[1];

    var walletName = $('#senderMessage' + formId).text().split('wallet ')[1];
    alert(walletName);

    $(".innerFormDiv").css("background-color", "white");
    document.getElementById('mockSend' + formId).style.display = 'block';
    document.getElementById('realSumbitButton' + formId).style.display = 'none';
    document.getElementById('editButton' + formId).style.display = 'none';
    $('#senderMessage' + formId).text('Sending money to ' + walletName);
}

function confirmTopUp(form) {

    alert('in');

    if (confirm("Are you sure you want to submit the value of ?"))
        $(form).submit();
    else
        return false;


// $('#topUpMessage').text('Your about to add ' +  amount + ' ' + currency + ' to wallet ' + walletName + ' using card ' + card + ' ?');
//     $('#top-up-confirm').modal('show');
}


function goToCards() {
    document.getElementById("go-back").submit();
}

function updateName(){
    var full_name = document.forms["personaldetails"].firstName.value;

    var name_format = /^([\w]{3,})+\s+([\w\s]{3,})+$/;

    if (!full_name.match(name_format)) {
        document.getElementById('error-fName').innerHTML = ' Please enter a valid full name*'
        return false;
    }

    return true;
}


function pass() {
    var full_name = document.forms["sign-upForm"].fullName.value;
    var user_name = document.forms["sign-upForm"].username.value;
    var email_address = document.forms["sign-upForm"].email.value;
    var phone_number = document.forms["sign-upForm"].phoneNumber.value;
    var pass_1 = document.forms["sign-upForm"].password.value;
    var pass_2 = document.forms["sign-upForm"].confirmPassword.value;



    var name_format = /^([\w]{3,})+\s+([\w\s]{3,})+$/;

    if (!full_name.match(name_format)) {
        document.getElementById('error-fullName').innerHTML = ' Please enter a valid full name*'
        return false;
    }


    if (user_name.length < 8 || user_name.length > 15) {
        document.getElementById('error-uname').innerHTML = ' Username must be between 8 and 15 characters long. *'
        return false;
    }

    var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!email_address.match(mail_format)) {
        document.getElementById('error-email').innerHTML = ' Please enter a valid email address*'
        return false;
    }

    var validation = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    if ((!phone_number.match(validation))) {
        document.getElementById('error-phone').innerHTML = ' Please enter a valid phone number*'
        return false;
    }

    if (pass_1 !== pass_2) {
        document.getElementById('error-pass').innerHTML = ' Passwords must match! *'
        document.getElementById('error-pass1').innerHTML = ' Passwords must match! *'
        return false;
    }

    if (pass_1.length < 8 || pass_1.length > 20) {
        document.getElementById('error-pass').innerHTML = ' Password must be between 8 and 20 characters long *'
        return false;
    }

    return true;
}

function verifyEmail() {

    var email_address = document.forms["emailAddress-form"].email.value;


    var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!email_address.match(mail_format)) {
        document.getElementById('error-email').innerHTML = ' Please enter a valid email address*'
        return false;
    }

    return true;
}

function verifyFullName() {

    var full_name = document.forms["updateCard"].editcardHolderName.value;
    var cvs  = document.forms["updateCard"].editcvvNumber.value;

    var cvsVal =  /^[0-9]*$/;

    if (!cvs.match(cvsVal)) {
        document.getElementById('error-cvs').innerHTML = ' Please enter a valid CVV number*'
        return false;
    }

    var name_format = /^([\w]{3,})+\s+([\w\s]{3,})+$/;
    if (!full_name.match(name_format)) {
        document.getElementById('error-fullname').innerHTML = ' Please enter a valid full name*'
        return false;
    }

    return true;
}

function verifyPhone() {

    var phone_number = document.forms["phone"].mobileNumber.value;


    var validation = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    if ((!phone_number.match(validation))) {
        document.getElementById('error-phone1').innerHTML = ' Please enter a valid phone number*'
        return false;
    }

    return true;
}


function validatePassword() {

    var new_password = document.forms["changePassword"].newPassword.value;
    var confirm_password = document.forms["changePassword"].confirmPassword.value;

    if (new_password !== confirm_password) {
        document.getElementById('error-pass').innerHTML = ' Passwords must match! *'
        document.getElementById('error-pass1').innerHTML = ' Passwords must match! *'
        return false;
    }

    if (new_password.length < 8 || new_password.length > 20) {
        document.getElementById('error-pass').innerHTML = ' Password can be between 8 and 20 characters long *'
        return false;
    }


    return true;
}


function validateCreditCard() {

    var ccNum = document.forms["addCard"].cardNumber.value;
    var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
    var amexpRegEx = /^(?:3[47][0-9]{13})$/;
    var discovRegEx = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;

    if (visaRegEx.test(ccNum)) {
        document.getElementById('error-card').innerHTML = ' Please enter a valid card number*'
        return false;
    } else if (mastercardRegEx.test(ccNum)) {
        document.getElementById('error-card').innerHTML = ' Please enter a valid card number*'
        return false;
    } else if (amexpRegEx.test(ccNum)) {
        document.getElementById('error-card').innerHTML = ' Please enter a valid card number*'
        return false;
    } else if (discovRegEx.test(ccNum)) {
        document.getElementById('error-card').innerHTML = ' Please enter a valid card number*'
        return false;
    }

    return true;
}

// Function to convert form to JSON
function formToJson(form) {
    var formArray = $(form).serializeArray();
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
};


function returnExchangeRate(source, target, amount) {

    if (source!==target) {
        var url = 'http://localhost:8080/api/v1/exchange/convert?source=' + source + '&target=' + target + '&amount=' + amount;
        $.ajax({
            url: url,
            success: function (result) {
                $('#recipientGets').val(parseFloat(result).toFixed(3));
            },
            error: function (jqXHR) {
                $('#recipientGets').text('N/A!');
            }
        });
    } else {
        $('#recipientGets').text(amount);
    }
}


function submitTransaction(form) {

    alert('in');

    $.ajax({
        type: "post",
        url: '/test/verify',
        data: $(form).serialize(),
        success: function (responseData, textStatus, jqXHR) {

            alert(responseData.rate);
            alert(textStatus.toString());
            alert(jqXHR.toString());


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('We couldn`t process your payment');
        }
    });


}


//testing!!! remove if unused!!!
function verifyConversion(recipientId, senderId) {
    alert(recipientId);
    alert(senderId);

    var urlToGetRecipient = "http://localhost:8080/api/v1/users/getById/" + recipientId;
    var urlToGetSender = "http://localhost:8080/api/v1/users/getById/" + senderId;
    var recipientCurrency;
    var senderWalletId = document.getElementById('senderWallet' + senderId).value;


    alert('the sender is using wallet with id ' + senderWalletId);

    var senderCurrency;

    $.ajax({
        url: urlToGetRecipient,
        success: function (result) {
            recipientCurrency = result.defaultWallet.currencyCode;
        },
        error: function (jqXHR, exception) {
            alert('error getting recipient currency')
            // Your error handling logic here..
        }
    });

    $.ajax({
        url: urlToGetSender,
        success: function (result) {
            recipientCurrency = result.defaultWallet.currencyCode;
        },
        error: function (jqXHR, exception) {
            alert('error getting recipient currency')
            // Your error handling logic here..
        }
    });
}

$(document.getElementById('bar-search')).ready(function () {
    $('.submit-on-enter').keydown(function (event) {
        if (event.keyCode === 13) {
            this.form.submit();
            return false;
        }
    });
});
