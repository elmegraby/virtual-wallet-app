package com.ilievmegraby.vwallet.exceptions;

public class ExceededReferralsException extends RuntimeException{

    public ExceededReferralsException(String message){
        super(message);
    }
}
