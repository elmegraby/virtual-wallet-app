package com.ilievmegraby.vwallet.exceptions;

public class NoDefaulWalletException extends RuntimeException {
    public NoDefaulWalletException(String message){
        super(message);
    }
}
