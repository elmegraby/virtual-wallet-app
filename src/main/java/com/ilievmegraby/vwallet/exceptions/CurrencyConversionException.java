package com.ilievmegraby.vwallet.exceptions;

public class CurrencyConversionException extends RuntimeException {
    public CurrencyConversionException(String message){
        super(message);
    }
}
