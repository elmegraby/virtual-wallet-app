package com.ilievmegraby.vwallet.exceptions;

public class ImageReadException extends RuntimeException{

    public ImageReadException(String message){
        super(message);
    }
}
