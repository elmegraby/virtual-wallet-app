package com.ilievmegraby.vwallet.exceptions;

public class CardDeniedException extends RuntimeException{
    public CardDeniedException(String message){
        super(message);
    }
}
