package com.ilievmegraby.vwallet.exceptions;

public class APIConnectionException extends RuntimeException{

    public APIConnectionException(String message){
        super(message);
    }
}
