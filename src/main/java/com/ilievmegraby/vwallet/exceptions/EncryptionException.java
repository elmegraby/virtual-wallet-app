package com.ilievmegraby.vwallet.exceptions;

public class EncryptionException extends RuntimeException {
    public EncryptionException(String message){
        super(message);
    }
}
