package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.TransactionDirection;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {
    private final int PAGE_SIZE = 5;
    private SessionFactory sessionFactory;
    private ExchangeRateService exchangeRateService;

    @Autowired
    public TransactionRepositoryImpl(SessionFactory sessionFactory, ExchangeRateService exchangeRateService) {
        this.sessionFactory = sessionFactory;
        this.exchangeRateService = exchangeRateService;
    }

    @Override
    public List<Transaction> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction", Transaction.class);
            return query.list();
        }
    }

    @Override
    public Transaction getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.get(Transaction.class, id);

          /*  if (transaction == null) {
                throw new EntityNotFoundException((String.format(ENTITY_NOT_FOUND_BY_ID_MESSAGE, "Transaction", id)));
            }

           */

            return transaction;
        }
    }

    @Override
    public List<Transaction> filter(String username, String direction, Date from, Date to, String recipient, String sortBy, int page) {
        StringBuilder hql = new StringBuilder("from  Transaction ");
        hql.append(buildQuery(username, direction, from, to, recipient, sortBy, page));

        try (Session session = sessionFactory.openSession()) {
            Query<Transaction> query = session.createQuery(hql.toString(), Transaction.class);
            query.setParameter("username", username.isEmpty()? "%" + username + "%" : username);
            query.setParameter("startDate", from);
            query.setParameter("endDate", to);
            query.setFirstResult((page - 1) * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);

            if (hql.toString().contains(":recipientName")) {
                query.setParameter("recipientName", recipient.isEmpty() ? "%" + recipient + "%" : recipient);
            }

            List<Transaction> result = query.list();
            return result;
        }
    }

    @Override
    public Transaction create(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transaction);
            session.getTransaction().commit();
            return transaction;
        }
    }

    @Override
    public void processTransaction(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            transaction.setVerified(true);
            session.update(transaction);
            Wallet senderWallet = transaction.getWalletUsed();

            senderWallet.setCurrentAmount(senderWallet.getCurrentAmount().subtract(transaction.getAmount()));
            session.update(senderWallet);

            Wallet recipientWallet = transaction.getRecipient().getDefaultWallet();

            CurrencyCode senderCurrency = senderWallet.getCurrencyCode();
            CurrencyCode recipientCurrency = recipientWallet.getCurrencyCode();

            if (senderCurrency != recipientCurrency) {
                BigDecimal sentAmount = transaction.getAmount();
                BigDecimal toBeReceived = exchangeRateService.convert(sentAmount, senderCurrency, recipientCurrency);
                recipientWallet.setCurrentAmount(recipientWallet.getCurrentAmount().add(toBeReceived));
            } else {
                recipientWallet.setCurrentAmount(recipientWallet.getCurrentAmount().add(transaction.getAmount()));
            }

            session.update(recipientWallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(transaction);
            session.getTransaction().commit();
            ;
        }
    }

    @Override
    public List<Transaction> getUserTransactions(String username) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Transaction> result = session.createQuery("from Transaction where sender.userName=:username or recipient.userName=:username", Transaction.class);
            result.setParameter("username", username);
            session.getTransaction().commit();
            return result.list();
        }
    }

    @Override
    public int getNumberOfPages(String username, String direction, Date from, Date to, String recipient, String sortBy, int page) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder hql = new StringBuilder("select count(*) from Transaction ");
            hql.append(buildQuery(username, direction, from, to, recipient, sortBy, page));
            Query query = session.createQuery(hql.toString());
            query.setParameter("username", username.isEmpty()? "%" + username + "%" : username);
            query.setParameter("startDate", from);
            query.setParameter("endDate", to);

            if (hql.toString().contains(":recipientName")) {
                query.setParameter("recipientName", recipient.isEmpty() ? "%" + recipient + "%" : recipient);
            }

            Long count = ((Long) query.uniqueResult());
            Integer totalTransactions = count.intValue();
            int pages = (totalTransactions + PAGE_SIZE - 1) / PAGE_SIZE;

            if (pages > 5) {
                pages = 5;
            }

            return pages;
        }
    }


    @Override
    public String buildQuery(String username, String direction, Date from, Date to, String recipient, String sortBy, int page) {

        StringBuilder hql = new StringBuilder("where ");

        switch (direction) {
            case "incoming":
                hql.append("recipient.userName = :username and ");
                break;
            case "outgoing":
                hql.append("sender.userName = :username and ");
                hql.append(recipient.isEmpty() ? "recipient.userName like :recipientName and " : "recipient.userName = :recipientName and ");
                break;
            case "all":
                hql.append("recipient.userName = :username or sender.userName = :username and ");
                break;
            default:
                hql.append(username.isEmpty() ? "sender.userName like  :username and " : "sender.userName = :username and ");
                hql.append(recipient.isEmpty() ? "recipient.userName like :recipientName and " : "recipient.userName = :recipientName and ");
        }

        hql.append("date(time) between :startDate and :endDate order by ");


        switch (sortBy) {
            case "AmountAscending":
                hql.append("amount asc");
                break;
            case "AmountDescending":
                hql.append("amount desc");
                break;
            case "DateAscending":
                hql.append("time asc");
                break;
            default:
                hql.append("time desc");
        }
        return hql.toString();
    }
}
