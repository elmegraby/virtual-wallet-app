package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;

import java.math.BigDecimal;

public interface WalletRepository {

    public Wallet getById(int id);

    public void createWallet(User user, Wallet wallet);

    public void addFunds(Wallet wallet, BigDecimal amount);

    public void addMember(User user, Wallet wallet);

    public void removeMember(User user, Wallet wallet);

    public boolean walletExists(int id);

    void setDefault(User user, Wallet wallet);
}
