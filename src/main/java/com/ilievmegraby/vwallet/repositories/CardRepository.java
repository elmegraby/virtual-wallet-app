package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.CardDetails;

import javax.smartcardio.Card;
import java.util.List;

public interface CardRepository {

    List<CardDetails> getAll();

    CardDetails getById(int id);

    List<CardDetails> getByUsername(String username);

    CardDetails create(CardDetails cardDetails);

    CardDetails update(CardDetails cardDetails);

    void delete(CardDetails cardDetails);
}
