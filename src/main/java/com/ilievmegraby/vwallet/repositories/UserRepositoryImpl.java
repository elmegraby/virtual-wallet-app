package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.ProfileState;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final int PAGE_SIZE = 5;
    private SessionFactory sessionFactory;
    private ReferralService referralService;
    private ExchangeRateService exchangeRateService;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, ReferralService referralService, ExchangeRateService exchangeRateService) {
        this.sessionFactory = sessionFactory;
        this.referralService = referralService;
        this.exchangeRateService = exchangeRateService;
    }


    @Override
    public List<User> getAllUsers(int page) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> query = session.createQuery("from User", User.class);
            query.setFirstResult((page - 1) * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);
            session.getTransaction().commit();
            return query.list();
        }
    }

    @Override
    public User getById(int id) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = session.get(User.class, id);
            session.getTransaction().commit();

            if (user == null) {
                throw new EntityNotFoundException(String.format("User with id %d was not found.", id));
            }

            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where email=:email", User.class);
            result.setParameter("email", email);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("User with email %s was not found.", email));
            }

            return result.list().get(0);
        }
    }

    @Override
    public User getByPhone(String phone) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where phoneNumber=:phone", User.class);
            result.setParameter("phone", phone);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("User with phone %s was not found.", phone));
            }

            return result.list().get(0);
        }
    }

    @Override
    public User getByName(String username) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where userName=:username", User.class);
            result.setParameter("username", username);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("User with username %s was not found.", username));
            }

            return result.list().get(0);
        }

    }

    @Override
    public User getUser(String input) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where userName=:input OR phoneNumber=:input OR " +
                    "email=:input", User.class);
            result.setParameter("input", input);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException("No user with the given details was found.");
            }

            return result.list().get(0);
        }
    }

    @Override
    public User createUser(User newUser) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Referral referral = referralService.findByEmail(newUser.getEmail());

            long millisec = System.currentTimeMillis();
            if (referral != null && referral.getExpiryDate().compareTo(new java.util.Date(millisec)) > 0) {
                newUser.setReferred(true);
                User user = referral.getUser();
                user.getDefaultWallet().setCurrentAmount(user
                        .getDefaultWallet()
                        .getCurrentAmount()
                        .add(exchangeRateService.convert(BigDecimal.valueOf(10), CurrencyCode.USD, user.getDefaultWallet().getCurrencyCode())));

                session.update(user);
            }

            session.save(newUser);
            session.getTransaction().commit();
            return newUser;
        }
    }


    @Override
    public User updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public void deleteUser(String userName) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = getByName(userName);
            user.setEnabled(false);
            session.remove(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean userExists(String username) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where userName=:username", User.class);
            result.setParameter("username", username);
            session.getTransaction().commit();
            return !result.list().isEmpty();
        }
    }


    @Override
    public void blockUser(String userName) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = getByName(userName);
            user.setState(ProfileState.BLOCKED);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void activateAccount(String username) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user  = getByName(username);
            user.setState(ProfileState.ACTIVE);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean emailAlreadyExists(String email) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where email=:email", User.class);
            result.setParameter("email", email);
            session.getTransaction().commit();
            return !result.list().isEmpty();
        }
    }

    @Override
    public boolean phoneAlreadyExists(String phone) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> result = session.createQuery("from User where phone=:phone", User.class);
            result.setParameter("phone", phone);
            session.getTransaction().commit();
            return !result.list().isEmpty();
        }
    }

    @Override
    public int getNumberOfPages() {
        try (Session session = sessionFactory.openSession()) {
            Long count = ((Long) session.createQuery("select count(*) from User where defaultWallet is not null").uniqueResult());
            Integer totalUsers = count.intValue();
            int pages = (totalUsers + PAGE_SIZE - 1) / PAGE_SIZE;
            return pages;
        }
    }

    @Override
    public int getNumberOfPagesAdmin() {
        try (Session session = sessionFactory.openSession()) {
            Long count = ((Long) session.createQuery("select count(*) from User ").uniqueResult());
            Integer totalUsers = count.intValue();
            int pages = (totalUsers + PAGE_SIZE - 1) / PAGE_SIZE;
            return pages;
        }
    }
}
