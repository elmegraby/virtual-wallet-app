package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.Role;

public interface RoleRepository {

    public Role getByName(String roleName);
}
