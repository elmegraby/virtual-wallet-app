package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.CardDetails;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

import java.util.List;

@Repository
public class CardRepositoryImpl implements CardRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CardDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CardDetails> query = session.createQuery("from CardDetails", CardDetails.class);
            return query.list();
        }
    }

    @Override
    public CardDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CardDetails cardDetails = session.get(CardDetails.class, id);

            if (cardDetails == null) {
                throw new EntityNotFoundException((String.format(ENTITY_NOT_FOUND_BY_PROPERTY_MESSAGE, "Card", "id", id)));
            }

            return cardDetails;
        }
    }

    @Override
    public List<CardDetails> getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<CardDetails> query = session.createQuery("from CardDetails where user.userName = :username", CardDetails.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

    @Override
    public CardDetails create(CardDetails cardDetails) {

        if (cardExists(cardDetails)) {
            throw new DuplicateEntityException(String.format(DUPLICATE_ENTITY_MESSAGE, "Card"));
        }

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(cardDetails);
            session.getTransaction().commit();
            return cardDetails;
        }
    }

    @Override
    public CardDetails update(CardDetails cardDetails) {

        if (!cardExists(cardDetails)) {
            throw new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_BY_OBJECT_COMPARISON, "card"));
        }

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(cardDetails);
            session.getTransaction().commit();
            return cardDetails;
        }
    }

    @Override
    public void delete(CardDetails cardDetails) {

        if (!cardExists(cardDetails)) {
            throw new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_BY_OBJECT_COMPARISON, "card"));
        }

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Hibernate.initialize(cardDetails.getBalanceLoadSet());
            for (BalanceLoad balanceLoad : cardDetails.getBalanceLoadSet()) {
                balanceLoad.setCardDetails(null);
                session.update(balanceLoad);
            }
            session.remove(cardDetails);
            session.getTransaction().commit();
        }
    }

    private boolean cardExists(CardDetails cardDetails) {
        try (Session session = sessionFactory.openSession()) {
            Query<CardDetails> query = session.createQuery("from  CardDetails where cardNumber = :inputCardNum", CardDetails.class);
            query.setParameter("inputCardNum", cardDetails.getCardNumber());
            return query.list().size() > 0;
        }

    }
}
