package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;

import java.math.BigDecimal;

public interface ExchangeRateRepository {

    ExchangeRate getConversionRate(CurrencyCode from, CurrencyCode to);

    void updateRates(ExchangeRate ... rates);
}
