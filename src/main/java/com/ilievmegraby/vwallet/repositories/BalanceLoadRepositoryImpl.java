package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.models.*;
import com.ilievmegraby.vwallet.services.WalletService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Repository
public class BalanceLoadRepositoryImpl implements BalanceLoadRepository {
    private final int PAGE_SIZE = 5;
    private WalletService walletService;
    private SessionFactory sessionFactory;


    @Autowired
    public BalanceLoadRepositoryImpl(SessionFactory sessionFactory, WalletService walletService) {
        this.sessionFactory = sessionFactory;
        this.walletService = walletService;
    }

    @Override
    public List<BalanceLoad> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<BalanceLoad> query = session.createQuery("from BalanceLoad", BalanceLoad.class);
            return query.list();
        }
    }

    @Override
    public List<BalanceLoad> filter(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy, int page) {
        try (Session session = sessionFactory.openSession()) {

            String orderCrit = getSortingOrder(orderBy);
            Query<BalanceLoad> query = session.createQuery(buildQuery(username, from, to, cardUsed, toWallet, orderBy) + orderCrit, BalanceLoad.class);

            query.setParameter("username", username.isEmpty() ? "%" + username + "%" : username);

            if (!cardUsed.isEmpty()) {
                query.setParameter("fromCard", cardUsed);
            }
            query.setParameter("toWallet", toWallet.isEmpty() ? "%" + toWallet + "%" : toWallet);
            query.setParameter("startDate", from);
            query.setParameter("endDate", to);

            query.setFirstResult((page - 1) * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);

            List<BalanceLoad> result = query.list();
            return result;
        }
    }

    @Override
    public int getNumberOfPages(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy, int page) {
        try (Session session = sessionFactory.openSession()) {
            String orderCrit = getSortingOrder(orderBy);
            Query query = session.createQuery("select count(*) " + buildQuery(username, from,to,cardUsed,toWallet,orderBy) + orderCrit);
            query.setParameter("username", username.isEmpty() ? "%" + username + "%" : username);

            if (!cardUsed.isEmpty()) {
                query.setParameter("fromCard", cardUsed);
            }

            query.setParameter("toWallet", toWallet.isEmpty() ? "%" + toWallet + "%" : toWallet);
            query.setParameter("startDate", from);
            query.setParameter("endDate", to);
            Long count = ((Long) query.uniqueResult());
            Integer totalTransactions = count.intValue();
            int pages = (totalTransactions + PAGE_SIZE - 1) / PAGE_SIZE;

            if (pages > 5) {
                pages = 5;
            }

            return pages;
        }
    }

    @Override
    public List<BalanceLoad> getByUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<BalanceLoad> query = session.createQuery("from BalanceLoad where user.id= :userId", BalanceLoad.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public List<BalanceLoad> getByWallet(int walletId) {
        try (Session session = sessionFactory.openSession()) {
            Query<BalanceLoad> query = session.createQuery("from BalanceLoad where user.id = :walletId", BalanceLoad.class);
            query.setParameter("walletId", walletId);
            return query.list();
        }
    }

    @Override
    public List<BalanceLoad> getByCard(String cardNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<BalanceLoad> query = session.createQuery("from BalanceLoad where cardDetails.cardNumber = :cardNumber", BalanceLoad.class);
            query.setParameter("cardNumber", cardNumber);
            return query.list();
        }
    }

    @Override
    public BalanceLoad create(BalanceLoad balanceLoad) {
        if (balanceLoadExists(balanceLoad)) {
            throw new DuplicateEntityException("Duplicate payment!");
        }

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            BigDecimal payment = new BigDecimal(balanceLoad.getAmount().longValue()).movePointLeft(2);
            walletService.addFunds(balanceLoad.getWallet(), payment);
            session.save(balanceLoad);
            session.getTransaction().commit();
            return balanceLoad;
        }
    }

    @Override
    public boolean balanceLoadExists(BalanceLoad balanceLoad) {
        try (Session session = sessionFactory.openSession()) {
            Query<BalanceLoad> query = session
                    .createQuery
                            ("from BalanceLoad where cardDetails.cardNumber = :cardNumber" +
                                    " and wallet.id = :walletId and loadTime = :time", BalanceLoad.class);
            query.setParameter("cardNumber", balanceLoad.getCardDetails().getCardNumber());
            query.setParameter("walletId", balanceLoad.getWallet().getId());
            query.setParameter("time", balanceLoad.getLoadTime());

            return query.list().size() > 0;
        }
    }

    @Override
    public String buildQuery(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy) {
        StringBuilder query = new StringBuilder("from BalanceLoad where user.userName ");

        query.append(username.isEmpty() ? "like " : "= ");
        query.append(":username and wallet.name ");
        query.append(toWallet.isEmpty() ? "like :toWallet " : "= :toWallet");
        if (!cardUsed.isEmpty()) {
            query.append(" and cardDetails.cardNumber = :fromCard");
        }
        query.append(" and date(loadTime) between :startDate and :endDate order by ");
        return query.toString();
    }


    private String getSortingOrder(String orderBy) {
        switch (orderBy) {
            case "amntAsc":
                return "amount asc";
            case "amntDesc":
                return "amount desc";
            case "dateAsc":
                return "loadTime asc";
            default:
                return "loadTime desc";
        }
    }
}
