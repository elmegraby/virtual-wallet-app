package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.enums.TransactionDirection;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface TransactionRepository {

    List<Transaction> getAll();

    Transaction getById(int id);

    List<Transaction> getUserTransactions(String username);

    List<Transaction> filter(String sender, String direction, Date from, Date to, String sortBy, String recipient, int page);

    Transaction create(Transaction transaction);

    public void processTransaction(Transaction transaction);

    void delete(Transaction transaction);

    int getNumberOfPages(String sender, String direction, Date from, Date to, String sortBy, String recipient, int page);

    String buildQuery(String sender, String direction, Date from, Date to, String sortBy, String recipient, int page);

}
