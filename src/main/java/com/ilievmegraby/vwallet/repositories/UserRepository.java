package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;

import java.util.List;

public interface UserRepository {

     int getNumberOfPages();

    int getNumberOfPagesAdmin();

     List<User> getAllUsers(int page);

     User getById(int id);

     User getByName(String userName);

    User getByEmail(String email);

    User getByPhone(String phone);

     User getUser(String input);

    User createUser(User user);

    User updateUser(User user);

    void deleteUser(String userName);

    boolean userExists(String username);

    void blockUser(String userName);

    void activateAccount(String username);

    boolean phoneAlreadyExists(String phone);

     boolean emailAlreadyExists(String email);
}
