package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.CurrencyConversionException;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Repository
public class ExchangeRateRepositoryImpl implements ExchangeRateRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public ExchangeRateRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public ExchangeRate getConversionRate(CurrencyCode from, CurrencyCode to) {
        try (Session session = sessionFactory.openSession()) {
            Query<ExchangeRate> query = session.createQuery("from ExchangeRate where source = :sourceCode and target = :targetCode", ExchangeRate.class);
            query.setParameter("sourceCode", from);
            query.setParameter("targetCode", to);
            List<ExchangeRate> result = query.list();

            if (result.size() == 0) {
                throw new CurrencyConversionException(String.format(CURRENCY_CONVERSION_UNSUCCESSFUL, from.toString(), to.toString()));
            }

            return query.list().get(0);
        }
    }

    @Override
    public void updateRates(ExchangeRate... rates) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (ExchangeRate rate : rates) {

                session.merge(rate);
            }
            session.getTransaction().commit();
        }
    }
}
