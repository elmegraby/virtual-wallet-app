package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public class WalletRepositoryImpl implements WalletRepository {

    private SessionFactory sessionFactory;
    private ExchangeRateService exchangeRateService;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory, ExchangeRateService exchangeRateService) {
        this.sessionFactory = sessionFactory;
        this.exchangeRateService = exchangeRateService;

    }

    @Override
    public Wallet getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Wallet wallet = session.get(Wallet.class, id);
            session.getTransaction().commit();

            if (wallet == null) {
                throw new EntityNotFoundException("Wallet was not found.");
            }
            Hibernate.initialize(wallet.getUsers());
            return wallet;
        }
    }

    @Override
    public void createWallet(User user, Wallet wallet) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(wallet);

            if (user.getWallets().isEmpty() && user.isReferred()) {
                user.setDefaultWallet(wallet);
                user.getDefaultWallet()
                        .setCurrentAmount(exchangeRateService.convert(BigDecimal.valueOf(10), CurrencyCode.USD, wallet.getCurrencyCode()));
            } else if (user.getWallets().isEmpty()) {
                user.setDefaultWallet(wallet);
            }

            user.getWallets().add(wallet);

            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addFunds(Wallet wallet, BigDecimal amount) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            wallet.addFunds(amount);
            session.update(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addMember(User user, Wallet wallet) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            wallet.getUsers().add(user);
            user.getWallets().add(wallet);

            if (user.getWallets().size() == 1) {
                user.setDefaultWallet(wallet);
            }

            session.update(user);
            session.update(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void removeMember(User user, Wallet wallet) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            wallet.getUsers().remove(user);
            session.update(wallet);
            user.getWallets().remove(wallet);

            if (user.getWallets().size() > 0) {
                for (Wallet userWallet : user.getWallets()) {
                    user.setDefaultWallet(userWallet);
                    break;
                }
            } else {
                user.setDefaultWallet(null);
            }

            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void setDefault(User user, Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setDefaultWallet(wallet);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean walletExists(int id) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Wallet wallet = session.get(Wallet.class, id);
            session.getTransaction().commit();
            return wallet != null;
        }
    }
}
