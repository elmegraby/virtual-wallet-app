package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.User;

public interface ReferralRepository {

    public Referral findByEmail(String email);

    public void createReferral(User user,String email);

    public void addFundsToReferrer(String email);
}
