package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public class ReferralRepositoryImpl implements ReferralRepository{

    private SessionFactory sessionFactory;

    @Autowired
    public ReferralRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Referral findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Referral> result = session.createQuery("from Referral where" +
                    " email=:email", Referral.class);
            result.setParameter("email", email);
            session.getTransaction().commit();

            return result.list().size() > 0 ? result.list().get(0) : null;
        }
    }

    @Override
    public void createReferral(User user, String email) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setReferralsCount(user.getReferralsCount()+1);
            session.update(user);
            Referral referral = new Referral(user, email);
            session.save(referral);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addFundsToReferrer(String email) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Referral referral = findByEmail(email);
            Wallet wallet = referral.getUser().getDefaultWallet();
            wallet.setCurrentAmount(wallet.getCurrentAmount().add(new BigDecimal(10))); // currency conversion to ba applied!
            session.getTransaction().commit();
        }
    }
}
