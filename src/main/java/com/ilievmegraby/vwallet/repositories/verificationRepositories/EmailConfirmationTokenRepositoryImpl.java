package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class EmailConfirmationTokenRepositoryImpl implements EmailConfirmationTokenRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public EmailConfirmationTokenRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public ConfirmationToken findByConfirmationToken(String confirmationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<ConfirmationToken> result = session.createQuery("from ConfirmationToken where" +
                    " confirmationToken=:confirmationToken", ConfirmationToken.class);
            result.setParameter("confirmationToken", confirmationToken);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException("Confirmation Token was not found.");
            }

            ConfirmationToken confirmationTkn = result.list().get(0);

            Long millisec = System.currentTimeMillis();
            Date date = new Date(millisec);

            if(date.compareTo(confirmationTkn.getExpiryDate()) > 0) {
                return null;
            }

            return confirmationTkn;
        }
    }

    @Override
    public void createConfirmationToken(ConfirmationToken confirmationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(confirmationToken);
            session.getTransaction().commit();
        }
    }


}
