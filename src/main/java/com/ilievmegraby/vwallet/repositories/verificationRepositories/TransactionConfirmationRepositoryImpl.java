package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class TransactionConfirmationRepositoryImpl implements TransactionConfirmationRepository{

    private SessionFactory sessionFactory;

    @Autowired
    public TransactionConfirmationRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public TransactionConfirmationCode findByCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<TransactionConfirmationCode> result = session.createQuery("from TransactionConfirmationCode where" +
                    " code=:code", TransactionConfirmationCode.class);
            result.setParameter("code", code);

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException("Confirmation code was not found.");
            }

            TransactionConfirmationCode transactionConfirmationCode = result.list().get(0);


            //Date date = new Date(millisec);

            long millisec = System.currentTimeMillis();
            if(new java.util.Date(millisec).compareTo(transactionConfirmationCode.getExpiryDate()) > 0) {
                return null;
            }

            //session.delete(transactionConfirmationCode);

            session.getTransaction().commit();
            return transactionConfirmationCode;
        }
    }

    @Override
    public void createCode(TransactionConfirmationCode transactionConfirmationCode) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(transactionConfirmationCode);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteCode(TransactionConfirmationCode transactionConfirmationCode) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(transactionConfirmationCode);
            session.getTransaction().commit();
        }
    }
}
