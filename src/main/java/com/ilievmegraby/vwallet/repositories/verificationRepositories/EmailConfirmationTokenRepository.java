package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;

public interface EmailConfirmationTokenRepository {

    public ConfirmationToken findByConfirmationToken(String confirmationToken);

    public void createConfirmationToken(ConfirmationToken confirmationToken);
}
