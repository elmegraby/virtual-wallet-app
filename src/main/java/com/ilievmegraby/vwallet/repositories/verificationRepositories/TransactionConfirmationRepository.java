package com.ilievmegraby.vwallet.repositories.verificationRepositories;

import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;

public interface TransactionConfirmationRepository {

    public TransactionConfirmationCode findByCode(String code);

    public void createCode(TransactionConfirmationCode transactionConfirmationCode);

    public void deleteCode(TransactionConfirmationCode transactionConfirmationCode);
}
