package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role getByName(String roleName) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Role> result = session.createQuery("from Role where role=:roleName", Role.class);
            result.setParameter("roleName", roleName);
            session.getTransaction().commit();

            if (result.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Role with username %s was not found.", roleName));
            }

            return result.list().get(0);
        }
    }
}
