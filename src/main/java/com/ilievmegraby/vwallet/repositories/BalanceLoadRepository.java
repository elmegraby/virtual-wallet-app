package com.ilievmegraby.vwallet.repositories;

import com.ilievmegraby.vwallet.models.BalanceLoad;

import java.util.Date;
import java.util.List;

public interface BalanceLoadRepository {
    List<BalanceLoad> getAll();

    List<BalanceLoad> filter(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy, int page);

    List<BalanceLoad> getByUser(int userId);

    List<BalanceLoad> getByWallet(int walletId);

    List<BalanceLoad> getByCard(String cardNumber);

    BalanceLoad create(BalanceLoad balanceLoad);

    boolean balanceLoadExists(BalanceLoad balanceLoad);

    int getNumberOfPages(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy, int page);

    String buildQuery(String username, Date from, Date to, String cardUsed, String toWallet, String orderBy);
}
