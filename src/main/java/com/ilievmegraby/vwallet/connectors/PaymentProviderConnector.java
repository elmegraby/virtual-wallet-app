package com.ilievmegraby.vwallet.connectors;

import com.ilievmegraby.vwallet.models.BalanceLoad;

public interface PaymentProviderConnector {
    void makePayment(BalanceLoad balanceLoad);
}
