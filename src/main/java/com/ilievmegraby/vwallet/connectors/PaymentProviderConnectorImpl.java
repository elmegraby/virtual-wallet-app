package com.ilievmegraby.vwallet.connectors;

import com.ilievmegraby.vwallet.exceptions.APIConnectionException;
import com.ilievmegraby.vwallet.exceptions.InsufficientFundsException;
import com.ilievmegraby.vwallet.models.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Component
public class PaymentProviderConnectorImpl implements PaymentProviderConnector {
    @Value("${bank.api.url}")
    private String BANK_API_URL;

    @Value("${bank.api.key}")
    private String BANK_API_KEY;


    @Override
    public void makePayment(BalanceLoad balanceLoad) throws InsufficientFundsException, APIConnectionException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-api-key", BANK_API_KEY);
        HttpEntity<BalanceLoad> entity = new HttpEntity<BalanceLoad>(balanceLoad, headers);
        Hibernate.initialize(balanceLoad.getCardDetails().getUser().getCards());

        try {
                restTemplate.exchange(BANK_API_URL,
                        HttpMethod.POST, entity, BalanceLoad.class);
        } catch (HttpClientErrorException.Forbidden ex) {
            throw new InsufficientFundsException(INSUFFICIENT_FUNDS_MESSAGE);
        } catch (HttpClientErrorException.BadRequest ex) {
            throw new APIConnectionException(BANK_REQUEST_BODY_ERROR);
        } catch (HttpClientErrorException.Unauthorized ex) {
            throw new APIConnectionException(BANK_REQUEST_WITH_INVALID_KEY);
        } catch (org.springframework.web.client.ResourceAccessException ex){
           throw new APIConnectionException(BANK_CONNECTION_NOT_POSSIBLE);
        }
    }
}
