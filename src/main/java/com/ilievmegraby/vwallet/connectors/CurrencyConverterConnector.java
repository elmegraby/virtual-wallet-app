package com.ilievmegraby.vwallet.connectors;

import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;

public interface CurrencyConverterConnector {
    ExchangeRate getLiveExchangeRate(CurrencyCode source, CurrencyCode target);
}
