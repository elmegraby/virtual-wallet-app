package com.ilievmegraby.vwallet.connectors;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ilievmegraby.vwallet.exceptions.APIConnectionException;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.stream.Collectors;

import static com.ilievmegraby.vwallet.constants.MessageConstants.CONVERSION_API_CONNECTION_ERROR;

@Component
public class CurrencyConverterConnectorImpl implements CurrencyConverterConnector {
    @Value("${currency.api.url}")
    private String API_MAIN_URL;

    @Value("${currency.api.mode}")
    private String API_URL_COMPLETER;

    @Value("${currency.api.key}")
    private String API_KEY;

    @Override
    public ExchangeRate getLiveExchangeRate(CurrencyCode from, CurrencyCode to) {
        String conversionRequest = from + "_" + to;
        BigDecimal result = new BigDecimal("0");
        String responseBody = "";

        try {
            URL url = new URL(API_MAIN_URL + conversionRequest + API_URL_COMPLETER + API_KEY);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");

            if (connection.getResponseCode() != 200) {
                throw new APIConnectionException(CONVERSION_API_CONNECTION_ERROR);
            }

            responseBody = new BufferedReader(new InputStreamReader(connection.getInputStream())).
                    lines().collect(Collectors.joining());

            JsonObject response = new Gson().fromJson(responseBody, JsonObject.class);
            BigDecimal conversionRate = new BigDecimal(response.get(conversionRequest).toString());

            ExchangeRate toReturn = new ExchangeRate(from, to, conversionRate);
            return toReturn;
        } catch (MalformedURLException ex) {
            throw new APIConnectionException(CONVERSION_API_CONNECTION_ERROR);
        } catch (IOException e) {
            throw new APIConnectionException(CONVERSION_API_CONNECTION_ERROR);
        }
    }
}
