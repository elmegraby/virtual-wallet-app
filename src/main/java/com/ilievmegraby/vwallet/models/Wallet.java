package com.ilievmegraby.vwallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.WalletType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = "wallets")
@Getter
@Setter
@NoArgsConstructor
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wallet_id")
    private int id;

    @PositiveOrZero
    @Column(name = "current_amount")
    private BigDecimal currentAmount;

    @JsonIgnore
    @ManyToMany(mappedBy = "wallets")
    private Set<User> users = new HashSet<>();

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @Column
    @Enumerated(EnumType.STRING)
    WalletType walletType;

    @Column
    @Enumerated(EnumType.STRING)
    CurrencyCode currencyCode;

    @Column
    String name;


    public Wallet(String name, BigDecimal currentAmount, User creator, WalletType walletType, CurrencyCode currencyCode) {
        this.currentAmount = currentAmount;
        this.users = new HashSet<>();
        this.creator = creator;
        this.walletType = walletType;
        this.currencyCode = currencyCode;
        this.name = name;
    }

    public void addFunds(BigDecimal amount) {
        this.currentAmount = this.currentAmount.add(amount);
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Wallet input = (Wallet) obj;

        if (input.id == id || input.creator.getUserName().equals(creator.getUserName())){
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creator);
    }
}
