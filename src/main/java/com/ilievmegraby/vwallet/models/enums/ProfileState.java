package com.ilievmegraby.vwallet.models.enums;

public enum ProfileState {

    BLOCKED("Blocked"),
    ACTIVE("Active"),
    UNVERIFIED("Unverified"),
    DELETED("Deleted");

    private String state;

    private ProfileState(final String state){
        this.state = state;
    }

    @Override
    public String toString(){
        return this.state;
    }
    public String getState() {
        return this.state;
    }
}
