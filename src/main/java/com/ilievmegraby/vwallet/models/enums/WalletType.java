package com.ilievmegraby.vwallet.models.enums;

public enum WalletType {

    PERSONAL("Personal"),
    JOINT("Joint");

    private String type;

    private WalletType(final String state){
        this.type = state;
    }

    @Override
    public String toString(){
        return this.type;
    }
    public String getType() {
        return this.type;
    }
}
