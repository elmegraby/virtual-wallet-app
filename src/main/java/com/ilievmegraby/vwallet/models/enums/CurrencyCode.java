package com.ilievmegraby.vwallet.models.enums;

public enum CurrencyCode {
    BGN,
    USD,
    EUR;

    @Override
    public String toString(){
        return name().toUpperCase();
    }
}
