package com.ilievmegraby.vwallet.models.enums;

public enum TransactionDirection {
    INCOMING("Incoming"),
    OUTGOING("Outgoing");

    private String direction;

    private TransactionDirection(final String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return this.direction;
    }

    public String getDirection() {
        return this.direction;
    }
}
