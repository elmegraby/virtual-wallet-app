package com.ilievmegraby.vwallet.models;

import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "rates")

@Getter
@Setter
public class ExchangeRate {
    @EmbeddedId
    private ExchangeRateID id;

    @Column(name = "source_code")
    @Enumerated(EnumType.STRING)
    private CurrencyCode source;

    @Column(name = "target_code")
    @Enumerated(EnumType.STRING)
    private CurrencyCode target;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "last_update")
    private Timestamp updated;

    @Column(name = "uptodate")
    private boolean isUpToDate;

    public ExchangeRate(CurrencyCode source, CurrencyCode target, BigDecimal rate) {
        this.source = source;
        this.target = target;
        this.rate = rate;
        this.updated = new Timestamp(new Date().getTime());
        this.id = new ExchangeRateID(source, target);
    }

    public ExchangeRate() {

    }
}
