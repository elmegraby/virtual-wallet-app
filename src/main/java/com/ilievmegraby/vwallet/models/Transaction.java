package com.ilievmegraby.vwallet.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private int id;

    @ManyToOne()
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne()
    @JoinColumn(name = "recipient_id")
    private User recipient;

    @Positive
    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "sent_time")
    private Timestamp time;

    @Column(name = "description")
    private String description;

    @ManyToOne()
    @JoinColumn(name = "senderwallet_id")
    private Wallet walletUsed;

    @ManyToOne()
    @JoinColumn(name = "recipientwallet_id")
    private Wallet recipientWallet;

    @Column
    private boolean verified;


    public Transaction(User sender, User recipient, BigDecimal amount, Wallet walletUsed, String description, Wallet recipientWallet) {
        this.sender = sender;
        this.recipient = recipient;
        this.amount = amount;
        this.walletUsed = walletUsed;
        this.recipientWallet = recipientWallet;
        this.description = description;
        this.verified = false;
        this.time = new Timestamp(new java.util.Date().getTime()); //to be deleted or modified if needed
    }
}


