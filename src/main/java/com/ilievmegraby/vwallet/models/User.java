package com.ilievmegraby.vwallet.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilievmegraby.vwallet.models.enums.ProfileState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Blob;
import java.util.*;

@Entity
@SQLDelete(sql= "update users set state ='DELETED' where user_id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "state <> 'DELETED'")
@Table(name="users")
@Getter @Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    int id;

    //@Length(min = 6, max = 15, message = "Üsername must be between 6 and 15 characters!")
    @Column(name = "username", unique = true)
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "phone", unique = true)
    private String phoneNumber;

    @Lob
    @Column(name = "image")
    private String image;

    @Column(name = "full_name")
    String fullName;

    @Column(name = "enabled")
    private boolean enabled;

    @Column
    @Enumerated(EnumType.STRING)
    ProfileState state;

    @JsonIgnore
    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable (
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles = new ArrayList<>();

    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable (
            name = "user_wallet",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "wallet_id")
    )                                                           // add cascade type and think about the orphan removal
    private Set<Wallet> wallets = new HashSet<>();

    @OneToOne()
    @JoinColumn(name ="wallet_id")
    private Wallet defaultWallet;

    @Column(name="referrals_count")
    private int referralsCount;

    @Column(name = "referred")
    private boolean referred;

   // to be reviewed
    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER,cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CardDetails>  cards = new HashSet<>();

    public User(String userName, String password, String phoneNumber,String email, String fullName
            , String image
             , Wallet defaultWallet) {
        this.userName = userName;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.image = image;
        this.fullName = fullName;
        this.enabled = true;
        this.state = ProfileState.UNVERIFIED;
        this.roles = new ArrayList<>();
        this.wallets = new HashSet<>();
        this.referralsCount = 0;
        this.referred = false;
    }

    public User(){

    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        User input = (User) obj;

        if (input.getUserName().equals(userName) || input.getEmail().equals(email) || input.getPhoneNumber().equals(phoneNumber) ){
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, phoneNumber, email);
    }

}
