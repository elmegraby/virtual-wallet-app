package com.ilievmegraby.vwallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "loads")
@Getter @Setter
public class BalanceLoad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "load_id")
    private int id;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "card_id")
    private CardDetails cardDetails;

    @Column(name = "description")
    private String description;

    @Column(name = "idem_key")
    private String idempotencyKey;

    @Column(name = "currency")
    private CurrencyCode currency;

    @Column(name = "loadDate")
    private Timestamp loadTime;

    @Positive
    @Column(name = "amount")
    private BigInteger amount;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;


    public BalanceLoad(CardDetails cardDetails, Timestamp loadTime, BigInteger amount, CurrencyCode currency, String description, Wallet wallet, User user) {
        this.cardDetails = cardDetails;
        this.loadTime = loadTime;
        this.amount = amount;
        this.description = description;
        this.currency = currency;
        this.idempotencyKey = generateIdempotencyKey();
        this.wallet = wallet;
        this.user = user;
    }

    public BalanceLoad() {

    }

    private String generateIdempotencyKey() {
        String source = cardDetails.getCardNumber() + loadTime.toString();
        UUID uuid = UUID.nameUUIDFromBytes(source.getBytes());
        return uuid.toString();
    }
}
