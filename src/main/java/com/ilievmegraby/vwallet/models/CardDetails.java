package com.ilievmegraby.vwallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ilievmegraby.vwallet.helperClasses.CardNumberEncrypter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "card_details")
@Getter
@Setter
public class CardDetails {

    //    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "carddetails_id")
    private int id;

    @Convert(converter = CardNumberEncrypter.class)
    @Column(name = "card_num")
    @Pattern(regexp = "^([\\d]{4}-){3}[\\d]{4}$", message = "Card numbers must be in the following format: XXXX-XXXX-XXXX-XXXX")
    private String cardNumber;

    @Pattern(regexp = "^[\\d]{2}\\/[\\d]{2}$", message = "Card expiration date must include valid month and year combination, e.g. 11/20.")
    @Column(name = "exp_date")
    private String expirationDate;

    @Pattern(regexp = "^[A-Za-z\\s]{2,40}$", message = "Card holder name must be between 2 and 40 characters. Only letters and spaces allowed.")
    @Column(name = "cardholder")
    private String cardHolderName;

    @Convert(converter = CardNumberEncrypter.class)
    @Pattern(regexp = "^[\\d]{3}", message = "Card CVS must be 3-digits long.")
    @Column(name = "cvs")
    private String CVS;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "cardDetails", fetch = FetchType.EAGER)
    Set<BalanceLoad> balanceLoadSet;

    public CardDetails(String cardNumber, String expirationDate, String cardHolderName, String CVS) {
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardHolderName = cardHolderName;
        this.CVS = CVS;
        this.balanceLoadSet = new HashSet<>();
    }

    public CardDetails() {
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CardDetails input = (CardDetails) obj;

        return Objects.equals(cardNumber, input.cardNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber);
    }
}
