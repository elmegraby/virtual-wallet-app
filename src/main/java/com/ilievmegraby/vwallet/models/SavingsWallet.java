package com.ilievmegraby.vwallet.models;

import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.WalletType;
import org.springframework.scheduling.annotation.Scheduled;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
@Entity
@Table(name="savings_wallets")
public class SavingsWallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="savings_wallet_id")
    private int id;

    @Positive
    @Column(name="amount")
    private BigDecimal amount;

    @Column(name = "start_date")
    private Timestamp startDate;

    @Column(name = "end_date")
    private Timestamp endDate;

    @ManyToOne
    private User owner;

    @OneToOne(targetEntity = Wallet.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "wallet_id")
    private Wallet usedWallet;

    @Column
    CurrencyCode currencyCode;

    public SavingsWallet(BigDecimal amount, Timestamp endDate, User owner, Wallet usedWallet, CurrencyCode currencyCode){
        this.amount = amount;
        this.startDate = new Timestamp(System.currentTimeMillis());
        this.endDate = endDate;
        this.owner = owner;
        this.usedWallet = usedWallet;
        this.currencyCode = currencyCode;
    }

    public SavingsWallet(){

    }

    public int getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public User getOwner() {
        return owner;
    }

    public Wallet getUsedWallet() {
        return usedWallet;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setStartDate() {
        this.startDate = new Timestamp(System.currentTimeMillis());;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

     public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setUsedWallet(Wallet usedWallet) {
        this.usedWallet = usedWallet;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }
}
