package com.ilievmegraby.vwallet.models.dtos;

import com.ilievmegraby.vwallet.models.Wallet;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter @Setter
public class TransactionDTO {

    private int senderId ;
    private int recipientId;
    private double amount;
    private Timestamp time;
    private int walletId;
    private boolean verified;
    private String description;

    public TransactionDTO(int senderId, int recipientId, double amount, Timestamp time
            , int walletId, boolean verified, String description) {
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.amount = amount;
        this.time = new Timestamp(System.currentTimeMillis());
        this.walletId = walletId;
        this.verified = verified;
        this.description = description;
    }

    public TransactionDTO(){

    }
}
