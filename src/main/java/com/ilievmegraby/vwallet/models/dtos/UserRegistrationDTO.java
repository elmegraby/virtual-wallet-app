package com.ilievmegraby.vwallet.models.dtos;

import com.ilievmegraby.vwallet.helperClasses.customAnnotation.ValidEmail;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationDTO {

    private String fullName;

    @NotNull
    @NotEmpty
    private String userName;

    @NotNull
    @NotBlank
    private String password;

    @ValidEmail
    private String email;

    @NotNull
    @NotBlank
    private String phoneNumber;
}
