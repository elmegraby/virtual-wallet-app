package com.ilievmegraby.vwallet.models.dtos;

import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.WalletType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class WalletDTO {
    
    private User creator;

    @NotNull
    private WalletType walletType;

    @NotNull
    private CurrencyCode currencyCode;

    @NotBlank
    @NotNull
    private String name;

}
