package com.ilievmegraby.vwallet.models.dtos;
import com.ilievmegraby.vwallet.helperClasses.customAnnotation.ValidEmail;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserUpdateDTO {

    @NotNull
    @NotEmpty
    String fullName;

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;

    @NotNull
    @NotEmpty
    private String phoneNumber;

    @NotNull
    @NotEmpty
    private String newPassword;

    @NotNull
    private MultipartFile image;

    public UserUpdateDTO(String fullName,String email, String phoneNumber,MultipartFile image, String newPassword){
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.newPassword = newPassword;
    }

    public UserUpdateDTO(){

    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public MultipartFile getImage() {
        return image;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
