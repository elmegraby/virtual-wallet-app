package com.ilievmegraby.vwallet.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class BalanceLoadDTO {
    int userId;
    private int cardId;
    private int walletId;
    private String description;
    private String currency;
    private double amount;
}
