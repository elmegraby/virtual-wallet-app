package com.ilievmegraby.vwallet.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="role_id")
    private int id;

    @Column(name = "role_name")
    private String role;

    @ManyToMany(mappedBy = "roles")
    List<User> users;

    public Role(String role){
        this.role = role;
    }

    public Role(){

    }

    public String getRole() {
        return role;
    }

    public List<User> getUsers() {
        return new ArrayList<>(this.users);
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
