package com.ilievmegraby.vwallet.models;

import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
public class ExchangeRateID implements Serializable {
    private CurrencyCode source;
    private CurrencyCode target;

    public ExchangeRateID(CurrencyCode source, CurrencyCode target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        ExchangeRateID comparingWith = (ExchangeRateID) object;

        if (!source.equals(((ExchangeRateID) object).source)) return false;
        return target.equals(((ExchangeRateID) object).target);
    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + target.hashCode();
        return result;
    }


}
