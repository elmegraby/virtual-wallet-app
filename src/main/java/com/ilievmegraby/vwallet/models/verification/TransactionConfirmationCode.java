package com.ilievmegraby.vwallet.models.verification;

import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "confirmation_code")
public class TransactionConfirmationCode{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="code_id")
    private int codeId;

    @Column(name="code")
    private String code;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;

    @OneToOne(targetEntity = Transaction.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "transaction_id")
    private Transaction transaction;

    public TransactionConfirmationCode(Transaction transaction) {
        this.transaction = transaction;
        this.createdDate = new Date();
        this.code = setCode();
        setExpiryDate();
    }

    public TransactionConfirmationCode(){

    }

    public String getCode() {

        return code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public String setCode() {

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

        StringBuilder stringBuilder = new StringBuilder(6);

        for (int i = 0; i < 6; i++) {

            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            stringBuilder.append(AlphaNumericString
                    .charAt(index));
        }

        return stringBuilder.toString();
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public void setExpiryDate() {
        long millisec = System.currentTimeMillis();
        Date date = new Date(millisec);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        this.expiryDate = c.getTime();
    }
}
