package com.ilievmegraby.vwallet.helperClasses;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.ImageReadException;
import com.ilievmegraby.vwallet.models.*;
import com.ilievmegraby.vwallet.models.dtos.*;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.ProfileState;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import com.ilievmegraby.vwallet.services.CardService;
import com.ilievmegraby.vwallet.services.RoleService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Component
public class EntityMapper {

    private PasswordEncoder passwordEncoder;
    private RoleService roleService;
    private CardService cardService;
    private UserService userService;
    private WalletService walletService;


    @Autowired
    public EntityMapper(PasswordEncoder passwordEncoder
            , RoleService roleService
            , CardService cardService
            , UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.cardService = cardService;
        this.userService= userService;

    }

    @Autowired
    private void setWalletService(WalletService walletService) {
        this.walletService = walletService;
    }

    public User getUserFromUpdateDTO(User user, UserUpdateDTO updateDTO) {
        if (updateDTO.getFullName() != null && !updateDTO.getFullName().isEmpty()) {
            user.setFullName(updateDTO.getFullName());
        }
        if (updateDTO.getEmail() != null && !updateDTO.getEmail().isEmpty()) {
            user.setEmail(updateDTO.getEmail());
        }

        if (updateDTO.getPhoneNumber() != null && !updateDTO.getPhoneNumber().isEmpty()) {
            user.setPhoneNumber(updateDTO.getPhoneNumber());
        }

        if (updateDTO.getImage() != null) {
            try {
                user.setImage(Base64.getEncoder().encodeToString(updateDTO.getImage().getBytes()));
            } catch (IOException ex) {
                throw new ImageReadException(IMAGE_READ_ERROR);
            }
        }

        if(updateDTO.getNewPassword()!=null) {
            user.setPassword(passwordEncoder.encode(updateDTO.getNewPassword()));
        }

        return user;
    }

    public User getUserFromRegistrationDTO(UserRegistrationDTO registrationDTO) {
        User user = new User();
        user.setFullName(registrationDTO.getFullName());
        user.setUserName(registrationDTO.getUserName());
        user.setPassword(passwordEncoder.encode(registrationDTO.getPassword()));
        user.setEmail(registrationDTO.getEmail());
        user.setPhoneNumber(registrationDTO.getPhoneNumber());
        user.setState(ProfileState.UNVERIFIED);
        Role role = roleService.getByName("ROLE_USER");
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        user.setEnabled(true);
        user.setReferralsCount(0);
        return user;
    }

    public Wallet getWalletFromDTO(WalletDTO walletDTO) {
        Wallet wallet = new Wallet();
        wallet.setCurrentAmount(new BigDecimal(0));
        wallet.setUsers(new HashSet<>());
        wallet.setCreator(walletDTO.getCreator());
        wallet.setCurrencyCode(walletDTO.getCurrencyCode());
        wallet.setWalletType(walletDTO.getWalletType());
        wallet.setName(walletDTO.getName());
        return wallet;
    }

    public BalanceLoad getBalanceLoadFromDTO(BalanceLoadDTO balanceLoadDTO) {
        User user = userService.getById(balanceLoadDTO.getUserId());
        BigInteger amount = BigInteger.valueOf(Math.round(balanceLoadDTO.getAmount() * 100));
        String description = balanceLoadDTO.getDescription();
        CardDetails cardDetails = cardService.getById(user, balanceLoadDTO.getCardId());
        Wallet wallet = walletService.getById(balanceLoadDTO.getWalletId());
        CurrencyCode currencyCode = wallet.getCurrencyCode();
        Timestamp createdOn = new Timestamp(new Date().getTime());
        BalanceLoad balanceLoad = new BalanceLoad(cardDetails, createdOn, amount, currencyCode, description, wallet, user);
        return balanceLoad;
    }

    public Transaction getTransactionFromDTO(TransactionDTO transactionDTO){
        Transaction transaction = new Transaction();
        transaction.setSender(userService.getById(transactionDTO.getSenderId()));
        transaction.setDescription(transactionDTO.getDescription());
        User recipient = userService.getById(transactionDTO.getRecipientId());
        transaction.setRecipient(recipient);
        transaction.setWalletUsed(walletService.getById(transactionDTO.getWalletId()));
        transaction.setRecipientWallet(recipient.getDefaultWallet());
        transaction.setAmount(BigDecimal.valueOf(transactionDTO.getAmount()));
        transaction.setTime(new Timestamp(System.currentTimeMillis()));
        transaction.setVerified(false);
        return transaction;
    }
}
