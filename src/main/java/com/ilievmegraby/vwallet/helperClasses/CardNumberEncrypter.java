package com.ilievmegraby.vwallet.helperClasses;

import com.ilievmegraby.vwallet.exceptions.EncryptionException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Base64;


@Converter
public class CardNumberEncrypter implements AttributeConverter<String, String> {
    private final String encryptionKey = "secretKey";
    private final byte[] keyBytes = Arrays.copyOf(encryptionKey.getBytes(), 16);


    @Override
    public String convertToDatabaseColumn(String field) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(field.getBytes("UTF-8")));
        } catch (Exception ex) {
           throw new EncryptionException(ex.getMessage());
        }
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(dbData)));
        } catch (Exception ex) {
            throw new EncryptionException(ex.getMessage());
        }
    }
}
