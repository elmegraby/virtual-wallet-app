package com.ilievmegraby.vwallet.helperClasses;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    public static Date[] getDateRangeFromString(String daterange) {
        String[] dates = daterange.split("\\s-\\s");
        Date fromDate;
        Date toDate;
        try {
            fromDate = new SimpleDateFormat("MM/dd/yyyy").parse(dates[0]);
            toDate = new SimpleDateFormat("MM/dd/yyyy").parse(dates[1]);
        } catch (ParseException e) {
            fromDate = new Date(0);
            toDate = new Date();
        }

        return new Date[]{fromDate, toDate};
    }

    public static String getDateFromTimestamp(Timestamp timestamp) {
        DateFormat template = new SimpleDateFormat("dd-M-yyyy HH:mm:ss");
        Date date = new Date(timestamp.getTime());
        return template.format(date) + " h";
    }
}
