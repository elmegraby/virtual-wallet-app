package com.ilievmegraby.vwallet.helperClasses;


import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Component
public class EmailFactory {
    @Value("${spring.mail.username}")
    private String senderEmail;

    public SimpleMailMessage getRegistrationEmail(String email, ConfirmationToken confirmationToken){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(MAIL_COMPLETE_REGISTRATION_SUBJECT);
        mailMessage.setFrom(senderEmail);
        mailMessage.setText(MAIL_COMPLETE_REGISTRATION_BODY+confirmationToken.getConfirmationToken());
        return mailMessage;
    }

    public SimpleMailMessage getRestRegistrationEmail(String email, ConfirmationToken confirmationToken){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(MAIL_COMPLETE_REGISTRATION_SUBJECT);
        mailMessage.setFrom(senderEmail);
        mailMessage.setText(MAIL_COMPLETE_REST_REGISTRATION_BODY + confirmationToken.getConfirmationToken());
        return mailMessage;
    }

    public SimpleMailMessage getTransactionEmail(String email, TransactionConfirmationCode transactionConfirmationCode){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(MAIL_VERIFY_TRANSACTION_SUBJECT);
        mailMessage.setFrom(senderEmail);
        mailMessage.setText(String.format(MAIL_VERIFY_TRANSACTION_BODY,transactionConfirmationCode.getCode()));
        return mailMessage;
    }

    public SimpleMailMessage getReferralEmail(String email){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(MAIL_REFERRAL_SUBJECT);
        mailMessage.setFrom(senderEmail);
        mailMessage.setText(MAIL_REFERRAL_BODY);
        return mailMessage;
    }

    public SimpleMailMessage getWalletInviteEmail(String invitedBy, String walletName, String email){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(MAIL_WALLET_INVITEE_SUBJECT);
        mailMessage.setFrom(senderEmail);
        mailMessage.setText(String.format(MAIL_WALLET_INVITEE_BODY, invitedBy, walletName));
        return mailMessage;
    }

}
