package com.ilievmegraby.vwallet.helperClasses;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;


public class Utilities {

    public static String formatBigDecimal(BigDecimal rate){
        rate = rate.setScale(3, BigDecimal.ROUND_DOWN);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(3);
        df.setMinimumFractionDigits(0);
        df.setGroupingUsed(false);
        return df.format(rate);
    }
}
