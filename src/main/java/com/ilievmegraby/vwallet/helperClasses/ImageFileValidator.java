package com.ilievmegraby.vwallet.helperClasses;

import com.ilievmegraby.vwallet.exceptions.ImageReadException;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ImageFileValidator{
    private static final long FIVE_MB_TO_BYTES = 5242880;

    public ImageFileValidator(){

    }

    public boolean isValid(MultipartFile file) {
        String fileType = file.getContentType();

        if (file.getSize() > FIVE_MB_TO_BYTES) {
            return false;
        }

        return fileType.equalsIgnoreCase("image/jpg") ||
                fileType.equalsIgnoreCase("image/png") ||
                fileType.equalsIgnoreCase("image/jpeg");
    }

}
