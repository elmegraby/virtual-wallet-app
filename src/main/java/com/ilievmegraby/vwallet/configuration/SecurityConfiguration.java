package com.ilievmegraby.vwallet.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    String databaseUrl, databaseUsername, databasePassword;
    private DataSource dataSource;

    @Autowired
    public SecurityConfiguration(DataSource dataSource, Environment environment) {
        this.dataSource = dataSource;
        this.databaseUrl = environment.getProperty("database.url");
        this.databaseUsername = environment.getProperty("database.username");
        this.databasePassword = environment.getProperty("database.password");
    }

    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.jdbcAuthentication().dataSource(this.dataSource)
                .usersByUsernameQuery("select username,password,enabled from users where username=?")
                .authoritiesByUsernameQuery("SELECT users.username as username, roles.role_name" +
                        " as role FROM users " +
                        "INNER JOIN user_role ON users.user_id = user_role.user_id " +
                        "INNER JOIN roles ON user_role.role_id = roles.role_id WHERE users.username = ? ");
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/api/v1/exchange/**").permitAll()
                .antMatchers("/api/**").authenticated()
//                .and().httpBasic();
                .antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/about").permitAll()
                .antMatchers("/topup-history/**").authenticated()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/transactions/**").authenticated()
                .antMatchers("/payment-tools/**").authenticated()
                .antMatchers("/refer").permitAll()
                .antMatchers("/profile/**").authenticated()
                .and().formLogin()
              .loginPage("/login").loginProcessingUrl("/authenticate").permitAll().
                defaultSuccessUrl("/payment-tools").
                and().logout().logoutSuccessUrl("/").permitAll();

    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }
}
