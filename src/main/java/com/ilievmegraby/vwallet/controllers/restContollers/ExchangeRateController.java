package com.ilievmegraby.vwallet.controllers.restContollers;

import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

@RestController
@RequestMapping("/api/v1/exchange")
public class ExchangeRateController {
    private ExchangeRateService exchangeRateService;

    @Autowired
    public ExchangeRateController(ExchangeRateService exchangeRateService, UserService userService, WalletService walletService) {
        this.exchangeRateService = exchangeRateService;
    }

    @GetMapping
    public ExchangeRate getExchangeRate(@RequestParam(name = "source") CurrencyCode from, @RequestParam(name = "target") CurrencyCode to) {
        ExchangeRate returning = exchangeRateService.getExchangeRate(from, to);
        return returning;
    }

    @GetMapping("/convert")
    public BigDecimal getConvertedSum(@RequestParam CurrencyCode source
            , @RequestParam CurrencyCode target, @RequestParam BigDecimal amount){
        return exchangeRateService.convert(amount, source, target);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<String> getTransactionRateMessage(@RequestBody TransactionDTO transactionDTO) {
        return exchangeRateService.getExchangeRateInfoMessage(transactionDTO);
    }
}
