package com.ilievmegraby.vwallet.controllers.restContollers;

import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.services.CardService;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/v1/cards")
public class CardController {
    private CardService cardService;
    private UserService userService;

    @Autowired
    public CardController(CardService cardService, UserService userService) {
        this.userService = userService;
        this.cardService = cardService;
    }


    @GetMapping("/getById/{id}")
    public CardDetails getById(@PathVariable int id, Principal principal) {
        try {
            User user = userService.getByName(principal.getName());
            return cardService.getById(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedActionException ex){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }


    @PostMapping("/new")
    public CardDetails create(@RequestBody @Valid CardDetails cardDetails) {
        try {
            return cardService.create(cardDetails);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/update")
    public CardDetails update(@RequestBody CardDetails card, Principal principal) {
        try {
            User user = userService.getByName(principal.getName());
            return cardService.update(user, card);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedActionException ex){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody int id, Principal principal) {
        try {
            User user = userService.getByName(principal.getName());
            cardService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedActionException ex){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }
}
