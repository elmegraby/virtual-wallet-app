package com.ilievmegraby.vwallet.controllers.restContollers;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.services.verificationServices.EmailConfirmationTokenService;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@RequestMapping("/api/v1/users")
@RestController
public class UserController {

    private UserService userService;
    private EmailConfirmationTokenService emailConfirmationTokenService;
    private EmailSenderService emailSenderService;
    private EmailFactory emailFactory;

    @Autowired
    public UserController(UserService userService, EmailConfirmationTokenService emailConfirmationTokenService
            , EmailSenderService emailSenderService, EmailFactory emailFactory) {
        this.userService = userService;
        this.emailConfirmationTokenService = emailConfirmationTokenService;
        this.emailSenderService = emailSenderService;
        this.emailFactory = emailFactory;
    }

    @GetMapping("/getById/{id}")
    public User getUserById(@PathVariable int id) {

        try {
            return userService.getById(id);
        } catch (EntityNotFoundException e) {

            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/getByUsername/{userName}")
    public User getUserByUserName(@PathVariable String userName) {

        try {
            return userService.getByName(userName);
        } catch (EntityNotFoundException e) {

            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/getByEmail/{email}")
    public User getUserByEmail(@PathVariable String email) {

        try {
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {

            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/getByPhone/{phone}")
    public User getUserByPhone(@PathVariable String phone) {

        try {
            return userService.getByPhone(phone);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/getUser/{input}")
    public User getUserByAnyInput(@PathVariable String input) {

        try {
            return userService.getUser(input);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public void createUser(@RequestBody @Valid UserRegistrationDTO userRegistrationDTO) {

        try {
            User user = userService.createUser(userRegistrationDTO);
            ConfirmationToken confirmationToken = new ConfirmationToken(user);
            emailConfirmationTokenService.createConfirmationToken(confirmationToken);
            SimpleMailMessage mailMessage = emailFactory.getRestRegistrationEmail(user.getEmail(), confirmationToken);
            emailSenderService.sendEmail(mailMessage);

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public void confirmUserAccount(@RequestParam("token") String confirmationToken) {
        ConfirmationToken token = emailConfirmationTokenService.findByConfirmationToken(confirmationToken);
        if (token != null) {
            userService.activateAccount(token.getUser().getUserName());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, INACTIVE_LINK_MESSAGE);
        }
    }


    @PutMapping("/update")
    public void updateUser(@RequestBody @Valid User user, @RequestBody @Valid UserUpdateDTO userUpdateDTO) {

        try {
            userService.updateUser(user, userUpdateDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{username}")
    public void deleteUser(@PathVariable String username) {
        try {
            userService.deleteUser(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/block/{username}")
    public void blockUser(@PathVariable String username) {
        try {
            userService.blockUser(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/unblock/{username}")
    public void unblockUser(@PathVariable String username) {
        try {
            userService.activateAccount(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
