package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

//import javax.jws.WebParam;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@ApiIgnore
@RequestMapping("/profile")
public class UserProfileController {

    private UserService userService;
    private ReferralService referralService;
    private EmailSenderService emailSenderService;
    private EmailFactory emailFactory;

    @Autowired
    public UserProfileController(UserService userService
            , ReferralService referralService
            , EmailSenderService emailSenderService
            , EmailFactory emailFactory){
        this.userService = userService;
        this.referralService = referralService;
        this.emailSenderService = emailSenderService;
        this.emailFactory = emailFactory;
    }

    @GetMapping
    public ModelAndView showProfile(ModelAndView modelAndView, Principal principal){

        if (principal != null) {
            User user = userService.getByName(principal.getName());
            modelAndView.setViewName("profile");
            modelAndView.addObject("updateDTO", new UserUpdateDTO());
            modelAndView.addObject("user", user);
            return modelAndView;
        } else {
            modelAndView.setViewName("login");
        }

        return modelAndView;
    }

    @PostMapping("/update")
    public ModelAndView updateProfile(Principal principal, ModelAndView modelAndView, UserUpdateDTO userUpdateDTO){
        User user = userService.getByName(principal.getName());
        userService.updateUser(user, userUpdateDTO);
        modelAndView.addObject("user", user);
        modelAndView.setViewName("redirect:/profile");
        return modelAndView;
    }

    @PostMapping("/refer")
    public ModelAndView refer(Principal principal, ModelAndView modelAndView,
                              @RequestParam(name = "email", required = false, defaultValue =  "") String referredEmail){
        User user = userService.getByName(principal.getName());
        referralService.createReferral(user,referredEmail);
        SimpleMailMessage mailMessage = emailFactory.getReferralEmail(referredEmail);
        emailSenderService.sendEmail(mailMessage);
        modelAndView.setViewName("referral-success");
        return modelAndView;
    }
}
