package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@Controller
@ApiIgnore
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/login")
    public String showLoginForm(Principal principal) {

        return "login";
    }
}

