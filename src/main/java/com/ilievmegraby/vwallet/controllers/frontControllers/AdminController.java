package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.TransactionService;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.model.IModel;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@ApiIgnore
@RequestMapping("/admin")
public class AdminController {

    private UserService userService;
    private TransactionService transactionService;
    private BalanceLoadService balanceLoadService;

    @Autowired
    public AdminController(UserService userService, TransactionService transactionService, BalanceLoadService balanceLoadService) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.balanceLoadService = balanceLoadService;
    }


    @GetMapping
    public ModelAndView showAdminPanel(Principal principal, ModelAndView modelAndView,  @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

        if (principal == null){ // Checking for principal == null is done, in case the session is lost.
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }

        List<User> users = userService.getAllUsers(page);
        modelAndView.setViewName("admin-users");
        modelAndView.addObject("pageCount", userService.getNumberOfPagesAdmin());
        modelAndView.addObject("users", users);
        modelAndView.addObject("user", userService.getByName(principal.getName()));
        return modelAndView;
    }

    @GetMapping("/finduser")
    public String findUser(Model model, @RequestParam(value = "user", required = false) String input, Principal principal) {

        if (principal == null){
            return "redirect:/login";
        }

        List<User> users = new ArrayList<>(); //unnecessary
        User user = userService.getUser(input);
        users.add(user);
        model.addAttribute("pageCount", 1);
        model.addAttribute("users", users);
        return "admin-users";
    }

    @GetMapping("/transactions")
    public ModelAndView showFilteredTransactions(ModelAndView modelAndView,
                                                 @RequestParam(name = "datesRange", required = false, defaultValue = "") String dateRange,
                                                 @RequestParam(name = "direction", required = false, defaultValue = "") String direction,
                                                 @RequestParam(name = "recipient", required = false, defaultValue = "") String recipient,
                                                 @RequestParam(name = "sender", required = false, defaultValue = "") String sender,
                                                 @RequestParam(name = "sortBy", required = false, defaultValue = "") String sortBy,
                                                 @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                                                 Principal principal) {

        if (principal==null){
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }

        modelAndView.addObject("datesRange", dateRange);
        modelAndView.addObject("direction", direction);
        modelAndView.addObject("recipient", recipient);
        modelAndView.addObject("sender", sender);
        modelAndView.addObject("sortBy", sortBy);
        modelAndView.addObject("pageCount", transactionService.getNumberOfPages(sender, direction, dateRange,recipient, sortBy, page));
        modelAndView.addObject("transactions", transactionService.filter(sender, direction, dateRange,recipient, sortBy ,page));
        modelAndView.addObject("user", userService.getByName(principal.getName()));
        modelAndView.setViewName("admin-transactions");
        return modelAndView;
    }


    @GetMapping("/topup-history")
    public ModelAndView showFilteredBalanceLoads(ModelAndView modelAndView,
                                                 @RequestParam(name = "datesRange", required = false, defaultValue = "") String dateRange,
                                                 @RequestParam(name = "username", required = false, defaultValue = "") String username,
                                                 @RequestParam(name = "toWallet", required = false, defaultValue = "") String toWallet,
                                                 @RequestParam(name = "cardUsed", required = false, defaultValue = "") String cardUsed,
                                                 @RequestParam(name = "orderBy", required = false, defaultValue = "") String orderBy,
                                                 @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                                                 Principal principal) {

        if (principal == null) {
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }

        User user = userService.getByName(principal.getName());
        modelAndView.addObject("balanceloads", balanceLoadService.filter(username, dateRange,cardUsed,toWallet, orderBy, page));
        modelAndView.addObject("pageCount", balanceLoadService.getNumberOfPages(username, dateRange,cardUsed,toWallet, orderBy, page));
        modelAndView.addObject("user", user);
        modelAndView.addObject("dateRange", dateRange);
        modelAndView.addObject("orderBy", orderBy);
        modelAndView.setViewName("admin-topup-history");
        return modelAndView;
    }

    @PostMapping("/block/{username}")
    public ModelAndView blockUser(ModelAndView modelAndView, @PathVariable String username) {
        userService.blockUser(username);
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

    @PostMapping("/activate/{username}")
    public ModelAndView activateUser(ModelAndView modelAndView, @PathVariable String username) {
        userService.activateAccount(username);
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

}
