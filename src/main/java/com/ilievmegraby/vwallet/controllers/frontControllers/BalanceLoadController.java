package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.Set;

@Controller
@ApiIgnore
@RequestMapping("/topup-history")
public class BalanceLoadController {
    private BalanceLoadService balanceLoadService;
    private UserService userService;

    @Autowired
    public BalanceLoadController(BalanceLoadService balanceLoadService, UserService userService) {
        this.balanceLoadService = balanceLoadService;
        this.userService = userService;
    }

    @GetMapping()
    public ModelAndView showFilteredBalanceLoads(ModelAndView modelAndView,
                                                 @RequestParam(name = "datesRange", required = false, defaultValue = "") String dateRange,
                                                 @RequestParam(name = "toWallet", required = false, defaultValue = "") String toWallet,
                                                 @RequestParam(name = "cardUsed", required = false, defaultValue = "") String cardUsed,
                                                 @RequestParam(name = "orderBy", required = false, defaultValue = "") String orderBy,
                                                 @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                                                 Principal principal) {

        if (principal == null) {
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }

        User user = userService.getByName(principal.getName());
        Set<CardDetails> cards = user.getCards();
        Set<Wallet> wallets = user.getWallets();
        modelAndView.addObject("user", user);
        modelAndView.addObject("cards", cards);
        modelAndView.addObject("wallets", wallets);
        modelAndView.addObject("datesRange", dateRange);
        modelAndView.addObject("toWallet", toWallet);
        modelAndView.addObject("cardUsed", cardUsed);
        modelAndView.addObject("orderBy", orderBy);
        modelAndView.addObject("page", page);
        modelAndView.addObject("balanceloads", balanceLoadService.filter(principal.getName(), dateRange, cardUsed,toWallet, orderBy, page));
        modelAndView.addObject("pageCount", balanceLoadService.getNumberOfPages(principal.getName(), dateRange, cardUsed,toWallet, orderBy, page));
        modelAndView.setViewName("topup-history-user");
        return modelAndView;
    }
}
