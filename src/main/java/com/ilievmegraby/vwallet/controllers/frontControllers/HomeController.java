package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@Controller
@ApiIgnore
@RequestMapping("/")
public class HomeController {

    @GetMapping
    public String returnHomepage(){
        return "index-2";
    }

}
