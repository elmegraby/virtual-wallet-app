package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.ProfileState;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import com.ilievmegraby.vwallet.services.TransactionService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.TransactionConfirmationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Controller
@ApiIgnore
@RequestMapping("/transactions")
public class TransactionController {

    private UserService userService;
    private TransactionService transactionService;
    private TransactionConfirmationService transactionConfirmationService;
    private EmailSenderService emailSenderService;
    private EmailFactory emailFactory;
    private ExchangeRateService exchangeRateService;

    @Autowired
    public TransactionController(UserService userService
            , TransactionService transactionService
            , TransactionConfirmationService transactionConfirmationService
            , EmailSenderService emailSenderService
            , EmailFactory emailFactory
            , ExchangeRateService exchangeRateService) {

        this.userService = userService;
        this.transactionService = transactionService;
        this.transactionConfirmationService = transactionConfirmationService;
        this.emailSenderService = emailSenderService;
        this.emailFactory = emailFactory;
        this.exchangeRateService = exchangeRateService;
    }


    @GetMapping()
    public ModelAndView showFilteredTransactions(ModelAndView modelAndView,
                                                 @RequestParam(name = "datesRange", required = false, defaultValue = "") String dateRange,
                                                 @RequestParam(name = "direction", required = false, defaultValue = "all") String direction,
                                                 @RequestParam(name = "recipient", required = false, defaultValue = "") String recipient,
                                                 @RequestParam(name = "sortBy", required = false, defaultValue = "") String sortBy,
                                                 @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                                                 Principal principal) {

        if (principal == null) {
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }

        User user = userService.getByName(principal.getName());
        modelAndView.addObject("datesRange", dateRange);
        modelAndView.addObject("direction", direction);
        modelAndView.addObject("recipient", recipient);
        modelAndView.addObject("sortBy", sortBy);
        modelAndView.addObject("pageCount", transactionService.getNumberOfPages(principal.getName(), direction, dateRange, recipient, sortBy, page));
        modelAndView.addObject("transactions", transactionService.filter(principal.getName(), direction, dateRange, recipient, sortBy, page));
        modelAndView.addObject("user", user);
        modelAndView.setViewName("transaction-history");
        return modelAndView;
    }


    @GetMapping("/new")
    public String showSendTransaction(@ModelAttribute(name = "transaction") TransactionDTO transactionDTO,
                                      @ModelAttribute(name = "wallet") WalletDTO walletDTO,
                                      @RequestParam(required = false, defaultValue = "") String username,
                                      @RequestParam(required = false, defaultValue = "1") int page, Model model, Principal principal) {

        if (principal == null) {
            return "redirect:/login";
        }

        User user = userService.getByName(principal.getName());

        if (user.getDefaultWallet() == null) {
            model.addAttribute("message", TRANSACTIONS_SENDER_NO_WALLET);
            return "after-reg-setup-wallet";
        }

        if (user.getState() == ProfileState.BLOCKED || user.getState() == ProfileState.UNVERIFIED) {
            return "account-blocked";
        }

        List<User> users = new ArrayList<>();
        if (username.isEmpty()) {
            users = userService.getAllUsers(page);
        } else {
            users = Arrays.asList(userService.getByName(username));
        }
        model.addAttribute("user", user);
        model.addAttribute("users", users);
        model.addAttribute("pageCount", userService.getNumberOfPages());
        return "create-transaction";
    }

    @GetMapping("/apply-code")
    public ModelAndView showVerificationForm(ModelAndView modelAndView) {
        modelAndView.setViewName("apply-code");
        return modelAndView;
    }


    /*@GetMapping("/finduser")
    public String findUser(Model model, @ModelAttribute(name = "transaction") TransactionDTO transactionDTO, @RequestParam(value = "user", required = false) String input, Principal principal) {
        List<User> users = new ArrayList<>();
        User user = userService.getUser(input);
        users.add(user);
        User loggedUser = userService.getByName(principal.getName());
        model.addAttribute("user", loggedUser);
        model.addAttribute("users", users);
        return "create-transaction";
    }

     */

    @PostMapping("/new")
    public ModelAndView newTransaction(ModelAndView modelAndView, Principal principal, TransactionDTO transactionDTO) {


        User user = userService.getByName(principal.getName());
        transactionDTO.setSenderId(user.getId());

        Transaction transaction = transactionService.create(transactionDTO);

        if (transaction.getAmount().compareTo(exchangeRateService.convert(BigDecimal.valueOf(1000), CurrencyCode.USD, transaction.getWalletUsed().getCurrencyCode())) > 0) {
            TransactionConfirmationCode transactionConfirmationCode = new TransactionConfirmationCode(transaction);
            transactionConfirmationService.createCode(transactionConfirmationCode);
            SimpleMailMessage mailMessage = emailFactory.getTransactionEmail(user.getEmail(), transactionConfirmationCode);
            emailSenderService.sendEmail(mailMessage);
            modelAndView.addObject("email", user.getEmail());
            modelAndView.setViewName("verify-transaction");
            return modelAndView;
        } else {
            transactionService.processTransaction(transaction);
            modelAndView.setViewName("transaction-success");
        }

        return modelAndView;
    }

    @PostMapping("/verify-transaction")
    public ModelAndView verifyTransaction(ModelAndView modelAndView, @RequestParam("code") String verificationCode) {
        TransactionConfirmationCode transactionConfirmationCode = transactionConfirmationService.findByCode(verificationCode);

        if (transactionConfirmationCode != null) {
            transactionService.processTransaction(transactionConfirmationCode.getTransaction());
            transactionConfirmationService.deleteCode(transactionConfirmationCode);
            modelAndView.setViewName("transaction-success");
        } else {
            modelAndView.addObject("message", "Verification code is either broken or expired!");
            modelAndView.setViewName("error");
        }

        return modelAndView;
    }
}
