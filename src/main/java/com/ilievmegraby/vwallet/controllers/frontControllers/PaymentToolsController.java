package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.BalanceLoadDTO;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.CardService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Set;

import static com.ilievmegraby.vwallet.constants.MessageConstants.WALLET_UNAUTHORIZED_ACTION;

@Controller
@ApiIgnore
@RequestMapping("/payment-tools")
public class PaymentToolsController {
    private CardService cardService;
    private UserService userService;
    private WalletService walletService;
    private BalanceLoadService balanceLoadService;

    @Autowired
    public PaymentToolsController(CardService cardService, UserService userService, WalletService walletService,
                                  BalanceLoadService balanceLoadService) {
        this.cardService = cardService;
        this.userService = userService;
        this.walletService = walletService;
        this.balanceLoadService = balanceLoadService;
    }


    @GetMapping()
    public String getHome(@ModelAttribute(name = "card") CardDetails cardDetails, @ModelAttribute(name = "wallet") Wallet wallet,
                          @ModelAttribute(name = "balanceload") BalanceLoadDTO balanceLoad, Model model, Principal principal) {

        if (principal == null) {
            return "redirect:/login";
        }
        User user = userService.getByName(principal.getName());
        List<CardDetails> cards = cardService.getByUsername(user, user.getUserName());
        Set<Wallet> wallets = user.getWallets();
        model.addAttribute("cards", cards);
        model.addAttribute("wallets", wallets);
        model.addAttribute("user", user);
        return "cards-wallets";
    }


    @PostMapping("/newcard")
    public String createCard(@Valid CardDetails cardDetails, BindingResult bindingResult, Principal principal) {

        if (principal == null) {
            return "redirect:/login";
        }


        if (bindingResult.hasErrors()) {
            return "redirect:/error";
        }

        User creator = userService.getByName(principal.getName());
        cardDetails.setUser(creator);
        cardService.create(cardDetails);
        return "redirect:/payment-tools";
    }

    @GetMapping("/card-update/{id}")
    public String getCardUpdateForm(@PathVariable int id, Model model, Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        }
        CardDetails card = cardService.getById(userService.getByName(principal.getName()),id);
        model.addAttribute("card", card);

        if (!card.getUser().getUserName().equals(principal.getName())) {
            return "error";
        }
        return "edit-card";

    }

    @PostMapping("/card-update")
    public String updateCard(@Valid CardDetails cardDetails, BindingResult bindingResult, Principal principal, Model model) {
        if (bindingResult.hasErrors()) {
            return "error";
        }
        User user = userService.getByName(principal.getName());
        cardService.update(user, cardDetails);
        return "redirect:/payment-tools";
    }

    @PostMapping("/card-delete")
    public String deleteCard(@RequestParam int id, Principal principal) {

        if (principal==null){
            return "redirect:/login";
        }
        User user = userService.getByName(principal.getName());
        cardService.delete(id, user);

        return "redirect:/payment-tools";
    }

    @PostMapping("/newwallet")
    public String createWallet(@Valid WalletDTO walletDTO, BindingResult bindingResult, Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        }
        User user = userService.getByName(principal.getName());
        walletDTO.setCreator(user);

        if (bindingResult.hasErrors()) {
            return "error";
        }
        walletService.createWallet(user, walletDTO);
        return "redirect:/payment-tools";
    }

    @PostMapping("/topup")
    private String topUpWallet(@Valid BalanceLoadDTO balanceLoad, BindingResult bindingResult, Principal principal) {

        if (principal == null) {
            return "redirect:/login";
        }
        if (bindingResult.hasErrors()) {
            return "error";
        }
        balanceLoadService.create(balanceLoad);
        return "redirect:/payment-tools";
    }

    @PostMapping("/setdefault")
    private String setDefaultWallet(@RequestParam int walletId, Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        }
        Wallet wallet = walletService.getById(walletId);
        User user = userService.getByName(principal.getName());
        walletService.setDefault(user, wallet);
        return "redirect:/payment-tools";
    }


    @GetMapping("/manage/{id}")
    public String manageWallet(@PathVariable int id, Principal principal, Model model) {
        Wallet toBeManaged = walletService.getById(id);
        if (principal == null) {
            return "redirect:/login";
        }

        if (!principal.getName().equals(toBeManaged.getCreator().getUserName())){
        model.addAttribute("message", WALLET_UNAUTHORIZED_ACTION);
        return "error";
        }
        Set<User> users = toBeManaged.getUsers();
        User admin = userService.getByName(principal.getName());
        model.addAttribute("wallet", toBeManaged);
        model.addAttribute("users", users);
        model.addAttribute("admin", admin);
        return "wallet-panel";
    }

    @PostMapping("/manage/invite")
    public String addMemberToWallet(@RequestParam int walletId, @RequestParam String user, Principal principal) {

        if (principal == null) {
            return "redirect:/login";
        }
        Wallet wallet = walletService.getById(walletId);
        User toAdd = userService.getUser(user);
        walletService.addMember(toAdd, wallet);
        return "redirect:/payment-tools/manage/" + walletId;
    }

    @PostMapping("/manage/remove")
    public String removeMemberFromWallet(@RequestParam int walletId, @RequestParam String user, Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        }
        Wallet wallet = walletService.getById(walletId);
        User toRemove = userService.getUser(user);
        walletService.removeMember(toRemove, wallet);
        return "redirect:/payment-tools/manage/" + walletId;
    }
}
