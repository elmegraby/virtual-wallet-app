package com.ilievmegraby.vwallet.controllers.frontControllers;

import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.services.verificationServices.EmailConfirmationTokenService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

import javax.smartcardio.Card;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@ApiIgnore
@RequestMapping("/registration")
public class RegistrationController {
    private UserService userService;
    private EmailConfirmationTokenService emailConfirmationTokenService;
    private EmailSenderService emailSenderService;
    private EmailFactory emailFactory;

    @Autowired
    public RegistrationController(UserService userService,EmailConfirmationTokenService emailConfirmationTokenService,
                                  EmailSenderService emailSenderService, EmailFactory emailFactory) {
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.emailConfirmationTokenService = emailConfirmationTokenService;
        this.emailFactory = emailFactory;
    }

    @GetMapping
    public ModelAndView displayRegistration(ModelAndView modelAndView, UserRegistrationDTO userDTO)
    {
        modelAndView.addObject("userDTO", userDTO);
        modelAndView.setViewName("signup");
        return modelAndView;
    }

    @PostMapping
    public ModelAndView registerUser(ModelAndView modelAndView, UserRegistrationDTO userRegistrationDTO)
    {
            User user = userService.createUser(userRegistrationDTO);
            ConfirmationToken confirmationToken = new ConfirmationToken(user);
            emailConfirmationTokenService.createConfirmationToken(confirmationToken);
            SimpleMailMessage mailMessage = emailFactory.getRegistrationEmail(user.getEmail(), confirmationToken);
            emailSenderService.sendEmail(mailMessage);
            modelAndView.addObject("email", user.getEmail());
            modelAndView.setViewName("verify-registration");

        return modelAndView;
    }

    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken)
    {
        ConfirmationToken token = emailConfirmationTokenService.findByConfirmationToken(confirmationToken);

        if(token != null) {
            User user = userService.getByEmail(token.getUser().getEmail());
            userService.activateAccount(user.getUserName());
            modelAndView.setViewName("registration-verified");
        }
        else {
            modelAndView.addObject("message","The link is expired or broken!");
            modelAndView.setViewName("error");
        }

        return modelAndView;
    }

    @GetMapping("/setup-wallet")
    public String setUpAccountDefaultWallet(@ModelAttribute(name="wallet") Wallet wallet, BindingResult bindingResult, Principal principal){
        if (bindingResult.hasErrors()){
            return "error";
        }

        return "after-reg-setup-wallet";
    }
}
