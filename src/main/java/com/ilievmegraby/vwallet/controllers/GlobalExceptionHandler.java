package com.ilievmegraby.vwallet.controllers;

import com.ilievmegraby.vwallet.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({EntityNotFoundException.class
            , CurrencyConversionException.class, DuplicateEntityException.class, ExceededReferralsException.class
            , ImageReadException.class, InvalidTypeException.class})
    public String handleDuplicateErrors(Exception exception, Model model) {

        model.addAttribute("message", exception.getMessage() != null
                ? exception.getMessage() : exception.getLocalizedMessage());
        return "error";
    }

    @ExceptionHandler({CardDeniedException.class,
            InsufficientFundsException.class,})
    public String handleCardErrors(Exception exception, Model model) {
        model.addAttribute("message", exception.getMessage() != null
                ? exception.getMessage() : exception.getLocalizedMessage());
        return "surprise";
    }

    @ExceptionHandler({APIConnectionException.class})
    public String handleAPIErrors(Exception exception, Model model) {
        model.addAttribute("message", exception.getMessage() != null
                ? exception.getMessage() : exception.getLocalizedMessage());
        return "error";
    }

    @ExceptionHandler({NoDefaulWalletException.class})
    public String handleWalletMissing(Exception exception, Model model) {
        model.addAttribute("message", exception.getMessage() != null
                ? exception.getMessage() : exception.getLocalizedMessage());
        return "after-reg-setup-wallet";
    }

}
