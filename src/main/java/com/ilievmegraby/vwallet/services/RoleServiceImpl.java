package com.ilievmegraby.vwallet.services;


import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    @Override
    public Role getByName(String roleName) {
        return roleRepository.getByName(roleName);
    }
}
