package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;

import java.util.List;

public interface UserService {

    public List<User> getAllUsers(int page);

    public User getById(int id);

    public User getByName(String userName);

    public User getByEmail(String email);

    public User getByPhone(String phone);

    public User getUser(String input);

    public User createUser(UserRegistrationDTO registrationDTO);

    public User updateUser(User user, UserUpdateDTO updateDTO);

    public void deleteUser(String userName);

    public void blockUser(String userName);

    public void activateAccount(String username);

  //  public List<User> getPage(int page, String userDetails);

    public int getNumberOfPages();

    public int getNumberOfPagesAdmin();

   // public boolean emailAlreadyExists(String email);
}
