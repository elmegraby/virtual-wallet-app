package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.connectors.CurrencyConverterConnector;
import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.APIConnectionException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.repositories.ExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import static com.ilievmegraby.vwallet.constants.MessageConstants.SAME_CURRENCY_TRANSACTION;
import static com.ilievmegraby.vwallet.helperClasses.DateConverter.getDateFromTimestamp;
import static com.ilievmegraby.vwallet.helperClasses.Utilities.formatBigDecimal;
import static com.ilievmegraby.vwallet.models.enums.CurrencyCode.*;

@Service
public class


ExchangeRateServiceImpl implements ExchangeRateService {
    private ExchangeRateRepository exchangeRateRepository;
    private CurrencyConverterConnector converter;
    private EntityMapper entityMapper;


    @Autowired
    public ExchangeRateServiceImpl(ExchangeRateRepository exchangeRateRepository, CurrencyConverterConnector converter) {
        this.exchangeRateRepository = exchangeRateRepository;
        this.converter = converter;
    }

    @Autowired
    @Lazy
    public void setEntityMapper(EntityMapper entityMapper) {
        this.entityMapper = entityMapper;
    }

    @Override
    public BigDecimal convert(BigDecimal amount, CurrencyCode from, CurrencyCode to) {
        if (from.equals(to)) {
            return amount;
        }
        return amount.multiply(getExchangeRate(from, to).getRate());
    }

    @Override
    public ExchangeRate getExchangeRate(CurrencyCode from, CurrencyCode to) {
        if (from.equals(to)) {
            return new ExchangeRate(from, to, BigDecimal.ONE);
        }

        try {
            ExchangeRate apiRate = converter.getLiveExchangeRate(from, to);
            apiRate.setUpToDate(true);
            return apiRate;

        } catch (APIConnectionException ex) {
            ExchangeRate dbRate = exchangeRateRepository.getConversionRate(from, to);
            dbRate.setUpToDate(false);
            return dbRate;
        }
    }

    @Override
    @Scheduled(fixedRate = 300000)
    public void updateRates() {
        ExchangeRate BGN_EUR = converter.getLiveExchangeRate(BGN, EUR);
        ExchangeRate EUR_BGN = converter.getLiveExchangeRate(EUR, BGN);
        ExchangeRate EUR_USD = converter.getLiveExchangeRate(EUR, USD);
        ExchangeRate USD_EUR = converter.getLiveExchangeRate(USD, EUR);
        ExchangeRate USD_BGN = converter.getLiveExchangeRate(USD, BGN);
        ExchangeRate BGN_USD = converter.getLiveExchangeRate(BGN, USD);

        exchangeRateRepository.updateRates(BGN_EUR, EUR_BGN, USD_BGN, BGN_USD, EUR_USD, USD_EUR);
    }

    @Override
    public ResponseEntity<String> getExchangeRateInfoMessage(TransactionDTO transactionDTO) {
        Transaction transaction = entityMapper.getTransactionFromDTO(transactionDTO);
        CurrencyCode senderCurrency = transaction.getWalletUsed().getCurrencyCode();
        CurrencyCode recepientCurrency = transaction.getRecipientWallet().getCurrencyCode();
        BigDecimal amount = new BigDecimal(transactionDTO.getAmount());
        ExchangeRate exchangeRate = getExchangeRate(senderCurrency, recepientCurrency);
        StringBuilder message = new StringBuilder();

        if (senderCurrency != recepientCurrency) {

            if (!exchangeRate.isUpToDate()) { message.append(String.format(MessageConstants.OUTDATED_EXCHANGE_RATE_MESSAGE));
            }

            String mainInformation = String.format(MessageConstants.EXCHANGE_RATE_INFO_MESSAGE,
                    amount, senderCurrency, recepientCurrency, formatBigDecimal(amount.multiply(exchangeRate.getRate())), recepientCurrency,
                    senderCurrency, recepientCurrency, formatBigDecimal(exchangeRate.getRate()), getDateFromTimestamp(exchangeRate.getUpdated()));

            message.append(mainInformation);
        } else {
            message.append(SAME_CURRENCY_TRANSACTION);
        }
        ResponseEntity<String> responseEntity = new ResponseEntity<String>(message.toString(), exchangeRate.isUpToDate() ?
                HttpStatus.OK : HttpStatus.SERVICE_UNAVAILABLE);

        return responseEntity;
    }
}
