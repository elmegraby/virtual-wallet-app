package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.ExceededReferralsException;
import com.ilievmegraby.vwallet.exceptions.NoDefaulWalletException;
import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.repositories.verificationRepositories.ReferralRepository;
import com.ilievmegraby.vwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;
import static com.ilievmegraby.vwallet.constants.MessageConstants.REFERRALS_LIMIT_REACHED;

@Service
public class ReferralServiceImpl implements ReferralService {

    private ReferralRepository referralRepository;

    @Autowired
    public ReferralServiceImpl(ReferralRepository referralRepository){
        this.referralRepository=referralRepository;
    }

    @Override
    public Referral findByEmail(String email) {
        return referralRepository.findByEmail(email);
    }

    @Override
    public void createReferral(User user, String email) {

        if (user.getDefaultWallet()==null){
            throw new NoDefaulWalletException(REFERRALS_NO_DEFAULT_WALLET);
        }

        if (user.getReferralsCount() >=5) {
            throw new ExceededReferralsException(REFERRALS_LIMIT_REACHED);
        }

        if (user.getEmail().equals(email)) {
            throw new DuplicateEntityException(REFERRALS_SELF_REFER);
        }

        if (referralRepository.findByEmail(email)!=null) {
            throw new DuplicateEntityException(String.format(REFERRALS_USER_DUPLICATE, email));
        }

        referralRepository.createReferral(user, email);
    }
}
