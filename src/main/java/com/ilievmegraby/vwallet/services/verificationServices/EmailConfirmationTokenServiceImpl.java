package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.repositories.verificationRepositories.EmailConfirmationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailConfirmationTokenServiceImpl implements EmailConfirmationTokenService {

    private EmailConfirmationTokenRepository emailConfirmationTokenRepository;

    @Autowired
    public EmailConfirmationTokenServiceImpl(EmailConfirmationTokenRepository emailConfirmationTokenRepository) {
        this.emailConfirmationTokenRepository = emailConfirmationTokenRepository;
    }

    @Override
    public ConfirmationToken findByConfirmationToken(String confirmationToken) {
        return emailConfirmationTokenRepository.findByConfirmationToken(confirmationToken);
    }

    @Override
    public void createConfirmationToken(ConfirmationToken confirmationToken) {
        emailConfirmationTokenRepository.createConfirmationToken(confirmationToken);
    }
}
