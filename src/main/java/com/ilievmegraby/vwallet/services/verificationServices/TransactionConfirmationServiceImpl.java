package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import com.ilievmegraby.vwallet.repositories.verificationRepositories.TransactionConfirmationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionConfirmationServiceImpl implements TransactionConfirmationService {

    private TransactionConfirmationRepository transactionConfirmationRepository;

    @Autowired
    public TransactionConfirmationServiceImpl(TransactionConfirmationRepository transactionConfirmationRepository){
        this.transactionConfirmationRepository = transactionConfirmationRepository;
    }

    @Override
    public TransactionConfirmationCode findByCode(String code) {
       return transactionConfirmationRepository.findByCode(code);
    }

    @Override
    public void createCode(TransactionConfirmationCode transactionConfirmationCode) {
        transactionConfirmationRepository.createCode(transactionConfirmationCode);
    }

    @Override
    public void deleteCode(TransactionConfirmationCode transactionConfirmationCode) {
        transactionConfirmationRepository.deleteCode(transactionConfirmationCode);
    }
}
