package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;

public interface EmailConfirmationTokenService {

    public ConfirmationToken findByConfirmationToken(String confirmationToken);

    public void createConfirmationToken(ConfirmationToken confirmationToken);
}
