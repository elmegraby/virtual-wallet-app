package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.User;

public interface ReferralService {

    public Referral findByEmail(String email);

    public void createReferral(User user, String email);
}
