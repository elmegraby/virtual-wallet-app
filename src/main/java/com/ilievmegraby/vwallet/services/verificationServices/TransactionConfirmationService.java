package com.ilievmegraby.vwallet.services.verificationServices;

import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;

public interface TransactionConfirmationService {

    public TransactionConfirmationCode findByCode(String code);

    public void createCode(TransactionConfirmationCode transactionConfirmationCode);

    public void deleteCode(TransactionConfirmationCode transactionConfirmationCode);
}
