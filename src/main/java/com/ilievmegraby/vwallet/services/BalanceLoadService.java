package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.BalanceLoadDTO;

import java.util.Date;
import java.util.List;

public interface BalanceLoadService {
    void confirmCardCharge(BalanceLoad balanceLoad);

    List<BalanceLoad> getAll();

    List<BalanceLoad> filter(String username, String daterange, String cardUsed, String toWallet, String orderBy, int page);

    List<BalanceLoad> getByUser(int userId);

    List<BalanceLoad> getByWallet(int walletId);

    List<BalanceLoad> getByCard(String cardNumber);

    BalanceLoad create(BalanceLoadDTO balanceLoadDTO);

    public int getNumberOfPages(String username, String dateRange, String cardUsed, String toWallet, String orderBy, int page);
}
