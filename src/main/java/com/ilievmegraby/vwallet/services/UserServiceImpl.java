package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.ImageReadException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.helperClasses.ImageFileValidator;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private EntityMapper entityMapper;
    private ImageFileValidator imageFileValidator;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ImageFileValidator imageFileValidator) {
        this.userRepository = userRepository;
        this.imageFileValidator = imageFileValidator;
    }
    @Autowired
    private void setEntityMapper(EntityMapper entityMapper) {
        this.entityMapper = entityMapper;
    }

    @Override
    public List<User> getAllUsers(int page) {
        return userRepository.getAllUsers(page);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByName(String userName) {
        return userRepository.getByName(userName);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByPhone(phone);
    }

    @Override
    public User getUser(String input) {
        return userRepository.getUser(input);
    }

    @Override
    public User createUser(UserRegistrationDTO registrationDTO) {

        if (userRepository.userExists(registrationDTO.getUserName())
                || userRepository.emailAlreadyExists(registrationDTO.getEmail())
                || userRepository.phoneAlreadyExists(registrationDTO.getPhoneNumber())) {

            throw new DuplicateEntityException(USER_ALREADY_EXISTS); }

        User newUser = entityMapper.getUserFromRegistrationDTO(registrationDTO);
        return userRepository.createUser(newUser);
    }

    @Override
    public User updateUser(User user, UserUpdateDTO updateDTO) {

        if (!userRepository.userExists(user.getUserName())
                || !userRepository.emailAlreadyExists(user.getEmail())
                || !userRepository.phoneAlreadyExists(user.getPhoneNumber())){
            throw new EntityNotFoundException(USER_CREDENTIALS_NOT_FOUND); }

        if (updateDTO.getImage() != null && !imageFileValidator.isValid(updateDTO.getImage())) {
            throw new ImageReadException(UNSUPPORTED_IMAGE_FILE);
        }
        User updatedUser = entityMapper.getUserFromUpdateDTO(user, updateDTO);
        return userRepository.updateUser(updatedUser);
    }

    @Override
    public void deleteUser(String userName) {
        if (!userRepository.userExists(userName)) {
            throw new EntityNotFoundException(String.format(USERNAME_NOT_FOUND, userName));
        }

        userRepository.deleteUser(userName);
    }

    @Override
    public void blockUser(String userName) {

        if (!userRepository.userExists(userName)) {
            throw new EntityNotFoundException(USERNAME_NOT_FOUND);

        }
        userRepository.blockUser(userName);
    }

    @Override
    public void activateAccount(String username) {

        if (!userRepository.userExists(username)) {
            throw new EntityNotFoundException(String.format(USERNAME_NOT_FOUND, username));

        }
        userRepository.activateAccount(username);
    }


    @Override
    public int getNumberOfPages() {
        return userRepository.getNumberOfPages();
    }

    @Override
    public int getNumberOfPagesAdmin() {
        return userRepository.getNumberOfPagesAdmin();
    }
}
