package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import java.util.List;

public interface TransactionService {

    List<Transaction> getAll();

    Transaction getById(int id);

    List<Transaction> getUserTransactions(String userName);

    List<Transaction> filter(String username, String direction, String daterange, String recipient, String sortBy, int page);

    Transaction create(TransactionDTO transactionDTO);

    public void processTransaction(Transaction transaction);

    public int getNumberOfPages(String sender, String direction, String dateRange, String sortBy, String recipient, int page);
}
