package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;

import java.util.List;

public interface CardService {
    CardDetails getById(User user, int id);

    List<CardDetails> getByUsername(User user, String username);

    CardDetails create(CardDetails cardDetails);

    CardDetails update(User user, CardDetails cardDetails);

    void delete(int id, User user);
}
