package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.Role;

public interface RoleService {

    public Role getByName(String roleName);

}
