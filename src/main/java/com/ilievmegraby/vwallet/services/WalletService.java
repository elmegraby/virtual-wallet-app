package com.ilievmegraby.vwallet.services;

        import com.ilievmegraby.vwallet.models.User;
        import com.ilievmegraby.vwallet.models.Wallet;
        import com.ilievmegraby.vwallet.models.dtos.WalletDTO;

        import java.math.BigDecimal;

public interface WalletService {
    Wallet getById(int id);

    void createWallet(User user, WalletDTO walletDTO);

    void setDefault(User user, Wallet wallet);

    void addFunds(Wallet wallet, BigDecimal amount);

    void addMember(User user, Wallet wallet);

    void removeMember(User user, Wallet wallet);


}
