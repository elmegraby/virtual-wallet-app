package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

public interface ExchangeRateService {

    ExchangeRate getExchangeRate(CurrencyCode from, CurrencyCode to);

    void updateRates();

    BigDecimal convert(BigDecimal amount, CurrencyCode from, CurrencyCode to);

    ResponseEntity<String> getExchangeRateInfoMessage(TransactionDTO transactionDTO);

}
