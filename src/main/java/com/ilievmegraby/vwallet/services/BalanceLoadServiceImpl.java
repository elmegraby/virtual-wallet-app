package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.connectors.PaymentProviderConnector;
import com.ilievmegraby.vwallet.connectors.PaymentProviderConnectorImpl;
import com.ilievmegraby.vwallet.helperClasses.DateConverter;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.dtos.BalanceLoadDTO;
import com.ilievmegraby.vwallet.repositories.BalanceLoadRepository;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BalanceLoadServiceImpl implements BalanceLoadService {
    private BalanceLoadRepository balanceLoadRepository;
    private EntityMapper entityMapper;
    private WalletService walletService;
    private PaymentProviderConnector paymentProviderConnector;
    private CardRepository cardRepository;

    @Autowired
    public BalanceLoadServiceImpl(BalanceLoadRepository balanceLoadRepository, EntityMapper entityMapper,
                                  WalletService walletService, PaymentProviderConnectorImpl paymentProviderConnector,
                                  CardRepository cardRepository) {
        this.balanceLoadRepository = balanceLoadRepository;
        this.entityMapper = entityMapper;
        this.walletService = walletService;
        this.paymentProviderConnector = paymentProviderConnector;
        this.cardRepository = cardRepository;

    }

    @Override
    public void confirmCardCharge(BalanceLoad balanceLoad) {
        paymentProviderConnector.makePayment(balanceLoad);
    }

    @Override
    public List<BalanceLoad> getAll() {
        return balanceLoadRepository.getAll();
    }

    @Override
    public List<BalanceLoad> filter(String username, String daterange, String cardUsed, String toWallet, String orderBy, int page) {
        Date[] dates = DateConverter.getDateRangeFromString(daterange);
        Date from = dates[0];
        Date to = dates[1];

        String cardNum = !cardUsed.isEmpty() ? cardRepository.getById(Integer.parseInt(cardUsed)).getCardNumber() : cardUsed;
        String walletUsed = !toWallet.isEmpty() ? walletService.getById(Integer.parseInt(toWallet)).getName() : toWallet;

        return balanceLoadRepository.filter(username, from, to, cardNum, walletUsed, orderBy, page);
    }


    @Override
    public List<BalanceLoad> getByUser(int userId) {
        return balanceLoadRepository.getByUser(userId);
    }

    @Override
    public List<BalanceLoad> getByWallet(int walletId) {
        return balanceLoadRepository.getByWallet(walletId);
    }

    @Override
    public List<BalanceLoad> getByCard(String cardNumber) {
        return balanceLoadRepository.getByCard(cardNumber);
    }

    @Override
    public BalanceLoad create(BalanceLoadDTO balanceLoadDTO) {
        BalanceLoad balanceLoad = entityMapper.getBalanceLoadFromDTO(balanceLoadDTO);
        confirmCardCharge(balanceLoad);
        return balanceLoadRepository.create(balanceLoad);
    }


    @Override
    public int getNumberOfPages(String username, String daterange, String cardUsed, String toWallet, String orderBy, int page) {
        Date[] dates = DateConverter.getDateRangeFromString(daterange);
        Date from = dates[0];
        Date to = dates[1];
        return balanceLoadRepository.getNumberOfPages(username, from, to, cardUsed, toWallet, orderBy, page);
    }
}
