package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.constants.MessageConstants;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.InvalidTypeException;
import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import com.ilievmegraby.vwallet.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import static com.ilievmegraby.vwallet.constants.MessageConstants.*;

@Service
public class WalletServiceImpl implements WalletService {

    private WalletRepository walletRepository;
    private EntityMapper entityMapper;
    private EmailSenderService emailSenderService;
    private EmailFactory emailFactory;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository, EmailFactory emailFactory,
                             EmailSenderService emailSenderService) {
        this.walletRepository = walletRepository;
        this.emailSenderService = emailSenderService;
        this.emailFactory = emailFactory;
    }

    @Autowired
    @Lazy
    public void setEntityMapper(EntityMapper entityMapper) {
        this.entityMapper = entityMapper;
    }

    @Override
    public Wallet getById(int id) {
        return walletRepository.getById(id);
    }

    @Override
    public void createWallet(User user, WalletDTO walletDTO) {

        Wallet wallet = entityMapper.getWalletFromDTO(walletDTO);

        walletRepository.createWallet(user, wallet);
    }

    @Override
    public void addFunds(Wallet wallet, BigDecimal amount) {

        if (!walletRepository.walletExists(wallet.getId())) {

            throw new EntityNotFoundException("Wallet was not found.");
        }

        walletRepository.addFunds(wallet, amount);
    }

    @Override
    @PreAuthorize("#wallet.creator.userName == authentication.principal.username")
    public void addMember(User user, Wallet wallet) {
        if (!walletRepository.walletExists(wallet.getId())) {
            throw new EntityNotFoundException("Wallet was not found.");
        }

        if (wallet.getWalletType().toString().equalsIgnoreCase("Personal")) {
            throw new InvalidTypeException(WALLET_ADD_REMOVE_USER_FROM_PERSONAL_WALLET);
        }

        if (!wallet.getUsers().contains(user)) {
            walletRepository.addMember(user, wallet);

            SimpleMailMessage invite = emailFactory.getWalletInviteEmail(wallet.getCreator().getFullName(), wallet.getName(), user.getEmail());
            emailSenderService.sendEmail(invite);
        }
    }

    @Override
    @PreAuthorize("#wallet.creator.userName == authentication.principal.username")
    public void removeMember(User user, Wallet wallet) {
        if (!walletRepository.walletExists(wallet.getId())) {
            throw new EntityNotFoundException("Wallet was not found.");
        }

        if (wallet.getWalletType().toString().equalsIgnoreCase("Personal")) {
            throw new InvalidTypeException(WALLET_ADD_REMOVE_USER_FROM_PERSONAL_WALLET);
        }

        walletRepository.removeMember(user, wallet);
    }

    @Override
    public void setDefault(User user, Wallet wallet) {
        walletRepository.setDefault(user, wallet);
    }
}
