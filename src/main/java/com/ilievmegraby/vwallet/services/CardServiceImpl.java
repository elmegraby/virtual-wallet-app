package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.connectors.PaymentProviderConnectorImpl;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.*;


@Service
public class CardServiceImpl implements CardService {
    private CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository, PaymentProviderConnectorImpl helper) {
        this.cardRepository = cardRepository;
    }

    @Override
    public CardDetails getById(User user, int id) {
        if (!user.getCards().contains(cardRepository.getById(id)) && user.getRoles().stream().noneMatch(r -> r.getRole().equals("ROLE_ADMIN"))) {
            throw new UnauthorizedActionException(UNAUTHORIZED_ACTION);
        }
        return cardRepository.getById(id);
    }

    @Override
    public List<CardDetails> getByUsername(User user, String username) {
        if (!user.getUserName().equals(username) && user.getRoles().stream().noneMatch(r -> r.getRole().equals("ROLE_ADMIN"))) {
            throw new UnauthorizedActionException(UNAUTHORIZED_ACTION);
        }
        return cardRepository.getByUsername(username);
    }

    @Override
    public CardDetails create(CardDetails cardDetails) {
        return cardRepository.create(cardDetails);
    }

    @Override
    public CardDetails update(User user, CardDetails cardDetails) {
        CardDetails originalCard = cardRepository.getById(cardDetails.getId());
        User cardHolder = originalCard.getUser();

        // The validation below is applied just in case somebody tries to access the Rest endpoint through postman.

        if (!cardHolder.equals(user) && user.getRoles().stream().noneMatch(r -> r.getRole().equals("ROLE_ADMIN"))) {
            throw new UnauthorizedActionException(UNAUTHORIZED_ACTION);
        }

        cardDetails.setUser(cardHolder);
        return cardRepository.update(cardDetails);
    }

    @Override
    public void delete(int id, User user) {
        CardDetails card = getById(user, id);

        cardRepository.delete(card);
    }
}
