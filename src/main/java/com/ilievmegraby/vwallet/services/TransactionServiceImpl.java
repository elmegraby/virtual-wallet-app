package com.ilievmegraby.vwallet.services;

import com.ilievmegraby.vwallet.exceptions.InsufficientFundsException;
import com.ilievmegraby.vwallet.helperClasses.DateConverter;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.TransactionDirection;
import com.ilievmegraby.vwallet.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    private TransactionRepository transactionRepository;
    private EntityMapper entityMapper;


    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, EntityMapper entityMapper) {
        this.transactionRepository = transactionRepository;
        this.entityMapper = entityMapper;

    }

    @Override
    public List<Transaction> getAll() {
        return transactionRepository.getAll();
    }

    @Override
    public Transaction getById(int id) {
        return transactionRepository.getById(id);
    }

    @Override
    public List<Transaction> getUserTransactions(String userName) {
        return transactionRepository.getUserTransactions(userName);
    }

    @Override
    public List<Transaction> filter(String username, String direction, String daterange, String recipient, String sortBy, int page) {
        Date [] dates = DateConverter.getDateRangeFromString(daterange);
        Date fromDate = dates[0];
        Date toDate = dates[1];
        return transactionRepository.filter(username, direction, fromDate, toDate, recipient, sortBy, page);
    }

    @Override
    public Transaction create(TransactionDTO transactionDTO) {

        Transaction transaction = entityMapper.getTransactionFromDTO(transactionDTO);

        if (transaction.getAmount().compareTo(transaction.getWalletUsed().getCurrentAmount()) > 0) {
            throw new InsufficientFundsException(String.format("Transaction cannot be processed due to insufficient funds."));
        }

        return transactionRepository.create(transaction);
    }

    @Override
    public void processTransaction(Transaction transaction) {

        if (transaction.getAmount().compareTo(transaction.getWalletUsed().getCurrentAmount()) > 0) {
            throw new InsufficientFundsException(String.format("Transaction cannot be processed due to insufficient funds."));
        }

        transactionRepository.processTransaction(transaction);
    }

    @Override
    public int getNumberOfPages(String sender, String direction, String dateRange, String recipient, String sortBy, int page) {
        Date [] dates = DateConverter.getDateRangeFromString(dateRange);
        Date from = dates[0];
        Date to = dates[1];
        return transactionRepository.getNumberOfPages(sender, direction, from, to, recipient, sortBy,page);
    }
}

