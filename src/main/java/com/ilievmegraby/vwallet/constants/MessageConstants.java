package com.ilievmegraby.vwallet.constants;

public class MessageConstants {

    //##### Generic errors ####################################################################################
    public static String ENTITY_NOT_FOUND_BY_PROPERTY_MESSAGE = "%s with %s %d doesn't exists!";
    public static String ENTITY_NOT_FOUND_BY_OBJECT_COMPARISON = "The %s you're trying to operate on doesn't exist!";
    public static String DUPLICATE_ENTITY_MESSAGE = "%s with duplicate values already exists!";
    public static String DUPLICATE_USER_PROPERTIES = "A user with this username, email or phone number already exists!";
    public static String UNSUPPORTED_IMAGE_FILE = "Images must be in jpg/jpeg/png format and less than 5 MB in size!";
    public static String IMAGE_READ_ERROR = "An Unexpected error occurred while reading the image file!";
    public static String USERNAME_NOT_FOUND = "User with username: %s was not found.";
    public static String USER_ALREADY_EXISTS = "User with the same username/email/phone already exists.";
    public static String USER_CREDENTIALS_NOT_FOUND = "No user with the given credentials was found";
    public static String INACTIVE_LINK_MESSAGE = "Link is broken or no longer active!";

    //##### Wallet panel operations ##################################################################################
    public static String WALLET_ADD_REMOVE_USER_FROM_PERSONAL_WALLET = "You cannot add or remove members to and from a personal wallet.";
    public static String WALLET_UNAUTHORIZED_ACTION = "Only the wallet's creator can add or remove users from it!";

    //##### Financial operations ####################################################################################
    public static String CARD_DENIED = "Adding card denied! Please contact you bank for assistance!";
    public static String TRANSACTIONS_SENDER_NO_WALLET = "You must first create a wallet before sending or receiving money.";
    public static String CURRENCY_CONVERSION_UNSUCCESSFUL = "No exchange rate found for %s and %s!";
    public static String CONVERSION_API_CONNECTION_ERROR = "Connection to the currency conversion service failed!";
    public static String INSUFFICIENT_FUNDS_MESSAGE = "Your bank has denied the payment. Please make sure you have sufficient funds.";
    public static String BANK_REQUEST_BODY_ERROR = "Key information is missing from the request. Please make sure you've filled out all the fields correctly or check you internet connection";
    public static String BANK_REQUEST_WITH_INVALID_KEY = "Payment API access denied: key missing or invalid!";
    public static String BANK_CONNECTION_NOT_POSSIBLE = "Connection with the bank could not be established. Please try again later.";
    public static String UNAUTHORIZED_ACTION = "You're not authorized to perform this action!";


    //##### Exchange rate messages #####################################################################################
    public static String OUTDATED_EXCHANGE_RATE_MESSAGE = "Exchange rate out of date!%n%nDue to technical issues" +
            " with our currency conversion provider the latest stored exchange rate will be used for this transaction.%n";

    public static String EXCHANGE_RATE_INFO_MESSAGE = "You're sending %s %s. Since the recipient's wallet is in %s they" +
            " are going to receive %s %s. The current exchange rate %s-%s is %s, last updated on %s";

    public static String SAME_CURRENCY_TRANSACTION = "Your wallets are in the same currency. What you send is what they get!";




    //##### Referrals ##################################################################################################
    public static String REFERRALS_NO_DEFAULT_WALLET = "You must have a wallet before referring a friend. Otherwise we wouldn't be able to transfer your referral bonus.";
    public static String REFERRALS_LIMIT_REACHED = "You have reached the maximum count of referrals.";
    public static String REFERRALS_USER_DUPLICATE = "User with email %s has already been referred.";
    public static String REFERRALS_SELF_REFER = "Come on, you can't refer yourself!";


    //##### Email message fields ####################################################################################
    public static String MAIL_COMPLETE_REGISTRATION_SUBJECT = "Complete Registration!";
    public static String MAIL_VERIFY_TRANSACTION_SUBJECT = "Verify Your Transaction!";
    public static String MAIL_REFERRAL_SUBJECT = "You have been referred!";
    public static String MAIL_WALLET_INVITEE_SUBJECT = "You've been added to a joint wallet!";

    public static String MAIL_REFERRAL_BODY = "You have been referred! Click on the link, complete your registration and get your 10$ bonus! http://localhost:8080/registration";
    public static String MAIL_WALLET_INVITEE_BODY = "Congratulations!%n %s has added you to his joint wallet %s.%nLog in and start using it now!%n http://localhost:8080";
    public static String MAIL_VERIFY_TRANSACTION_BODY = "To process your transaction, you need to verify it first by clicking the link: http://localhost:8080/transactions/apply-code and submitting the following code: %s";
    public static String MAIL_COMPLETE_REST_REGISTRATION_BODY = "To confirm your account in Virtual Wallet, please click here: http://localhost:8080/api/v1/users/confirm-account?token=";
    public static String MAIL_COMPLETE_REGISTRATION_BODY = "To confirm your account in Virtual Wallet, please click here : http://localhost:8080/registration/confirm-account?token=";

}
