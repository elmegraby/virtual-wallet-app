package com.ilievmegraby.vwallet.ServiceTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.connectors.PaymentProviderConnector;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.BalanceLoadDTO;
import com.ilievmegraby.vwallet.repositories.BalanceLoadRepository;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import com.ilievmegraby.vwallet.services.BalanceLoadServiceImpl;
import com.ilievmegraby.vwallet.services.WalletService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;


import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BalanceLoadServiceImplTests {

    private BalanceLoad balanceLoad;
    private Wallet wallet;
    private CardDetails card;
    private BalanceLoadDTO balanceLoadDTO;


    @Mock
    BalanceLoadRepository balanceLoadRepository;

    @Mock
    CardRepository cardRepository;

    @Mock
    WalletService walletService;

    @Mock
    EntityMapper entityMapper;

     @Mock
     PaymentProviderConnector bankAPIConnector;

    @InjectMocks
    BalanceLoadServiceImpl balanceLoadService;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before() {
        //this.exchangeRate = ObjectFactory.getExchangeRate();
        this.balanceLoad = ObjectFactory.getBalanceLoad();
        this.wallet  = ObjectFactory.getWallet();
        this.card = ObjectFactory.getCardDetails();
        this.balanceLoadDTO = ObjectFactory.getBalanceLoadDTO();

    }


    @Test
    public void getAll_ShouldCallRepository() {
        //Arrange, Act
        balanceLoadService.getAll();
        //Assert
        Mockito.verify(balanceLoadRepository, times(1)).getAll();
    }

    @Test
    public void getByUser_ShouldCallRepository() {
        //Arrange, Act
        balanceLoadService.getByUser(1);
        //Assert
        Mockito.verify(balanceLoadRepository, times(1)).getByUser(anyInt());
    }

    @Test
    public void getByCard_ShouldCallRepository() {
        //Arrange, Act
        balanceLoadService.getByCard("1234-1234-1234-1234");
        //Assert
        Mockito.verify(balanceLoadRepository, times(1)).getByCard(anyString());
    }


    @Test
    public void getByWallet_ShouldCallRepository() {
        //Arrange, Act
        balanceLoadService.getByWallet(1);
        //Assert
        Mockito.verify(balanceLoadRepository, times(1)).getByWallet(anyInt());
    }


    @Test
    public void confirmCardCharge_ShouldCallBankAPIConnector() {
        //Arrange, Act
        balanceLoadService.confirmCardCharge(balanceLoad);
        //Assert
        Mockito.verify(bankAPIConnector,times(1)).makePayment(any());
    }


    @Test
    public void filter_ShouldCallRepository() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(card);
        when(walletService.getById(anyInt())).thenReturn(wallet);

        // Act
        balanceLoadService.filter("iliev-telerik"
                ,"12/12/19 - 01/02/20"
                , "1"
                ,"2"
                ,"amnt desc"
                ,1);
        //Assert
        Mockito.verify(balanceLoadRepository, times(1)).filter(anyString()
                , any()
                , any()
                , anyString()
                , anyString()
                , anyString()
                , anyInt());
    }

    @Test
    public void create_ShouldCallRepository() {
        //Arrange
        when(entityMapper.getBalanceLoadFromDTO(balanceLoadDTO)).thenReturn(balanceLoad);
        // Act
        balanceLoadService.create(balanceLoadDTO);
                //Assert
        Mockito.verify(balanceLoadRepository, times(1)).create(balanceLoad);
    }

    @Test
    public void getNumberOfPages_ShouldCallRepository() {
        //Arrange

        balanceLoadService.getNumberOfPages("iliev-telerik"
                ,"12/12/19 - 01/02/20"
                , "1"
                ,"2"
                ,"amnt desc"
                ,1);

        Mockito.verify(balanceLoadRepository, times(1)).getNumberOfPages(anyString()
                , any()
                , any()
                , anyString()
                , anyString()
                , anyString()
                , anyInt());
    }
}
