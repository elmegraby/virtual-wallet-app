package com.ilievmegraby.vwallet.ServiceTests;


import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.InvalidTypeException;
import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.models.enums.WalletType;
import com.ilievmegraby.vwallet.repositories.RoleRepository;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import com.ilievmegraby.vwallet.repositories.WalletRepository;
import com.ilievmegraby.vwallet.repositories.WalletRepositoryImpl;
import com.ilievmegraby.vwallet.services.RoleServiceImpl;
import com.ilievmegraby.vwallet.services.UserServiceImpl;
import com.ilievmegraby.vwallet.services.WalletServiceImpl;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.springframework.mail.SimpleMailMessage;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WalletServiceImplTests {

    private Wallet wallet;
    private User user;
    private WalletDTO walletDTO;
    private BigDecimal amount;

    @Mock
    WalletRepository walletRepository;

    @Mock
    EntityMapper entityMapper;

    @Mock
    EmailSenderService emailSenderService;

    @Mock
    EmailFactory emailFactory;

    @InjectMocks
    WalletServiceImpl walletService;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before(){
        this.wallet = ObjectFactory.getWallet();
        this.user= ObjectFactory.getUser();
        this.walletDTO = ObjectFactory.getWalletDTO();
        this.amount = new BigDecimal(20);

    }


    @Test
    public void getById_ShouldCallRepository() {
        //Arrange, Act
        walletService.getById(anyInt());
        //Assert
        Mockito.verify(walletRepository, times(1)).getById(anyInt());
    }

    @Test
    public void createWallet_ShouldCallRepository(){
        //Arrange, Act
        walletService.createWallet(user, walletDTO);
        //Assert
        Mockito.verify(walletRepository, times(1)).createWallet(any(),any());
        //any() is an argument type due to internal WalletServiceImpl call to a method in EntityMapper.class. We want to avoid mocking the class.

    }

    @Test
    public void addFunds_ShouldCallRepository(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(true);
        //Act
        walletService.addFunds(wallet,amount);
        //Assert
        Mockito.verify(walletRepository, times(1)).addFunds(wallet,amount);

    }

    @Test (expected = EntityNotFoundException.class)
    public void addFunds_ShouldThrowException_WhenWalletNotFound(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(false);
        //Act
        walletService.addFunds(wallet, amount);
    }


    @Test (expected = EntityNotFoundException.class)
    public void addMember_ShouldThrowException_WhenWalletNotFound(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(false);
        wallet.setCreator(user);
        //Act
        walletService.addMember(user,wallet);
    }

    @Test (expected = InvalidTypeException.class)
    public void addMember_ShouldThrowException_WhenWalletIsOfPersonalType(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(true);
        wallet.setCreator(user);
        //Act
        walletService.addMember(user,wallet);
    }

    @Test
    public void addMember_ShouldCallRepository(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(true);
        wallet.setCreator(user);
       /* Mockito.when(emailFactory.getWalletInviteEmail("iiiev", "My Wallet", "iliev@telerik.com"))
                .thenReturn(new SimpleMailMessage());

        */
        wallet.setWalletType(WalletType.JOINT);
        //Act
        walletService.addMember(user,wallet);
        //Assert
        Mockito.verify(walletRepository, times(1)).addMember(user, wallet);

    }

    @Test (expected = EntityNotFoundException.class)
    public void removeMember_ShouldThrowException_WhenWalletNotFound(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(false);
        wallet.setCreator(user);
        //Act
        walletService.removeMember(user, wallet);
    }

    @Test (expected = InvalidTypeException.class)
    public void removeMember_ShouldThrowException_WhenWalletIsOfPersonalType(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(true);
        wallet.setCreator(user);
        //Act
        walletService.removeMember(user,wallet);
    }

    @Test
    public void removeMember_ShouldCallRepository(){
        //Arrange
        Mockito.when(walletRepository.walletExists(anyInt())).thenReturn(true);
        wallet.setWalletType(WalletType.JOINT);
        wallet.setCreator(user);
        //Act
        walletService.removeMember(user,wallet);
        //Assert
        Mockito.verify(walletRepository, times(1)).removeMember(user,wallet);

    }

    @Test
    public void setDefault_ShouldCallRepository(){
        //Arrange, Act
        walletService.setDefault(user, wallet);
        //Assert
        Mockito.verify(walletRepository, times(1)).setDefault(user, wallet);

    }
}
