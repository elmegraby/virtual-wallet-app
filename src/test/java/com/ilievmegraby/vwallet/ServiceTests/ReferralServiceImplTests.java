
package com.ilievmegraby.vwallet.ServiceTests;


import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.ExceededReferralsException;
import com.ilievmegraby.vwallet.exceptions.NoDefaulWalletException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Referral;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.repositories.RoleRepository;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import com.ilievmegraby.vwallet.repositories.verificationRepositories.ReferralRepository;
import com.ilievmegraby.vwallet.services.RoleServiceImpl;
import com.ilievmegraby.vwallet.services.UserServiceImpl;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReferralServiceImplTests {

    private Referral referral;
    private User user;
    private Wallet wallet;

    @Mock
    ReferralRepository referralRepository;

    @InjectMocks
    ReferralServiceImpl referralService;

    @Before
    public void before() {
        this.referral = ObjectFactory.getReferral();
        this.user=ObjectFactory.getUser();
        this.wallet =ObjectFactory.getWallet();
        user.setDefaultWallet(wallet);
    }

    @Test
    public void getByEmail_ShouldCallRepository() {
        //Arrange, Act
        referralService.findByEmail(referral.getEmail());
        //Assert
        Mockito.verify(referralRepository, times(1)).findByEmail(referral.getEmail());
    }

    @Test(expected = ExceededReferralsException.class)
    public void createReferral_ShouldThrowExceptionWhenReferralCountExceeded() {
        //Arrange
        user.setReferralsCount(6);
        // Act
        referralService.createReferral(user, anyString());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createReferral_ShouldThrowExceptionWhenUserAlreadyReferred() {
        //Arrange
        Mockito.when(referralRepository.findByEmail(anyString())).thenReturn(referral);
        user.setReferralsCount(3);
        // Act
        referralService.createReferral(user, anyString());
    }

    @Test(expected = DuplicateEntityException.class)
    public void createReferral_ShouldThrowExceptionWhenEmailSameAsReferrerEmail() {
        //Arrange
        Mockito.when(referralRepository.findByEmail(anyString())).thenReturn(referral);
        user.setReferralsCount(3);
        user.setEmail("ilkoiliev@abv.bg");

        // Act
        referralService.createReferral(user, "ilkoiliev@abv.bg");
    }

    @Test(expected = NoDefaulWalletException.class)
    public void createReferral_shouldThrowException_whenNoDefaultWalletAvailable() {
        //Arrange
        Mockito.when(referralRepository.findByEmail(anyString())).thenReturn(referral);
        user.setDefaultWallet(null);

        // Act
        referralService.createReferral(user, "ilkoiliev123@abv.bg");
    }



    @Test
    public void createReferral_ShouldCallRepository() {
        //Arrange, Act
        referralService.createReferral(user, referral.getEmail());
        //Assert
        Mockito.verify(referralRepository, times(1)).createReferral(user,referral.getEmail());
    }

}

