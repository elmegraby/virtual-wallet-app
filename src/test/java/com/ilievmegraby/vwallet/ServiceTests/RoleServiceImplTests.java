package com.ilievmegraby.vwallet.ServiceTests;


import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.repositories.RoleRepository;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import com.ilievmegraby.vwallet.services.RoleServiceImpl;
import com.ilievmegraby.vwallet.services.UserServiceImpl;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTests {

    private Role role;

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Before
    public void before() {
        this.role = ObjectFactory.getRole();
    }

    @Test
    public void getByName_ShouldCallRepository() {
        //Arrange, Act
        roleService.getByName(role.getRole());
        //Assert
        Mockito.verify(roleRepository, times(1)).getByName(role.getRole());
    }

}
