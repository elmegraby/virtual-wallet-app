
package com.ilievmegraby.vwallet.ServiceTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.connectors.CurrencyConverterConnector;
import com.ilievmegraby.vwallet.connectors.PaymentProviderConnector;
import com.ilievmegraby.vwallet.exceptions.APIConnectionException;

import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.repositories.ExchangeRateRepository;
import com.ilievmegraby.vwallet.repositories.RoleRepository;
import com.ilievmegraby.vwallet.services.ExchangeRateServiceImpl;
import com.ilievmegraby.vwallet.services.RoleServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.swing.text.html.parser.Entity;

import static org.mockito.ArgumentMatchers.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceImplTests {

    private ExchangeRate exchangeRate;
    private TransactionDTO transactionDTO;
    private Transaction transaction;
    private Wallet wallet;

    @Mock
    ExchangeRateRepository exchangeRateRepository;

    @Mock
    CurrencyConverterConnector currencyConverterAPIConnector;

    @Mock
    EntityMapper entityMapper;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    ExchangeRateServiceImpl exchangeRateService;

    @Before
    public void before() {
        this.exchangeRate = ObjectFactory.getExchangeRate();
        this.transactionDTO = ObjectFactory.getTransactionDTO();
        this.transaction = ObjectFactory.getTransaction();
        this.wallet = ObjectFactory.getRecipientWallet();
    }

    @Test
    public void getExchangeRate_ShouldCallRepository() {
        //Arrange
        Mockito.when(currencyConverterAPIConnector.getLiveExchangeRate(CurrencyCode.EUR, CurrencyCode.BGN))
                .thenThrow(new APIConnectionException("No connection to API"));
        Mockito.when(exchangeRateRepository.getConversionRate(any(), any())).thenReturn(exchangeRate);

        //Act
        exchangeRateService.getExchangeRate(CurrencyCode.EUR, CurrencyCode.BGN);
        //Assert
        Mockito.verify(exchangeRateRepository, times(1)).getConversionRate(CurrencyCode.EUR, CurrencyCode.BGN);
    }

    @Test
    public void updateRates_ShouldCallRepository() {
        //Arrange
        Mockito.when(currencyConverterAPIConnector.getLiveExchangeRate(CurrencyCode.EUR, CurrencyCode.BGN)).thenReturn(exchangeRate);

        // Act
        exchangeRateService.updateRates();
        //Assert
        Mockito.verify(exchangeRateRepository, times(1)).updateRates(any(),any(), any(),any(),any(),any());
    }

//    @Test
//    public void ratesUpToDate_ShouldCallRepository() {
//        //Arrange, Act
//        exchangeRateService.ratesUpToDate();
//        //Assert
//        Mockito.verify(exchangeRateRepository, times(1)).ratesUpToDate();
//    }


    @Test
    public void getExchangeRateInfoMessage_ShouldReturnResponseEntity() {
        //Arrange
        Mockito.when(entityMapper.getTransactionFromDTO(transactionDTO)).thenReturn(transaction);
        exchangeRate.setUpToDate(true);
        // Act
        ResponseEntity<String> entity = exchangeRateService.getExchangeRateInfoMessage(transactionDTO);
        //Assert
        Assert.assertEquals(503, entity.getStatusCodeValue());
    }

    @Test
    public void getExchangeRateInfoMessage_ShouldReturnResponseEntity_andDifferentMessage_whenCurrenciesNotEqual() {
        //Arrange
        transaction.setRecipientWallet(wallet);
        Mockito.when(entityMapper.getTransactionFromDTO(transactionDTO)).thenReturn(transaction);
        exchangeRate.setUpToDate(true);
        Mockito.when(currencyConverterAPIConnector.getLiveExchangeRate(any(), any())).thenReturn(exchangeRate);
        // Act
        ResponseEntity<String> entity = exchangeRateService.getExchangeRateInfoMessage(transactionDTO);
        //Assert
        Assert.assertEquals(200, entity.getStatusCodeValue());
    }

    @Test
    public void convert_ShouldReturnSameAmount_WhenCurrenciesAreSame() {
        //Arrange
        // Act
        BigDecimal result = exchangeRateService.convert(BigDecimal.valueOf(10), CurrencyCode.EUR, CurrencyCode.EUR);
        //Assert
        Assert.assertEquals(BigDecimal.valueOf(10), result);
    }

    @Test
    public void getExchangeRateInfoMessage_ShouldReturnResponseEntity_andDifferentMessage_whenCurrenciesNotEqual_Outdated() {
        //Arrange
        exchangeRate.setUpToDate(false);
        transaction.setRecipientWallet(wallet);
        Mockito.when(entityMapper.getTransactionFromDTO(transactionDTO)).thenReturn(transaction);
        Mockito.when(currencyConverterAPIConnector.getLiveExchangeRate(any(), any())).thenReturn(exchangeRate);
        // Act
        ResponseEntity<String> entity = exchangeRateService.getExchangeRateInfoMessage(transactionDTO);
        //Assert
        Assert.assertEquals(200, entity.getStatusCodeValue());
    }

    @Test
    public void convert1_ShouldReturnSameAmount_WhenCurrenciesAreSame() {
        //Arrange
        Mockito.when(currencyConverterAPIConnector.getLiveExchangeRate(any(), any())).thenReturn(exchangeRate);
        // Act
        BigDecimal result = exchangeRateService.convert(BigDecimal.valueOf(1), CurrencyCode.EUR, CurrencyCode.BGN);
        //Assert
        Assert.assertEquals(BigDecimal.valueOf(1.56), result.setScale(2, RoundingMode.CEILING));
    }


}
