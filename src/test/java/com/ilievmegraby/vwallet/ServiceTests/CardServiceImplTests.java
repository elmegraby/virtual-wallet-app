
package com.ilievmegraby.vwallet.ServiceTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.Role;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import com.ilievmegraby.vwallet.services.CardServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CardServiceImplTests {
    private CardDetails validCard;
    private CardDetails invalidCard;
    private List<Role> roles = new ArrayList<>();
    private User user;
    private User secondUser;
    private String exceptionMessage;
    private List<CardDetails> cardDetailsList = new ArrayList<>();

    @Mock
    CardRepository cardRepository;

    @InjectMocks
    CardServiceImpl cardService;

    @Before
    public void before() {
        this.validCard = ObjectFactory.getCardDetails();
        this.exceptionMessage = ObjectFactory.MESSAGE_BODY;
        this.cardDetailsList.add(validCard);
        this.user = ObjectFactory.getUser();
        this.secondUser = ObjectFactory.getSecondUser();
        this.roles= new ArrayList<>();
        roles.add(ObjectFactory.getRole());
        roles.add(ObjectFactory.getAdminRole());
        user.setRoles(roles);
    }


    @Test
    public void getById_should_return_card_when_present() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(validCard);
        //Act
        CardDetails returned = cardService.getById(user,1);
        //Assert
        Assert.assertEquals(returned, validCard);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_should_throw_when_card_doesnt_exist() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenThrow(new EntityNotFoundException(exceptionMessage));
        //Act
        cardService.getById(user,1);
    }

    @Test
    public void getByUsername_should_return_card_when_present() {
        //Arrange
        when(cardRepository.getByUsername(anyString())).thenReturn(cardDetailsList);
        //Act
        List<CardDetails> returned = cardService.getByUsername(user, validCard.getCardHolderName());
        //Assert
        Assert.assertEquals(returned, cardDetailsList);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByUsername_should_throw_when_card_doesnt_exist() {
        //Arrange
        when(cardRepository.getByUsername(anyString())).thenThrow(new EntityNotFoundException(exceptionMessage));
        //Act
        cardService.getByUsername(user, validCard.getCardHolderName());
    }

    @Test(expected = UnauthorizedActionException.class)
    public void getByUsername_should_throwException_when_user_not_authorized_or_card_assigned_to_another_user() {
        //Arrange, act
        cardService.getByUsername(secondUser, user.getUserName());
    }


    @Test
    public void create_should_call_repository() {
        //Act
        cardService.create(validCard);
        //Assert
        Mockito.verify(cardRepository, times(1)).create(validCard);
    }

    @Test
    public void create_should_return_card_after_creating() {
        //Arrange
        when(cardRepository.create(validCard)).thenReturn(validCard);
        //Act
        CardDetails created = cardService.create(validCard);
        //Assert
        Assert.assertEquals(created, validCard);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_should_throw_when_card_exists() {
        //Arrange
        when(cardRepository.create(validCard)).thenThrow(new DuplicateEntityException(exceptionMessage));
        //Act
        cardService.create(validCard);
    }

    @Test
    public void update_should_call_repository() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(validCard);
        //Act
        cardService.update(user,validCard);
        //Assert
        Mockito.verify(cardRepository, times(1)).update(validCard);
    }


    @Test
    public void update_should_return_card_after_updating() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(validCard);
        when(cardRepository.update(validCard)).thenReturn(validCard);
        //Act
        CardDetails updated = cardService.update(user,validCard);
        //Assert
        Assert.assertEquals(updated, validCard);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_should_throw_when_card_doesnt_exists() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(validCard);
        when(cardRepository.update(validCard)).thenThrow(new EntityNotFoundException(exceptionMessage));
        //Act
        cardService.update(user,validCard);
    }

    @Test(expected = UnauthorizedActionException.class)
    public void update_should_throwException_when_user_not_authorized_or_card_assigned_to_another_user() {
        //Arrange
        when(cardRepository.getById(anyInt())).thenReturn(validCard);
        List<Role> rolesUser = new ArrayList<>();
        rolesUser.add(ObjectFactory.getRole());
        user.setRoles(rolesUser);
        validCard.setUser(user);
        //Act
        cardService.update(secondUser, validCard);
    }

    @Test(expected = EntityNotFoundException.class)
    public void delete_should_throwException_when_cardId_doesnt_exist() {
        //Arrange
        when(cardService.getById(user, anyInt())).thenReturn(validCard);
        doThrow(new EntityNotFoundException(exceptionMessage)).when(cardRepository).delete(any());
        validCard.setUser(ObjectFactory.getUser());
        cardService.delete(validCard.getId(), user);
        //Act
        cardService.delete(1, user);
    }



    @Test(expected = UnauthorizedActionException.class)
    public void delete_should_throwException_when_user_not_authorized_or_card_assigned_to_another_user() {
        //Arrange
        when(cardRepository.getById(1)).thenReturn(validCard);
        List<Role> rolesUser = new ArrayList<>();
        rolesUser.add(ObjectFactory.getRole());
        user.setRoles(rolesUser);
        validCard.setUser(user);
        //Act
        cardService.delete(1, secondUser);
    }



    @Test
    public void deleteCard_ShouldCallRepository() {
        //Arrange, Act
        when(cardService.getById(user, anyInt())).thenReturn(validCard);
        validCard.setUser(ObjectFactory.getUser());
        cardService.delete(validCard.getId(), user);
        //Assert
        Mockito.verify(cardRepository, times(1)).delete(any());
    }
}

