package com.ilievmegraby.vwallet.ServiceTests;


import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.ImageReadException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.helperClasses.ImageFileValidator;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.repositories.UserRepository;
import com.ilievmegraby.vwallet.services.UserServiceImpl;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    private User user;
    private UserRegistrationDTO userRegistrationDTO;
    private UserUpdateDTO userUpdateDTO;

    @Mock
    UserRepository userRepository;

    @Mock
    ReferralService referralService;

    @Mock
    EntityMapper entityMapper;

    @Mock
    ImageFileValidator imageFileValidator;

    @InjectMocks
    UserServiceImpl userService;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void before() {
        this.user = ObjectFactory.getUser();
        this.userRegistrationDTO = ObjectFactory.getRegistrationDTO();
        this.userUpdateDTO = ObjectFactory.getUpdateDTO();
    }


    @Test
    public void getAllUsers_ShouldReturnEmptyList_WhenNoUsersAdded() {
        //Arrange
        when(userRepository.getAllUsers(anyInt())).thenReturn(new ArrayList<>());
        //Act
        userService.getAllUsers(1);
        //Assert
        Assert.assertTrue(userRepository.getAllUsers(1).isEmpty());
    }

    @Test
    public void getAllUsers_ShouldCallRepository() {
        //Arrange, Act
        userService.getAllUsers(1);
        //Assert
        Mockito.verify(userRepository, times(1)).getAllUsers(1);
    }

    @Test
    public void getById_ShouldCallRepository() {
        //Arrange, Act
        userService.getById(anyInt());
        //Assert
        Mockito.verify(userRepository, times(1)).getById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_ShouldThrowException_WhenUserNotFound() {
        //Arrange
        when(userRepository.getById(anyInt())).thenThrow(new EntityNotFoundException("User not found"));
        //Act
        userService.getById(anyInt());
    }

    @Test
    public void getByName_ShouldCallRepository() {
        //Arrange, Act
        userService.getByName(anyString());
        //Assert
        Mockito.verify(userRepository, times(1)).getByName(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByName_ShouldThrowException_WhenUserNotFound() {
        //Arrange
        when(userRepository.getByName(anyString())).thenThrow(new EntityNotFoundException("User not found"));
        //Act
        userService.getByName(anyString());
    }


    @Test
    public void getByPhone_ShouldCallRepository() {
        //Arrange, Act
        userService.getByPhone(anyString());
        //Assert
        Mockito.verify(userRepository, times(1)).getByPhone(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByPhone_ShouldThrowException_WhenUserNotFound() {
        //Arrange
        when(userRepository.getByPhone(anyString())).thenThrow(new EntityNotFoundException("User not found"));
        //Act
        userService.getByPhone(anyString());
    }

    @Test
    public void getByEmail_ShouldCallRepository() {
        //Arrange, Act
        userService.getByEmail(anyString());
        //Assert
        Mockito.verify(userRepository, times(1)).getByEmail(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByEmail_ShouldThrowException_WhenUserNotFound() {
        //Arrange
        when(userRepository.getByEmail(anyString())).thenThrow(new EntityNotFoundException("User not found"));
        //Act
        userService.getByEmail(anyString());
    }

    @Test
    public void getUser_ShouldCallRepository() {
        //Arrange, Act
        userService.getUser(anyString());
        //Assert
        Mockito.verify(userRepository, times(1)).getUser(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUser_ShouldThrowException_WhenUserNotFound() {
        //Arrange
        when(userRepository.getUser(anyString())).thenThrow(new EntityNotFoundException("User not found"));
        //Act
        userService.getUser(anyString());
    }

    @Test
    public void createUser_ShouldCallRepository() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(false);
        when(userRepository.emailAlreadyExists(anyString())).thenReturn(false);
        when(userRepository.phoneAlreadyExists(anyString())).thenReturn(false);
        // Act
        userService.createUser(userRegistrationDTO);
        //Assert
        Mockito.verify(userRepository, times(1)).createUser(any()); //
    }



    @Test(expected = DuplicateEntityException.class)
    public void createUser_ShouldThrowException_WhenUserNameAlreadyInUse() {
        //Arrange
        when(userRepository.userExists(anyString()))
                .thenReturn(true);
        //Act
        userService.createUser(userRegistrationDTO);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_ShouldThrowException_WhenPhoneAlreadyInUse() {
        //Arrange
        when(userRepository.phoneAlreadyExists(anyString()))
                .thenReturn(true);
        //Act
        userService.createUser(userRegistrationDTO);
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_ShouldThrowException_WhenEmailAlreadyInUse() {
        //Arrange
        when(userRepository.emailAlreadyExists(anyString()))
                .thenReturn(true);

        //Act
        userService.createUser(userRegistrationDTO);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUser_ShouldThrowException_WhenUserNotFound(){
        //Arrange,Act
        userService.updateUser(user,userUpdateDTO);
    }

    @Test
    public void updateUser_ShouldCallRepository() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(true);
        when(userRepository.emailAlreadyExists(anyString())).thenReturn(true);
        when(userRepository.phoneAlreadyExists(anyString())).thenReturn(true);
        when(imageFileValidator.isValid(any())).thenReturn(true);

        // Act
        userService.updateUser(user, userUpdateDTO);
        //Assert
        Mockito.verify(userRepository, times(1)).updateUser(any());
    }

    @Test(expected = ImageReadException.class)
    public void updateUser_ShouldThrowError_whenImageNotValid() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(true);
        when(userRepository.emailAlreadyExists(anyString())).thenReturn(true);
        when(userRepository.phoneAlreadyExists(anyString())).thenReturn(true);
        when(imageFileValidator.isValid(any())).thenReturn(false);

        // Act
        userService.updateUser(user, userUpdateDTO);
    }


    @Test(expected = EntityNotFoundException.class)
    public void deleteUser_ShouldThrowException_WhenUserNotFound(){
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(false);

        //Act
        userService.deleteUser(anyString());
    }

    @Test
    public void deleteUser_ShouldCallRepository() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(true);

        // Act
        userService.deleteUser(user.getUserName());
        //Assert
        Mockito.verify(userRepository, times(1)).deleteUser(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void blockUser_ShouldThrowException_WhenUserNotFound(){
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(false);
        //Act
        userService.blockUser(anyString());
    }

    @Test
    public void blockUser_ShouldCallRepository() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(true);

        // Act
        userService.blockUser(user.getUserName());
        //Assert
        Mockito.verify(userRepository, times(1)).blockUser(anyString());
    }

    @Test
    public void activateAccount_ShouldCallRepository() {
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(true);

        //  Act
        userService.activateAccount("ilko123");
        //Assert
        Mockito.verify(userRepository, times(1)).activateAccount(anyString());
    }

    @Test(expected = EntityNotFoundException.class)
    public void activateAccount_ShouldThrowException_WhenUserNotFound(){
        //Arrange
        when(userRepository.userExists(anyString())).thenReturn(false);
        //Act
        userService.activateAccount("ilko123");
    }

    @Test
    public void getNumberOfPages_ShouldCallRepository() {
        // Arrange, Act
        userService.getNumberOfPages();
        //Assert
        Mockito.verify(userRepository, times(1)).getNumberOfPages();
    }

    @Test
    public void getNumberOfPagesAdmin_ShouldCallRepository() {
        // Arrange, Act
        userService.getNumberOfPagesAdmin();
        //Assert
        Mockito.verify(userRepository, times(1)).getNumberOfPagesAdmin();
    }
}
