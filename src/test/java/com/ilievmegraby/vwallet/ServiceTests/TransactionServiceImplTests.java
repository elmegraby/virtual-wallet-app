
package com.ilievmegraby.vwallet.ServiceTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.InsufficientFundsException;
import com.ilievmegraby.vwallet.helperClasses.EntityMapper;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.repositories.TransactionRepository;
import com.ilievmegraby.vwallet.services.TransactionServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTests {
    private Transaction transaction;
    private TransactionDTO transactionDTO;
    private String exceptionMessage;


    @Mock
    TransactionRepository repository;

    @Mock
    EntityMapper entityMapper;

    @InjectMocks
    TransactionServiceImpl service;

    @Before
    public void before() {
        transaction = ObjectFactory.getTransaction();
        exceptionMessage = ObjectFactory.MESSAGE_BODY;
        transactionDTO = ObjectFactory.getTransactionDTO();
    }

    @Test
    public void getAll_should_return_all_transactions_when_present(){
        //Arrange
        List<Transaction> transactionsList = Arrays.asList(transaction);
        when(repository.getAll()).thenReturn(transactionsList);
        //Act
        List<Transaction> returned = service.getAll();
        //Assert
        Assert.assertEquals(transactionsList.size(), returned.size());
    }

    @Test
    public void getById_should_return_transaction_when_present(){
    //Arrange
    when(repository.getById(transaction.getId())).thenReturn(transaction);
    //Act
    Transaction returned = service.getById(transaction.getId());
    //Assert
     Assert.assertEquals(transaction, returned);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_should_throw_when_transaction_doesnt_exists(){
        //Arrange
        when(repository.getById(anyInt())).thenThrow(new EntityNotFoundException(exceptionMessage));
        //Act and assert
        service.getById(1);
    }



   @Test
    public void filter_should_call_repository(){ //to be changed !!! testing with any weakens the test
        //Arrange
       // when(repository.filter(any(), any(), any(), any(),any(), any(),any())).thenReturn(transactionsList);
        //Act
        service.filter("iliev", "outgoing","12/12/19 - 12/12/20","iliev","amntDesc", 1);
        //Assert
       verify(repository, times(1)).filter(anyString(), anyString(), any(),any(), anyString(),anyString(), anyInt());
    }




    @Test
    public void create_ShouldCallRepository(){
        //Arrange
        when(repository.create(transaction)).thenReturn(transaction);
        when(entityMapper.getTransactionFromDTO(transactionDTO)).thenReturn(transaction);
        //Act
        Transaction returned = service.create(transactionDTO);
        //Assert
        Assert.assertEquals(transaction, returned);
    }

    @Test(expected = InsufficientFundsException.class)
    public void create_ShouldThrowException_whenFundsInsufficient(){
        //Arrange
        transaction.setAmount(BigDecimal.valueOf(120));
        when(entityMapper.getTransactionFromDTO(transactionDTO)).thenReturn(transaction);
        //Act
        service.create(transactionDTO);

    }

    @Test
    public void process_should_call_repository(){
        //Act
        service.processTransaction(transaction);
        //Assert
        verify(repository, times(1)).processTransaction(transaction);
    }

    @Test(expected = InsufficientFundsException.class)
    public void process_ShouldThrowException_whenFundsInsufficient(){
        //Arrange
        transaction.setAmount(BigDecimal.valueOf(120));
        //Act
        service.processTransaction(transaction);

    }

    @Test
    public void getUserTransactions_shouldCallRepository(){
        //Act
        service.getUserTransactions("iliev-telerik");
        //Assert
        verify(repository, times(1)).getUserTransactions(anyString());
    }

    @Test
    public void filter_shouldCallRepository(){
        //Act

        service.filter("iliev-telerik"
                , "outgoing"
                , "12/09/20 - 11/10/2020"
                , "megraby"
                , "amount desc"
                , 1);

        //Assert
        verify(repository, times(1)).filter(anyString(), anyString(), any(), any(), anyString(), anyString(),anyInt());
    }

    @Test
    public void getNumberOfPages_shouldCallRepository(){
        //Act

        service.getNumberOfPages("iliev-telerik"
                , "outgoing"
                , "12/09/20 - 11/10/2020"
                , "megraby"
                , "amount desc"
                , 1);

        //Assert
        verify(repository, times(1)).getNumberOfPages(anyString(), anyString(), any(), any(), anyString(), anyString(),anyInt());
    }
}

