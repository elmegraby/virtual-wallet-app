package com.ilievmegraby.vwallet.frontControllersTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.TransactionService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.EmailConfirmationTokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationControllerTests {

    private User user;
    private UserRegistrationDTO userRegistrationDTO;
    private ConfirmationToken confirmationToken;

    @MockBean
    UserService userService;

    @MockBean
    EmailConfirmationTokenService emailConfirmationTokenService;

    @MockBean
    EmailSenderService emailSenderService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.user= ObjectFactory.getUser();
        this.userRegistrationDTO = ObjectFactory.getRegistrationDTO();
        this.confirmationToken = ObjectFactory.getConfirmationToken();
    }

    @Test
    public void registrationController_shouldReturn_registrationPage_andStatusOK() throws Exception{

        mockMvc.perform(get("/registration", 1))
                .andExpect(status().isOk()).andExpect(view().name("signup"));
    }

    @Test
    public void registerUser_shouldReturn_registrationVerificationPage_andStatusOK() throws Exception{
        Mockito.when(userService.createUser(any())).thenReturn(user);

        mockMvc.perform(post("/registration", 1))
                .andExpect(status().isOk()).andExpect(view().name("verify-registration"));
    }

    @Test
    public void confirmAccount_shouldReturn_registrationVerificationPage_andStatusOK() throws Exception{
        Mockito.when(emailConfirmationTokenService.findByConfirmationToken(anyString())).thenReturn(confirmationToken);
        Mockito.when(userService.getByEmail(anyString())).thenReturn(user);

        (mockMvc.perform(post("/registration/confirm-account", 1).param("token","3xdsfd")))
                .andExpect(status().isOk()).andExpect(view().name("registration-verified"));
    }


    @Test
    public void confirmAccount_shouldReturn_errorPage_whenTokenInvalid() throws Exception{
        Mockito.when(emailConfirmationTokenService.findByConfirmationToken(anyString())).thenReturn(null);
       // Mockito.when(userService.getByEmail(anyString())).thenReturn(user);

        (mockMvc.perform(post("/registration/confirm-account", 1).param("token","3xdsfd")))
                .andExpect(status().isOk()).andExpect(view().name("error"));
    }

    @Test
    public void setUpWallet_shouldReturn_createWalletPage_andStatusOK() throws Exception{


        (mockMvc.perform(get("/registration/setup-wallet", 1)))
                .andExpect(status().isOk()).andExpect(view().name("after-reg-setup-wallet"));
    }
}
