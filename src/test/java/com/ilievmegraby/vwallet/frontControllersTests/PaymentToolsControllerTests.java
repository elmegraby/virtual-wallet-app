package com.ilievmegraby.vwallet.frontControllersTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.BalanceLoadDTO;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.CardService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.WalletService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.ilievmegraby.vwallet.ObjectFactory.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsIterableContaining.hasItem;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//@WebMvcTest
public class PaymentToolsControllerTests {
    private User user;
    private Principal mockPrincipal = mock(Principal.class);
    private CardDetails cardDetails;
    private CardDetails invalidCard;
    private Wallet wallet;
    private WalletDTO postWalletDTO;
    private BalanceLoadDTO balanceLoadDTO;
    private BalanceLoad balanceLoad;

    @MockBean
    private UserService userService;

    @MockBean
    private WalletService walletService;

    @MockBean
    private CardService cardService;

    @MockBean
    private BalanceLoadService balanceLoadService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Before
    public void before() {
//        mockMvc = webAppContextSetup(wac).build();
        user = getUser();
        cardDetails = getCardDetails();
        wallet = getWallet();
        invalidCard = getInvalidCard();
        postWalletDTO = getWalletDTO();
        balanceLoadDTO = getBalanceLoadDTO();
        balanceLoad = getBalanceLoad();
    }


    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void getHome_should_return_main_cardsAndWallets_page() throws Exception {
        //Arrange
        Set<Wallet> wallets = new HashSet<>();
        wallets.add(wallet);
        user.setWallets(wallets);
        user.setDefaultWallet(wallet);
        when(cardService.getByUsername(any(), anyString())).thenReturn(Arrays.asList(cardDetails));
        when(userService.getByName(anyString())).thenReturn(user);
        // act, assert
        mockMvc.perform(get("/payment-tools")).
                andExpect(model().attribute("cards", Arrays.asList(cardDetails))).
                andExpect(model().attribute("wallets", hasItem(allOf(hasProperty("name", is(wallet.getName())))))).
                andExpect(model().attribute("user", hasProperty("userName", is(user.getUserName())))).
                andExpect(status().isOk()).andExpect(view().name("cards-wallets"));
    }

   /* @Test
    public void getHome_should_redirect_to_login_when_unauthenticated() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(get("/payment-tools"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

    */


   /* @Test
    public void create_card_should_redirect_to_login_when_unauthenticated() throws Exception {
        mockMvc.perform(post("/payment-tools/newcard/")).andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

    */


//    @Test
//    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
//    public void create_card_should_redirect_to_error_when_card_invalid() throws Exception {
//      mockMvc.perform(post("/payment-tools/newcard").content(asJsonString(invalidCard))
//              .principal(mockPrincipal)
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//              .andExpect(status().is3xxRedirection())
//              .andExpect(view().name("redirect:/error"));
//    }

    /*@Test
    public void getCardUpdateForm_should_redirect_to_login_when_unauthorized() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(get("/payment-tools/card-update/{id}", 1)).andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

     */

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void updateCard_should_return_mainPage_if_successful() throws Exception {
        //Arrange
        when(userService.getByName(anyString())).thenReturn(user);
        when(cardService.update(user, cardDetails)).thenReturn(cardDetails);
        //Act
        mockMvc.perform(post("/payment-tools/card-update").content(asJsonString(cardDetails))
//         .principal(mockPrincipal)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools"));
        //Assert
    }


    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void create_card_should_redirect_to_payment_tools_if_successful() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(post("/payment-tools/newcard").content(asJsonString(cardDetails))
                .principal(mockPrincipal)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools"));
    }

 /*   @Test
    public void delete_should_redirect_to_login_when_user_unauthorized() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(post("/payment-tools/card-delete/").param("id", "1")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

  */

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void delete_should_redirect_to_mainPage_if_successful() throws Exception {
        //Act, assert
        mockMvc.perform(post("/payment-tools/card-delete/").param("id", "1")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools"));

    }

   /* @Test
    public void createWallet_should_redirect_to_login_when_user_unauthorized() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(post("/payment-tools/newwallet/").content(asJsonString(postWalletDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

    */

//    @Test
//    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
//    public void create_Wallet_should_return_mainPage_when_successful() throws Exception {
//        //Arrange
//        when(userService.getByName(anyString())).thenReturn(user);
//        //Act, assert
//        mockMvc.perform(post("/payment-tools/newwallet/")).
//                andExpect(status().is3xxRedirection())
//                .andExpect(view().name("redirect:/payment-tools"));
//    }

   /* @Test
    public void topUpWallet_should_redirect_to_login_when_user_unauthorized() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(post("/payment-tools/topup/").content(asJsonString(balanceLoadDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

    */


 /*   @Test
    public void setDefaultWallet_should_redirect_to_login_when_user_unauthorized() throws Exception {
        //Arrange, act, assert
        mockMvc.perform(post("/payment-tools/setdefault/").param("walletId", "1")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

  */


    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void setDefaultWallet_should_return_mainPage_when_successful() throws Exception {
        //Arrange
        when(walletService.getById(anyInt())).thenReturn(wallet);
        when(userService.getByName(anyString())).thenReturn(user);

        //Act, assert
        mockMvc.perform(post("/payment-tools/setdefault").param("walletId", "1")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools"));
    }

 /*   @Test
    public void manageWallet_should_redirect_to_login_when_unauthorized() throws Exception {
        //Arrange
        when(walletService.getById(anyInt())).thenReturn(wallet);
        // Act, assert
        mockMvc.perform(get("/payment-tools/manage/{id}", 1)).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

  */

    @Test
    @WithMockUser(username = "iliev-telerik", roles = {"USER", "ADMIN"})
    public void manageWallet_should_return_walletPanel_when_user_authorized() throws Exception {
        //Arrange
        wallet.getUsers().add(user);
        wallet.setCreator(user);
        when(walletService.getById(anyInt())).thenReturn(wallet);
        when(userService.getByName(anyString())).thenReturn(user);

        //Act, assert
        mockMvc.perform(get("/payment-tools/manage/{id}", 1)).
                andExpect(model().attribute("users", hasItem(allOf(hasProperty("userName", is(user.getUserName())))))).
                andExpect(model().attribute("wallet", hasProperty("name", is(wallet.getName())))).
                andExpect(status().isOk())
                .andExpect(view().name("wallet-panel"));
    }


   /* @Test
    public void addMemberTOWallet_should_redirect_to_login_when_unauthorized() throws Exception {
        //Arrange
        // Act, assert
        mockMvc.perform(post("/payment-tools/manage/invite").param("walletId", "1")
                .param("user", "username")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

    */

    @Test
    @WithMockUser(username = "iliev-telerik", roles = {"USER", "ADMIN"})
    public void addMemberToWallet_should_redirect_towallet_panel_when_successful() throws Exception {
        //Arrange
        when(walletService.getById(anyInt())).thenReturn(wallet);
        when(userService.getByName(anyString())).thenReturn(user);

        //Act, assert
        mockMvc.perform(post("/payment-tools/manage/invite").param("walletId", "1")
                .param("user", "username")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools/manage/" + 1));
    }

/*    @Test
    public void removeMemberFromWallet_should_redirect_to_login_when_unauthorized() throws Exception {
        //Arrange
        // Act, assert
        mockMvc.perform(post("/payment-tools/manage/remove").param("walletId", "1")
                .param("user", "username")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
    }

 */

    @Test
    @WithMockUser(username = "iliev-telerik", roles = {"USER", "ADMIN"})
    public void removeMemberFromWallet_should_redirect_towallet_panel_when_successful() throws Exception {
        //Arrange
        when(walletService.getById(anyInt())).thenReturn(wallet);
        when(userService.getByName(anyString())).thenReturn(user);

        //Act, assert
        mockMvc.perform(post("/payment-tools/manage/remove").param("walletId", "1")
                .param("user", "username")).
                andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/payment-tools/manage/" + 1));
    }


}


