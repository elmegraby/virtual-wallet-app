package com.ilievmegraby.vwallet.frontControllersTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.ilievmegraby.vwallet.ObjectFactory.getCardDetails;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username="admin",roles={"USER","ADMIN"})
public class BalanceLoadControllersTests {

    private BalanceLoad balanceLoad;
    private User user;

    @MockBean
    UserService userService;

    @MockBean
    BalanceLoadService balanceLoadService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.balanceLoad = ObjectFactory.getBalanceLoad();
        this.user= ObjectFactory.getUser();
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showFilteredBalanceLoads_shouldReturn_BalanceLoads_andStatusOK() throws Exception{
        //Arrange
        List<BalanceLoad> loads = new ArrayList<>();
        loads.add(balanceLoad);
        user.setCards(new HashSet<>());
        user.setWallets(new HashSet<>());
        Mockito.when(userService.getByName("admin")).thenReturn(user);
        Mockito.when(balanceLoadService.filter(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(loads);
        Mockito.when(balanceLoadService.getNumberOfPages(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(1);

        //Act,Assert

        mockMvc.perform(get("/topup-history", 1).param("datesRange", "12/12 - 12/12")
                .param("toWallet", "MyWallet").param("cardUsed", "mycard").param("orderBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().isOk()).andExpect(view().name("topup-history-user"))
                 .andExpect(model().attribute("balanceloads",hasSize(1)))
                 .andExpect(model().attribute("balanceloads", hasItem(allOf(hasProperty("description", is("Add money to virtual wallet."))))))
                 .andExpect(model().attribute("pageCount", is(1)))
                 .andExpect(model().attribute("user",hasProperty("userName",is("iliev-telerik"))));
    }

    /*@Test
    public void showFilteredBalanceLoads_shouldReturn_LoginPage_whenUserUnauthorized() throws Exception{
        //Arrange, Act, Assert


        mockMvc.perform(get("/topup-history", 1).param("datesRange", "12/12 - 12/12")
                .param("toWallet", "MyWallet").param("cardUsed", "mycard").param("orderBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

     */
}
