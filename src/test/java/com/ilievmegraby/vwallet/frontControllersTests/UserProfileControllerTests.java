package com.ilievmegraby.vwallet.frontControllersTests;

import com.google.gson.Gson;
import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.EmailConfirmationTokenService;
import com.ilievmegraby.vwallet.services.verificationServices.ReferralService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserProfileControllerTests{

    private User user;
    private UserUpdateDTO userUpdateDTO;

    @MockBean
    UserService userService;

    @MockBean
    ReferralService referralService;

    @MockBean
    EmailSenderService emailSenderService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.user= ObjectFactory.getUser();
        this.userUpdateDTO = ObjectFactory.getUpdateDTO();

    }

    @Test
    @WithMockUser
    public void showProfile_shouldReturnProfilePage_andStatusOK() throws Exception{
        Mockito.when(userService.getByName(anyString())).thenReturn(user);

        mockMvc.perform(get("/profile", 1))
                .andExpect(status().isOk()).andExpect(view().name("profile"));
    }

   /* @Test
    public void showProfile_shouldReturnLoginPage_whenUserUnauthenticated() throws Exception{

        mockMvc.perform(get("/profile", 1))
                .andExpect(status().isOk()).andExpect(view().name("login"));
    }

    */

    @Test
    @WithMockUser
    public void updateProfile_shouldReturnProfile_andStatus3xx() throws Exception{

        Gson gson = new Gson();
        String dtoToJson = gson.toJson(userUpdateDTO);

        mockMvc.perform(post("/profile/update", 1).contentType(MediaType.APPLICATION_JSON).content(dtoToJson))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/profile"));
    }


    @Test
    @WithMockUser
    public void refer_shouldReturnStatusOK() throws Exception{

        mockMvc.perform(post("/profile/refer", 1))
                .andExpect(status().isOk()).andExpect(view().name("referral-success"));
    }
}
