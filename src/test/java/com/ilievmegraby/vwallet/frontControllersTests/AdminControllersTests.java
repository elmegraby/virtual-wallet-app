package com.ilievmegraby.vwallet.frontControllersTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.configuration.SecurityConfiguration;
import com.ilievmegraby.vwallet.controllers.frontControllers.AdminController;
import com.ilievmegraby.vwallet.models.BalanceLoad;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.services.BalanceLoadService;
import com.ilievmegraby.vwallet.services.TransactionService;
import com.ilievmegraby.vwallet.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username="admin",roles={"USER","ADMIN"})
public class AdminControllersTests {
    private User user;
    private Transaction transaction;
    private BalanceLoad balanceLoad;

    @MockBean
    UserService userService;

    @MockBean
    TransactionService transactionService;

    @MockBean
    BalanceLoadService balanceLoadService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.user= ObjectFactory.getUser();
        this.transaction = ObjectFactory.getTransaction();
        this.balanceLoad = ObjectFactory.getBalanceLoad();
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showAdminPanel_shouldReturn_allUsers_onFirstPage_andStatusOK() throws Exception{
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.getAllUsers(anyInt())).thenReturn(users);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(userService.getNumberOfPagesAdmin()).thenReturn(1);

        mockMvc.perform(get("/admin", 1))
                .andExpect(status().isOk()).andExpect(view().name("admin-users"))
                .andExpect(model().attribute("users", hasItem(allOf(hasProperty("userName", is("iliev-telerik"))))))
                .andExpect(model().attribute("pageCount", is(1))).
                andExpect(model().attribute("user",hasProperty("userName", is("iliev-telerik")) ));
    }


   /* @Test
    public void showAdminPanel_shouldReturn_loginPage_whenUser_Unauthenticated() throws Exception{
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.getAllUsers(anyInt())).thenReturn(users);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(userService.getNumberOfPagesAdmin()).thenReturn(1);

        mockMvc.perform(get("/admin", 1))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

    */

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void findUser_shouldReturn_User_whenFound() throws Exception{
        Mockito.when(userService.getUser(anyString())).thenReturn(user);

        mockMvc.perform(get("/admin/finduser", 1).param("user","iliev-telerik"))
                .andExpect(status().isOk()).andExpect(view().name("admin-users"))
                .andExpect(model().attribute("users", hasItem(allOf(hasProperty("userName", is("iliev-telerik"))))));
    }



  /*  @Test
    public void findUser_shouldReturn_loginPage_whenUser_Unauthenticated() throws Exception{
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.getAllUsers(anyInt())).thenReturn(users);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(userService.getNumberOfPages()).thenReturn(1);

        mockMvc.perform(get("/admin/finduser", 1).param("user","iliev-telerik"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

   */

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showFilteredTransactions_shouldReturn_transactions_andStatusOK() throws Exception{
        //Arrange
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(transactionService.filter(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(transactions);
        Mockito.when(transactionService.getNumberOfPages(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(1);


        mockMvc.perform(get("/admin/transactions", 1).param("datesRange", "12/12 - 12/12")
                .param("recipient", "MyWallet").param("sender", "mycard").param("sortBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().isOk()).andExpect(view().name("admin-transactions"))
                .andExpect(model().attribute("transactions",hasSize(1)))
                .andExpect(model().attribute("transactions", hasItem(allOf(hasProperty("description", is("pay a friend"))))))
                .andExpect(model().attribute("pageCount", is(1)));
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showFilteredBalanceLoads_shouldReturn_BalanceLoads_andStatusOK() throws Exception{
        //Arrange
        List<BalanceLoad> loads = new ArrayList<>();
        loads.add(balanceLoad);
        user.setCards(new HashSet<>());
        user.setWallets(new HashSet<>());
        Mockito.when(userService.getByName("admin")).thenReturn(user);
        Mockito.when(balanceLoadService.filter(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(loads);
        Mockito.when(balanceLoadService.getNumberOfPages(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(1);

        //Act,Assert

        mockMvc.perform(get("/admin/topup-history", 1).param("datesRange", "12/12 - 12/12")
                .param("toWallet", "MyWallet").param("cardUsed", "mycard").param("orderBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().isOk()).andExpect(view().name("admin-topup-history"))
                .andExpect(model().attribute("balanceloads",hasSize(1)))
                .andExpect(model().attribute("balanceloads", hasItem(allOf(hasProperty("description", is("Add money to virtual wallet."))))))
                .andExpect(model().attribute("pageCount", is(1)))
                .andExpect(model().attribute("user",hasProperty("userName",is("iliev-telerik"))));
    }

   /* @Test
    public void showFilteredBalanceLoads_shouldReturn_loginPage_whenUnauthenticated() throws Exception{
        //Arrange

        //Act,Assert

        mockMvc.perform(get("/admin/topup-history", 1).param("datesRange", "12/12 - 12/12")
                .param("toWallet", "MyWallet").param("cardUsed", "mycard").param("orderBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

    */

  /*  @Test
    public void showFilteredTransactions_shouldReturn_loginPage_whenUserUnauthenticated() throws Exception{
        //Arrange,Act,Assert


        mockMvc.perform(get("/admin/transactions", 1).param("datesRange", "12/12 - 12/12")
                .param("recipient", "MyWallet").param("sender", "mycard").param("sortBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

   */

    @Test
    public void activateUser_should_redirectToAdminPage() throws Exception{
        mockMvc.perform(post("/admin/activate/{username}", "iliev-telerik").param("user","iliev-telerik"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/admin"));
    }

    @Test
    public void blockUser_should_redirectToAdminPage() throws Exception{
        mockMvc.perform(post("/admin/block/{username}", "iliev-telerik").param("user","iliev-telerik"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/admin"));
    }

}
