package com.ilievmegraby.vwallet.frontControllersTests;

import com.google.gson.Gson;
import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailFactory;
import com.ilievmegraby.vwallet.helperClasses.EmailSenderService;
import com.ilievmegraby.vwallet.models.Transaction;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.Wallet;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.models.dtos.WalletDTO;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.ProfileState;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import com.ilievmegraby.vwallet.services.TransactionService;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.TransactionConfirmationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import static org.mockito.ArgumentMatchers.any;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ilievmegraby.vwallet.constants.MessageConstants.TRANSACTIONS_SENDER_NO_WALLET;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTests {

    private Transaction transaction;
    private TransactionDTO transactionDTO;
    private User user;
    private Wallet wallet;
    private TransactionConfirmationCode transactionConfirmationCode;

    @MockBean
    UserService userService;

    @MockBean
    TransactionService transactionService;

    @MockBean
    TransactionConfirmationService transactionConfirmationService;

    @MockBean
    ExchangeRateService exchangeRateService;

    @MockBean
    EmailSenderService emailSenderService;

    @MockBean
    EmailFactory emailFactory;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.user= ObjectFactory.getUser();
        this.transaction = ObjectFactory.getTransaction();
        this.wallet = ObjectFactory.getWallet();
        this.transactionDTO = ObjectFactory.getTransactionDTO();
        this.transactionConfirmationCode = ObjectFactory.getTCC();

    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showFilteredTransactions_shouldReturn_transactions_andStatusOK() throws Exception{
        //Arrange
        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(transactionService.filter(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(transactions);
        Mockito.when(transactionService.getNumberOfPages(anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyString()
                ,anyInt())).thenReturn(1);


        mockMvc.perform(get("/transactions", 1).param("datesRange", "12/12 - 12/12")
                .param("direction", "MyWallet").param("recipient", "mycard").param("sortBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().isOk()).andExpect(view().name("transaction-history"))
                .andExpect(model().attribute("transactions",hasSize(1)))
                .andExpect(model().attribute("transactions", hasItem(allOf(hasProperty("description", is("pay a friend"))))))
                .andExpect(model().attribute("pageCount", is(1)));
    }

   /* @Test
    public void showFilteredTransactions_shouldReturn_status3xx_whenUserunAuthorized() throws Exception{
        //Arrange


       //Act,Assert
        mockMvc.perform(get("/transactions", 1).param("datesRange", "12/12 - 12/12")
                .param("direction", "MyWallet").param("recipient", "mycard").param("sortBy", "amntDesc")
                .param("page", "1"))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

    */




    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showSendTransactions_shouldReturn_statusOK() throws Exception{
        //Arrange
        user.setDefaultWallet(wallet);
        user.setState(ProfileState.ACTIVE);
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(userService.getAllUsers(anyInt())).thenReturn(users);
        Mockito.when(userService.getNumberOfPages()).thenReturn(1);

        //Act,Assert
        mockMvc.perform(get("/transactions/new", 1))
                .andExpect(status().isOk()).andExpect(view().name("create-transaction"))
                .andExpect(model().attribute("users", hasSize(1)))
                .andExpect(model().attribute("user",hasProperty("userName", is("iliev-telerik"))))
                .andExpect(model().attribute("pageCount", is(1)));
    }

  /*  @Test
    public void showSendTransactions_shouldReturn_status3xx_andLoginPage_whenPrincipalIsNull() throws Exception{
        //Arrange,Act,Assert

        mockMvc.perform(get("/transactions/new", 1))
                .andExpect(status().is3xxRedirection()).andExpect(view().name("redirect:/login"));
    }

   */

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showSendTransactions_shouldReturn_createWalletPage_whenDefaultWalletIsNull() throws Exception{
        //Arrange,Act,Assert
        user.setDefaultWallet(null);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        mockMvc.perform(get("/transactions/new", 1))
                .andExpect(status().isOk()).andExpect(view().name("after-reg-setup-wallet"));
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void showSendTransactions_shouldReturn_accountBlockedPage_whenAccountIsBlocked() throws Exception{
        //Arrange
        user.setDefaultWallet(wallet);
        user.setState(ProfileState.BLOCKED);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);

        //Act,Assert
        mockMvc.perform(get("/transactions/new", 1))
                .andExpect(status().isOk()).andExpect(view().name("account-blocked"));
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void applyCode_shouldReturn_applyCodePage() throws Exception{
        //Arrange,Act,Assert
        mockMvc.perform(get("/transactions/apply-code", 1))
                .andExpect(status().isOk()).andExpect(view().name("apply-code"));
    }


//    @Test
//    @WithMockUser(username="admin",roles={"USER","ADMIN"})
//    public void newTransaction_shouldReturn_transactionSuccessPage_andStatusOk_whenSumUnderConstraint() throws Exception{
//        //Arrange
//        transaction.setWalletUsed(wallet);
//        user.setDefaultWallet(wallet);
//        Mockito.when(userService.getByName(anyString())).thenReturn(user);
//        Mockito.when(transactionService.create(any())).thenReturn(transaction);
//        Mockito.when(emailFactory.getTransactionEmail(anyString(),any())).thenReturn(new SimpleMailMessage());
//
//        Gson gson = new Gson();
//        String dtoToJson  = gson.toJson(transactionDTO);
//
//        //Act,Assert
//        mockMvc.perform(post("/transactions/new", 1).contentType(MediaType.APPLICATION_JSON).content(dtoToJson))
//                .andExpect(status().isOk()).andExpect(view().name("transaction-success"));
//    }


    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void newTransaction_shouldReturn_verifyTransactionPage_andStatusOk_whenSumOverConstraint() throws Exception{
        //Arrange
        transaction.setAmount(BigDecimal.valueOf(1100));
        user.setDefaultWallet(wallet);
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(transactionService.create(any())).thenReturn(transaction);
        Mockito.when(exchangeRateService.convert(any(), any(),any())).thenReturn(BigDecimal.valueOf(1000));

        Gson gson = new Gson();
        String dtoToJson  = gson.toJson(transactionDTO);

        //Act,Assert
        mockMvc.perform(post("/transactions/new", 1).contentType(MediaType.APPLICATION_JSON).content(dtoToJson))
                .andExpect(status().isOk()).andExpect(view().name("verify-transaction"));
    }


    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void verifyTransaction_shouldReturn_transactionSuccesspage_andStatusOk_whenTokenIsValid() throws Exception{
        //Arrange
        Mockito.when(transactionConfirmationService.findByCode(anyString())).thenReturn(transactionConfirmationCode);

        //Act,Assert
        mockMvc.perform(post("/transactions/verify-transaction", 1).param("code", "1dfw1332fds"))
                .andExpect(status().isOk()).andExpect(view().name("transaction-success"));
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void verifyTransaction_shouldReturn_errorPage_whenTokenInvalid() throws Exception{
        //Arrange
        Mockito.when(transactionConfirmationService.findByCode(anyString())).thenReturn(null);

        //Act,Assert
        mockMvc.perform(post("/transactions/verify-transaction", 1).param("code", "1dfw1332fds"))
                .andExpect(status().isOk()).andExpect(view().name("error"));
    }
}
