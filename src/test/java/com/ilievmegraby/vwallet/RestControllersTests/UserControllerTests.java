package com.ilievmegraby.vwallet.RestControllersTests;

import com.google.gson.Gson;
import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.models.dtos.UserRegistrationDTO;
import com.ilievmegraby.vwallet.models.dtos.UserUpdateDTO;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.services.UserService;
import com.ilievmegraby.vwallet.services.verificationServices.EmailConfirmationTokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class UserControllerTests {

    private User user;
    private UserRegistrationDTO userRegistrationDTO;
    private UserUpdateDTO userUpdateDTO;
    private ConfirmationToken confirmationToken;

    @MockBean
    UserService service;

    @MockBean
    EmailConfirmationTokenService emailConfirmationTokenService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before(){
        this.user= ObjectFactory.getUser();
        this.userRegistrationDTO = ObjectFactory.getRegistrationDTO();
        this.userUpdateDTO = ObjectFactory.getUpdateDTO();
        this.confirmationToken = ObjectFactory.getConfirmationToken();

    }


  /*  @Test
    public void getAll_shouldReturnStatusOK() throws Exception{
        //Arrange
        mockMvc.perform(get("/api/v1/users/getById/{id}", 1)).andExpect(status().isOk()).
                andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());
    }

   */


    @Test
    public void getById_ShouldReturnStatusOk_whenUserExists() throws Exception{
        //Arrange

        Mockito.when(service.getById(anyInt())).thenReturn(user);

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getById/{id}", 1)).andExpect(status().isOk()).
                andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());

    }

    @Test
    public void getById_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception{
        //Arrange
        Mockito.when(service.getById(anyInt())).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert
        mockMvc.perform(get("/api/v1/users/getById/{id}", 1)).andExpect(status().is4xxClientError());
       //  andExpect(jsonPath("$.userName").
         //value(user.getUserName())).andDo(print());

    }


    @Test
    public void getByName_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception {
        //Arrange
        Mockito.when(service.getByName(anyString())).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByUsername/{userName}", "john")).andExpect(status().is4xxClientError());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void getByName_ShouldReturnStatusOk_whenUserExists() throws Exception{
        //Arrange
        Mockito.when(service.getByName(anyString())).thenReturn(user);

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByUsername/{userName}", "john")).andExpect(status().isOk()).
        andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());

    }

    @Test
    public void getByPhone_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception {
        //Arrange
        Mockito.when(service.getByPhone(anyString())).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByPhone/{phone}", "+359877664")).andExpect(status().is4xxClientError());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void getByPhone_ShouldReturnStatusOk_whenUserExists() throws Exception{
        //Arrange
        Mockito.when(service.getByPhone(anyString())).thenReturn(user);

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByPhone/{phone}", "+359877664")).andExpect(status().isOk()).
                andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());

    }

    @Test
    public void getByEmail_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception {
        //Arrange
        Mockito.when(service.getByEmail(anyString())).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByEmail/{email}", "ilko@mail.bg")).andExpect(status().is4xxClientError());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void getByEmail_ShouldReturnStatusOk_whenUserExists() throws Exception{
        //Arrange
        Mockito.when(service.getByEmail(anyString())).thenReturn(user);

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getByEmail/{email}", "ilko@mail.bg")).andExpect(status().isOk()).
                andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());

    }

    @Test
    public void getUser_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception {
        //Arrange
        Mockito.when(service.getUser(anyString())).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/getUser/{input}", "iliev")).andExpect(status().is4xxClientError());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void getByUser_ShouldReturnStatusOk_whenUserExists() throws Exception{
        //Arrange
        Mockito.when(service.getUser(anyString())).thenReturn(user);

        // Act, Assert
        mockMvc.perform(get("/api/v1/users/getUser/{input}", "iliev")).andExpect(status().isOk()).
                andExpect(jsonPath("$.userName").value(user.getUserName())).andDo(print());

    }

    @Test
    public void createUser_ShouldReturnStatus4xx_whenUserExists() throws Exception {
        //Arrange
        Mockito.when(service.createUser(userRegistrationDTO)).thenThrow(new DuplicateEntityException("User exiosts"));

        // Act, Assert

        mockMvc.perform(get("/api/v1/users/new", "iliev")).andExpect(status().is4xxClientError());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void createUser_ShouldReturnStatusOk_whenUserCreated() throws Exception {
        //Arrange
        Mockito.when(service.createUser(any())).thenReturn(user);
        // Mockito.when(emailConfirmationTokenService.createConfirmationToken(any()))

        Gson gson = new Gson();
        String dtoToJson = gson.toJson(userRegistrationDTO);

        // Act, Assert
        mockMvc.perform(post("/api/v1/users/new")
                .contentType(MediaType.APPLICATION_JSON).content(dtoToJson))
                .andExpect(status().isOk());
    }


    @Test
    public void confirmUserAccount_ShouldReturnStatusOk_whenTokenIsValid() throws Exception {
        //Arrange
        Mockito.when(emailConfirmationTokenService.findByConfirmationToken(anyString())).thenReturn(confirmationToken);

        // Act, Assert
        mockMvc.perform(get("/api/v1/users/confirm-account").param("token", "3xfsdggtrt")).andExpect(status().isOk());
    }

    @Test
    public void confirmUserAccount_ShouldReturnStatus4xx_whenTokenInvalid() throws Exception {
        //Arrange
        Mockito.when(emailConfirmationTokenService.findByConfirmationToken(anyString())).thenReturn(null);

        // Act, Assert
        mockMvc.perform(get("/api/v1/users/confirm-account").param("token", "3xfsdggtrt")).andExpect(status().is4xxClientError());
    }

/*   @Test
    public void updateUser_ShouldReturnStatusOk_whenUserExists() throws Exception{

        // Arrange
       Mockito.when(service.updateUser(user,userUpdateDTO)).thenReturn(user);
       //Mockito.when()
        Gson gson = new Gson();
        String body = gson.toJson(user);

        //Act, Assert
        mockMvc.perform(post("/api/v1/users/update")
                .contentType(MediaType.APPLICATION_JSON).content(body))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUser_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception {
        //Arrange
        Mockito.when(service.updateUser(user,userUpdateDTO)).thenThrow(new EntityNotFoundException("User not found"));

        // Act, Assert

        mockMvc.perform(put("/api/v1/users/update", "iliev")).andExpect(status().is4xxClientError());
        *//*andExpect(jsonPath("$.name") *//*
        // value(user.getName())).andDo(print());
    } */


    @Test
    public void deleteUser_ShouldReturnStatusOk_whenUserExists() throws Exception {
        //Arrange, Act, Assert

        mockMvc.perform(delete("/api/v1/users/delete/{userName}", "iliev"))
                .andExpect(status().isOk());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void deleteUser_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception{
        //Arrange
        doThrow(new EntityNotFoundException("User not found")).when(service).deleteUser(anyString());

        // Act, Assert

        mockMvc.perform(delete("/api/v1/users/delete/{userName}", "ilko")).andExpect(status().is4xxClientError());
    }


    @Test
    public void blockUser_ShouldReturnStatusOk_whenUserExists() throws Exception {
        //Arrange, Act, Assert

        mockMvc.perform(put("/api/v1/users/block/{userName}", "iliev"))
                .andExpect(status().isOk());
        /*andExpect(jsonPath("$.name") */
        // value(user.getName())).andDo(print());
    }

    @Test
    public void blockUser_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception{
        //Arrange
        doThrow(new EntityNotFoundException("User not found")).when(service).blockUser(anyString());

        // Act, Assert

        mockMvc.perform(put("/api/v1/users/block/{userName}", "ilko")).andExpect(status().is4xxClientError());
    }

    @Test
    public void unblockUser_ShouldReturnStatusOk_whenUserExists() throws Exception {
        //Arrange, Act, Assert

        mockMvc.perform(put("/api/v1/users/unblock/{userName}", "iliev"))
                .andExpect(status().isOk());
        //*andExpect(jsonPath("$.name") *//*
        // value(user.getName())).andDo(print());
    }


    @Test
    public void unblockUser_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception{
        //Arrange
        doThrow(new EntityNotFoundException("User not found")).when(service).activateAccount(anyString());

        // Act, Assert

        mockMvc.perform(put("/api/v1/users/unblock/{userName}", "ilko")).andExpect(status().is4xxClientError());
    }




  /*  @Test
    public void setAdmin_ShouldReturnStatusOk_whenUserExists() throws Exception {
        //Arrange, Act, Assert

        mockMvc.perform(put("/api/v1/users/setAdmin/{userName}", "iliev"))
                .andExpect(status().isOk());
        *//*andExpect(jsonPath("$.name") *//*
        // value(user.getName())).andDo(print());
    }

    @Test
    public void setAdmin_ShouldReturnStatus4xx_whenUserNonExistent() throws Exception{
        //Arrange
        doThrow(new EntityNotFoundException("User not found")).when(service).setAdmin(anyString());

        // Act, Assert

        mockMvc.perform(put("/api/v1/users/setAdmin/{userName}", "ilko")).andExpect(status().is4xxClientError());
    }
    */
}
