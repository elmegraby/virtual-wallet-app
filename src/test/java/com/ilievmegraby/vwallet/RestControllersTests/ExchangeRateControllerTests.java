
package com.ilievmegraby.vwallet.RestControllersTests;

import com.google.gson.Gson;
import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.models.ExchangeRate;
import com.ilievmegraby.vwallet.models.dtos.TransactionDTO;
import com.ilievmegraby.vwallet.services.ExchangeRateService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static com.ilievmegraby.vwallet.ObjectFactory.getCardDetails;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class ExchangeRateControllerTests {

    private ExchangeRate exchangeRate;
    private TransactionDTO transactionDTO;

    @MockBean
    ExchangeRateService exchangeRateService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.exchangeRate = ObjectFactory.getExchangeRate();
        this.transactionDTO = ObjectFactory.getTransactionDTO();

    }


    @Test
    public void confirmUserAccount_ShouldReturnExchangeRate() throws Exception {
        //Arrange
        Mockito.when(exchangeRateService.getExchangeRate(any(), any())).thenReturn(exchangeRate);

        // Act, Assert
        mockMvc.perform(get("/api/v1/exchange").param("source", "EUR").param("target", "BGN")).
                andExpect(status().isOk());
    }

    /*@Test
    public void getConvertedSum_ShouldReturnConvertedSum() throws Exception {
        //Arrange
        Mockito.when(exchangeRateService.convert(any(), any(), any())).thenReturn(BigDecimal.valueOf(10));

        // Act, Assert
        mockMvc.perform(get("/api/v1/exchange").p.param("", "EUR").param("", String.valueOf(10))).
                andExpect(status().isOk());
    }

     */

   /* @Test
    public void createUser_ShouldReturnStatusOk_whenUserCreated() throws Exception {
        //Arrange
        ResponseEntity<String> resp = new ResponseEntity<>("hello", HttpStatus.OK);
        Mockito.when(exchangeRateService.getExchangeRateInfoMessage(any())).thenReturn(resp);
        Gson gson = new Gson();
        String dtoToJson = gson.toJson(transactionDTO);

        // Act, Assert
        mockMvc.perform(post("/api/v1/exchange")
                .contentType(MediaType.APPLICATION_JSON).content(dtoToJson));

    }

    */
}

