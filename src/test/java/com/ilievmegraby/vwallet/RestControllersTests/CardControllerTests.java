package com.ilievmegraby.vwallet.RestControllersTests;

import com.ilievmegraby.vwallet.ObjectFactory;
import com.ilievmegraby.vwallet.exceptions.DuplicateEntityException;
import com.ilievmegraby.vwallet.exceptions.EntityNotFoundException;
import com.ilievmegraby.vwallet.exceptions.UnauthorizedActionException;
import com.ilievmegraby.vwallet.models.CardDetails;
import com.ilievmegraby.vwallet.models.User;
import com.ilievmegraby.vwallet.repositories.CardRepository;
import com.ilievmegraby.vwallet.services.CardService;
import com.ilievmegraby.vwallet.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.smartcardio.Card;

import java.security.Principal;

import static com.ilievmegraby.vwallet.ObjectFactory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username="admin",roles={"USER","ADMIN"})
public class CardControllerTests {
    private CardDetails cardDetails;
    private User user;


    @MockBean
    CardRepository cardRepository;

    @MockBean
    CardService cardService;

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void before() {
        this.cardDetails = getCardDetails();
        this.user = ObjectFactory.getUser();
    }

    @Test
    public void getById_should_return_status_200_when_card_exists() throws Exception {
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        Mockito.when(cardRepository.getById(anyInt())).thenReturn(cardDetails);
        Mockito.when(cardService.getById(any(),anyInt())).thenReturn(cardDetails);

        //Act and assert
        mockMvc.perform(get("/api/v1/cards/getById/{id}", 1))
                .andExpect(status().isOk()).
                andExpect(jsonPath("$.cardNumber").value(cardDetails.getCardNumber()))
                .andExpect(jsonPath("$.expirationDate").value(cardDetails.getExpirationDate()))
                .andExpect(jsonPath("$.cardHolderName").value(cardDetails.getCardHolderName()))
                .andExpect(jsonPath("$.cvs").value(cardDetails.getCVS()));
    }

    @Test
    public void getById_should_return_status_404_when_card_not_found() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        Mockito.when(cardService.getById(any(), anyInt())).thenThrow(new EntityNotFoundException(MESSAGE_BODY));
        //Act and assert
        mockMvc.perform(get("/api/v1/cards/getById/{id}", 1))
                .andExpect(status().isNotFound());

    }


    @Test
    public void getById_should_return_status_401_when_unauthorized() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(anyString())).thenReturn(user);
        Mockito.when(cardService.getById(any(), anyInt())).thenThrow(new UnauthorizedActionException(MESSAGE_BODY));
       //Act and assert
        mockMvc.perform(get("/api/v1/cards/getById/1", 1))
                .andExpect(status().isUnauthorized());
    }


    @Test
    public void create_should_return_status_200_when_card_created() throws Exception {
        //Arrange
        Mockito.when(cardService.create(cardDetails)).thenReturn(cardDetails);

        //Act and assert
        mockMvc.perform(post("/api/v1/cards/new").content(asJsonString(cardDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).
                andExpect(jsonPath("$.cardNumber").value(cardDetails.getCardNumber()))
                .andExpect(jsonPath("$.expirationDate").value(cardDetails.getExpirationDate()))
                .andExpect(jsonPath("$.cardHolderName").value(cardDetails.getCardHolderName()))
                .andExpect(jsonPath("$.cvs").value(cardDetails.getCVS())).andDo(print());
    }

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    public void create_should_return_status_409_when_card_duplicated() throws Exception{
        //Arrange
        Mockito.when(cardService.create(cardDetails)).thenThrow(new DuplicateEntityException(MESSAGE_BODY));
        //Act and assert
        mockMvc.perform(post("/api/v1/cards/new").content(asJsonString(cardDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void update_should_return_status_200_when_card_updated() throws Exception {
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        Mockito.when(cardRepository.getById(anyInt())).thenReturn(cardDetails);
        Mockito.when(cardService.update(user, cardDetails)).thenReturn(cardDetails);
        //Act

        //Assert
        mockMvc.perform(put("/api/v1/cards/update").content(asJsonString(cardDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).
                andExpect(jsonPath("$.cardNumber").value(cardDetails.getCardNumber()))
                .andExpect(jsonPath("$.expirationDate").value(cardDetails.getExpirationDate()))
                .andExpect(jsonPath("$.cardHolderName").value(cardDetails.getCardHolderName()))
                .andExpect(jsonPath("$.cvs").value(cardDetails.getCVS())).andDo(print());
    }

    @Test
    public void update_should_return_status_404_when_card_not_found() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        Mockito.when(cardService.update(user,cardDetails)).thenThrow(new EntityNotFoundException(MESSAGE_BODY));
        //Act and assert
        mockMvc.perform(put("/api/v1/cards/update").content(asJsonString(cardDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_should_return_status_401_when_unauthorized() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        Mockito.when(cardService.update(user,cardDetails)).thenThrow(new UnauthorizedActionException(MESSAGE_BODY));
        //Act and assert
        mockMvc.perform(put("/api/v1/cards/update").content(asJsonString(cardDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }


    @Test
    public void delete_should_return_status_OK_when_authorized() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenReturn(user);
        //Act and assert
        mockMvc.perform(delete("/api/v1/cards/delete").content("1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_should_thow_code4xx_when_entity_not_found() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenThrow(new EntityNotFoundException("user not found"));
        //Act and assert
        mockMvc.perform(delete("/api/v1/cards/delete").content("1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void delete_should_thow_code4xx_when_user_unauthorized() throws Exception{
        //Arrange
        Mockito.when(userService.getByName(any())).thenThrow(new UnauthorizedActionException("unauthorized"));
        //Act and assert
        mockMvc.perform(delete("/api/v1/cards/delete").content("1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}


