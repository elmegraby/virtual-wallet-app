package com.ilievmegraby.vwallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ilievmegraby.vwallet.ServiceTests.BalanceLoadServiceImplTests;
import com.ilievmegraby.vwallet.models.*;
import com.ilievmegraby.vwallet.models.dtos.*;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode;
import com.ilievmegraby.vwallet.models.enums.WalletType;
import com.ilievmegraby.vwallet.models.verification.ConfirmationToken;
import com.ilievmegraby.vwallet.models.verification.TransactionConfirmationCode;
import org.apache.tomcat.util.buf.UEncoder;
import org.springframework.mock.web.MockMultipartFile;
import com.ilievmegraby.vwallet.models.enums.CurrencyCode.*;

import javax.smartcardio.Card;
import java.awt.font.TransformAttribute;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.sql.Timestamp;

public class ObjectFactory {

    public static User getUser() {
        return new User("iliev-telerik", "iliev955", "+359675654"
                , "iliev955@mail.bg", "ilko iliev", "image", getWallet());
    }

    public static User getSecondUser() {
        return new User("iliev-telerik-1", "iliev955", "+359675653"
                , "iliev956@mail.bg", "ilko iliev", "image", getWallet());
    }

    public static Transaction getTransaction() {
        return new Transaction(getUser(), getUser(), new BigDecimal(100), getWallet(), "pay a friend", getWallet());
    }

    public static Wallet getWallet() {
        return new Wallet("wallet1", new BigDecimal("100"), new User(), WalletType.PERSONAL, CurrencyCode.EUR);
    }

    public static Wallet getRecipientWallet(){
        return new Wallet("wallet2", new BigDecimal("100"), new User(), WalletType.PERSONAL, CurrencyCode.USD);
    }

    public static Role getRole() {
        return new Role("ROLE_USER");
    }

    public static Role getAdminRole() {
        return new Role("ROLE_ADMIN");
    }

    public static CardDetails getCardDetails() {
        CardDetails card = new CardDetails("1234-5678-9101-1213"
                , "03/24", "ILKO ILIEV", "123");
        card.setUser(getUser());
        return card;
    }

    public static CardDetails getInvalidCard(){
        CardDetails card = getCardDetails();
        card.setCardNumber(null);
        return card;
    }

    public static BalanceLoad getBalanceLoad() {
        return new BalanceLoad(getCardDetails(), new Timestamp(new java.util.Date().getTime()), BigInteger.valueOf(123)
                , com.ilievmegraby.vwallet.models.enums.CurrencyCode.BGN, "Add money to virtual wallet.", getWallet(), getUser());
    }

    public static Referral getReferral() {
        return new Referral(getUser(), "ilko95@mail.bg");
    }

    public static ExchangeRate getExchangeRate() {
        double rate = 1.56;
        return new ExchangeRate(CurrencyCode.USD, CurrencyCode.BGN, new BigDecimal(rate, MathContext.DECIMAL64));
    }

    public static UserRegistrationDTO getRegistrationDTO() {
        return new UserRegistrationDTO("iliev-telerik", "ilko iliev"
                , "pass123567", "iliev95@mail.bg", "+359675645");
    }

    public static UserUpdateDTO getUpdateDTO() {
        return new UserUpdateDTO("ilko iliev", "iliev95@mail.bg", "+35955634554", new MockMultipartFile(
                "test.txt",
                "ProfileImage".getBytes()), "ilko123123");
    }

    public static WalletDTO getWalletDTO() {
        return new WalletDTO(new User(), WalletType.PERSONAL, CurrencyCode.EUR, "wallet1");
    }

    public static TransactionDTO getTransactionDTO() {
        long current = System.currentTimeMillis();
        Timestamp time = new Timestamp(current);
        return new TransactionDTO(1, 2, 80, time, 1, true, "Pay a fr");
    }

    public static BalanceLoadDTO getBalanceLoadDTO() {
        return new BalanceLoadDTO(1, 1, 1, "Pay a friend", "USD", 100);
    }

    public static TransactionConfirmationCode getTCC() {
        return new TransactionConfirmationCode(getTransaction());
    }

    public static ConfirmationToken getConfirmationToken() {
        return new ConfirmationToken(getUser());
    }

    public static String MESSAGE_BODY = "Exception message";

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
