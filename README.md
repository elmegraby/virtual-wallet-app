# Virtual Wallet

Telerik Academy Final Project /team project/

- A web application that allows users to create virtual wallets, manage
credit cards used to deposit money into them and then make transfers
to others with live multi-currency support
- User and admin roles present
- REST API for external use

Technologies: Java, Spring, Hibernate, Thymeleaf, MySQL, Gradle

<p align="center"><img src="https://i.imgur.com/LIu4bLd.jpg"/></p>
<p align="center"><img src="https://i.imgur.com/hhOsJL4.jpg"/></p>
<p align="center"><img src="https://i.imgur.com/0ztJXOX.jpg"/></p>
<p align="center"><img src="https://i.imgur.com/c2ehHca.jpg"/></p>
